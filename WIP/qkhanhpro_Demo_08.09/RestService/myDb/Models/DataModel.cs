﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myDb.Models
{
    public class DataModel
    {
        public DataModel()
        { 

        }
        public DataModel(string data)
        {
            myString = data;
        }

        public int Id { get; set; }

        public string myString { get; set; }
    }
}
