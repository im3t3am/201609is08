﻿using myDb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myDb.EfDbServices
{
    
    public class myDbContext : DbContext
    {
        public myDbContext(string connstr)
            : base(connstr)
        {
            Database.SetInitializer<myDbContext>(new MyInitializer());
        }

        public DbSet<DataModel> Models { get; set; } 
    }
}
