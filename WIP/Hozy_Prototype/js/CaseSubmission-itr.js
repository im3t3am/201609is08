angular.module("CaseSubmission",['flow'])
.config(['flowFactoryProvider', function (flowFactoryProvider) {
  flowFactoryProvider.defaults = {
    target: 'upload.php',
    permanentErrors: [404, 500, 501],
    maxChunkRetries: 1,
    chunkRetryInterval: 5000,
    simultaneousUploads: 4
  };
  flowFactoryProvider.on('catchAll', function (event) {
    console.log('catchAll', arguments);
  });
  // Can be used with different implementations of Flow.js
  // flowFactoryProvider.factory = fustyFlowFactory;
}])
.controller("AdditionalDetailController", function(){
    var addDetail = this;
    var ctrl = this;
    var range = [];
        var range1 = [];
        for(var i=0;i<=99;i++) {
            range.push(i);
        }
        for(var i=0;i<=11;i++) {
            range1.push(i);
        }
        addDetail.range = range;
        addDetail.range1 = range1; 
    
    this.updateHtml = function() {
      ctrl.tinymceHtml = $sce.trustAsHtml(ctrl.tinymce);
    };
    this.tinymceOptions = {
        onChange: function(e) {
          // put logic here for keypress and cut/paste changes
        },
        inline: false,
        plugins : 'advlist autolink link image lists charmap print preview',
        skin: 'lightgray',
        theme : 'modern'
      };
        
	addDetail.JusDoc=[
		{DocName:'Document1.docx', DocDes:'Document 1 description'},
		{DocName:'Document2.pdf', DocDes:'Document 2 description'}
	];

    addDetail.SitePlansCheck = function(){
        if (addDetail.checked1 == false && addDetail.checked2 == false && addDetail.checked3 == false) {
            alert('Must choose at least one item');
          addDetail.checked1 = true;
        }

        
    };

	addDetail.AddJusDoc = function(){
		addDetail.JusDoc.push({DocName:addDetail.JusDocName,DocDes:addDetail.JusDocDes});
		addDetail.JusDocName = '';
		addDetail.JusDocDes = '';
	};
    addDetail.toggle = function(){
        alert('a');
        if (addDetail.checked1 == true && addDetail.checked2 == true) {
          addDetail.myVar =true;
        }
        else{addDetail.myVar=false;}
    }; 
	addDetail.RemoveJusDoc = function(idx){
		addDetail.JusDoc.splice(idx, 1);
	};
	addDetail.PMLetter=[
		{PMLetterName:'Letter from parent Ministry.pdf', PMLetterDes:'Letter description'}
	];
	addDetail.AddPMLetter = function(){
		addDetail.PMLetter.push({PMLetterName:addDetail.PMLetterName,PMLetterDes:addDetail.PMLetterDes});
		addDetail.PMLetterName = '';
		addDetail.PMLetterDes = '';
	};
	addDetail.RemovePMLetter = function(idx){
		addDetail.PMLetter.splice(idx, 1);
	};
	addDetail.parseFloat=function(val) {
	    return isNaN(parseFloat(val)) ? 0: parseFloat(val);
	}
	addDetail.nextCaseDetails = function(){
		switch(addDetail.dropCaseType){
			case '1':
				window.location.href = addDetail.dropSubCaseType;
				break;
			case '2':
				window.location.href = 'SUB-ProvideCaseDetail-NONFA-MNDEnquiry.html';
				break;
			case '3':
				window.location.href = 'SUB-ProvideCaseDetail-NONFA-PlanningRequisition.html';
				break;
			case '4':
				window.location.href = 'SUB-ProvideCaseDetail-NONFA-SiteSearch.html';
				break;
			case '5':
				window.location.href = 'SUB-ProvideCaseDetail-NONFA-Pre-Submission.html';
				break;
		}
	};
    addDetail.dropsubmissiontype = {
        'Consultation to Agencies for clearance': {
            'Inter-Agency Consultation':{
                'Inter-Agency Consultation':'Inter-Agency Consultation'
            }
        },
        'Submission to URA for approval': {
           'Enquiry': {
                'MND Enquiry':'MND Enquiry',
                'MND General Enquiry':'MND General Enquiry',
                'Enquiry from DS and Above':'Enquiry from DS and Above'
            },
            'Formal Application': {
                'Extension of Validity':'Extension of Validity',
                'Formal Application (FA)':'Formal Application (FA)'
            },
            'Land Use Consultation': {
                'Extension of TA/TOL':'Extension of TA/TOL',
                'General Enquiry':'General Enquiry',
                'Interim Use New':'Interim Use New',
                'Interim Use Prior Approval':'Interim Use Prior Approval',
                'Interim Use Vacant State Land':'Interim Use Vacant State Land',
                'LUC - Alienation':'LUC - Alienation',
                'Signage':'Signage'
            },
            'Planning Requisition': {
                'Planning Requisition':'Planning Requisition'
            },
            'Pre-Submission Consultation': {
                'Pre-Submission Consultation':'Pre-Submission Consultation'
            },
            'Site Search': {
                'Site Search':'Site Search'
            },
            'Inter-Agency Consultation': {
                'Inter-Agency Consultation':'Inter-Agency Consultation'
            }
        }
    };
    addDetail.showFA = function(){
    	if(angular.isUndefined(addDetail.subCaseTypes)){
    		return false;
    	}
    	if(addDetail.subCaseTypes==null){
    		return false;
    	}
    	if(addDetail.subCaseTypes.$$hashKey == 'object:4'){
    		return true;
    	}
    	return false;
    };
    addDetail.nextSubmissionCase = function(){
    	if(angular.isUndefined(addDetail.subCaseType)){
    		return false;
    	}
    	if(addDetail.subCaseType==null){
    		return false;
    	}
    	switch(addDetail.subCaseType){
    		case 'MND Enquiry':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-MNDEnquiry.html';
    			break;
            case 'Enquiry from DS and Above':
                window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-EnquiryfromDSandAbove.html';
                break;
            case 'MND General Enquiry':
                window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-MNDGeneralEnquiry.html';
                break;
    		case 'Extension of Validity':
    			window.location.href = './FA/SUB-ITR-ProvideCaseDetail-ExtensionOfValidity.html';
    			break;
    		case 'Formal Application (FA)':
    			window.location.href = './FA/SUB-ITR-CaseDetails.html';
    			break;
    		case 'Extension of TA/TOL':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-Extension-TOL.html';
    			break;
    		case 'General Enquiry':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-GeneralEnquiry.html';
    			break;
    		case 'Interim Use New':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-InterimUse-StateProperty.html';
    			break;
    		case 'Interim Use Prior Approval':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-InterimUse-PriorApproval.html';
    			break;
    		case 'Interim Use Vacant State Land':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-InterimUse-VacantStateLand.html';
    			break;
    		case 'LUC - Alienation':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-Alienation.html';
    			break;
    		case 'Signage':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-Signage.html';
    			break;
    		case 'Planning Requisition':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-PlanningRequisition.html';
    			break;
    		case 'Pre-Submission Consultation':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-PreSubmission.html';
    			break;
    		case 'Site Search':
    			window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-SiteSearch.html';
    			break;
            case 'Inter-Agency Consultation':
                window.location.href = './NONFA/SUB-ITR-ProvideCaseDetail-NONFA-Inter-Agency-Consultation.html';
            break;
    	}
    };
    
})
.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0].name;
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}])
.filter('currentdate',['$filter',  function($filter) {
    return function() {
        return $filter('date')(new Date(), 'dd/MM/yyyy hh:mm:ss a');
    };
}])
.filter('filesize',function(){
    return function(value){
        return (parseFloat(value / 1024000).toFixed(2)+" MB");
    };
})