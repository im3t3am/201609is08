$(document).ready(function(){
	changeContentSize();
	$('.changeCaptcha').on('click', function(){
		$('#CaptchaImg').attr('src','img/captcha.jpg');
	});
	$('.fa-info-circle').tooltip();
	$('.collapse-all').on('click', function(){
		changeContentSize();
		if($(this).find('.fa:first').hasClass('fa-compress')){
			$(this).html('<i class="fa fa-expand"></i> Expand All');
            $('.section-title').find('.fa:first').removeClass('fa-minus-circle').addClass('fa-plus-circle');
            $('.section-content').css('display', 'none');
		}else{
			$(this).html('<i class="fa fa-compress"></i> Collapse All');
            $('.section-title').find('.fa:first').removeClass('fa-plus-circle').addClass('fa-minus-circle');
            $('.section-content').css('display', 'block');
		}
	});
	$('.section-title').on('click', function(){
		changeContentSize();
		if($(this).find('.fa:first').hasClass('fa-minus-circle')){
			$(this).find('.fa:first').removeClass('fa-minus-circle').addClass('fa-plus-circle');
		}else{
			$(this).find('.fa:first').removeClass('fa-plus-circle').addClass('fa-minus-circle');
		}
		$(this).parent().find('.section-content:first').toggle(500);
	});
	$('.chkSection').on('change', function(){
		var isCheck = $(this).is(':checked');
		if(isCheck){
			$(this).closest('.checkbox-section').find('.checkbox-content:first').show();
		}else{

			$(this).closest('.checkbox-section').find('.checkbox-content:first').hide();
		}
	});

	// Validate Data
	$('#btnSubmit').on('click', function(){
		var errorCount =0;
		var fieldFocus="";
		$('.fieldRequired').each(function(){
			if($.trim($(this).val()).length==0){				
				errorCount++;
				$(this).css('border-color','#D9534F');
				$(this).parent().find('.valMessage:first').css('display','block');
				if(errorCount==1){
					$(this).focus();
				}
			}else{
				$(this).css('border-color','#ccc');
				$(this).parent().find('.valMessage:first').css('display','none');
			}
		});
		if(errorCount>0){
			return false;	
		}
        window.location.href = $(this).attr('next-view');
		return true;
	});
	$('.btnIamFrom').on('click', function(){
		$('.rdIamFrom').each(function(){
			if($(this).is(":checked")){
				var nextStep = $(this).val();
				window.location.href = nextStep;
			}
		});
	});
	$('.fieldRequired').on('change', function(){
		if($.trim($(this).val()).length==0){				
			$(this).css('border-color','#D9534F');
			$(this).parent().find('.valMessage:first').css('display','block');
		}else{
			$(this).css('border-color','#ccc');
			$(this).parent().find('.valMessage:first').css('display','none');
		}
	});
    $('.fieldRequired').on('keyup', function(){
        if($.trim($(this).val()).length==0){                
            $(this).css('border-color','#D9534F');
            $(this).parent().find('.valMessage:first').css('display','block');
        }else{
            $(this).css('border-color','#ccc');
            $(this).parent().find('.valMessage:first').css('display','none');
        }
    });
    var availWidth =  window.screen.availWidth;
    if(availWidth<768){
        $('.stepCircle').addClass('circleTooltip');
        $('.circleTooltip').tooltip();
    }else{
        $('.stepCircle').removeClass('circleTooltip');
    }
	$(window).on("resize", function () {
		changeContentSize();
        var sizeWidth = $('#site-wrapper').width();
        if(sizeWidth<768){
        	$('.stepCircle').addClass('circleTooltip');
        	$('.circleTooltip').tooltip();
        }else{
        	$('.stepCircle').removeClass('circleTooltip');
        }
    });
     $('.glyphicon.form-control-feedback').each(function(){
     	$(this).on('click',function(){
	    	$(this).parent().find('input:first').val("");
	    	$(this).css('display','none');
	    	$(this).parent().parent().find('.glyphicon-ok:first').css('display','none');
	    });
     	var dataInput = $(this).parent().find('input:first');
     	dataInput.focus(function(){
     		$(this).parent().find('.glyphicon-remove:first').css('display','block');
     	});
     	dataInput.blur(function(){
     		if ($(this).parent().find('.glyphicon-remove:first').is(':hover')) 
     		{
		    }else{
		    	$(this).parent().find('.glyphicon-remove:first').css('display','none');
		    }
     	});
     	dataInput.on('keyup',function(){
	     	if(isValidEmailAddress($(this).val())){
	     		var icon = $(this).parent().parent().find('.glyphicon-ok:first').css('display','block');
	     	}else{
	     		$(this).parent().parent().find('.glyphicon-ok:first').css('display','none');
	     	}
     	});
     });
   

    // Search Form
    $('#SearchType').on('change', function(){
    	changeContentSize();
    	var divId = $(this).val();
    	$('.'+divId).show().siblings().hide();
    	$('.sectionSearchResult').hide();
    	$('.sectionSearchResult1').hide();
    	if(divId=="Others"){
    		$('.search-button').hide();
    	}else{
    		$('.search-button').show();
    	}
    });
    $('#dropOthers').on('change', function(){
    	changeContentSize();
    	var otherValue = $(this).val();
    	if(otherValue.length>0){
    		$('.sectionSearchResult').show();
    	}else{
    		$('.sectionSearchResult').hide();
    	}
    });
    $('#btnSearch').on('click', function(){
    	changeContentSize();
    	var check = $('#SearchType').val();
    	if(check=="searchByDocument"){
    		$('.sectionSearchResult').hide();
    		$('.sectionSearchResult1').show();
    	}else{
    		$('.sectionSearchResult1').hide();
    		$('.sectionSearchResult').show();
    	}
    	
        if($('.section-title').find('.fa:first').hasClass('fa-minus-circle')){
            $('.section-title').find('.fa:first').removeClass('fa-minus-circle').addClass('fa-plus-circle');
        }
        $('.section-title').parent().find('.section-content:first').hide();
        $('.search-result-title').trigger('click');
    });

    //Gridview
    $('.gridSortLink').on('click', function(){
    	if($(this).find('.fa:first').hasClass('fa-sort')){
    		$(this).find('.fa:first').removeClass('fa-sort').addClass('fa-caret-up')
    	}
    	else
    	{
		    if($(this).find('.fa:first').hasClass('fa-caret-down')){
	    		$(this).find('.fa:first').removeClass('fa-caret-down').addClass('fa-caret-up');
	    	}else{
	    		$(this).find('.fa:first').removeClass('fa-caret-up').addClass('fa-caret-down');
	    	}
    	}
    });
    $('.gridFilterLink').on('click', function(){
    	$(this).closest('th').css('background-color','#579ddb');
    	var thWidth = $(this).closest('th').width();
    	var ulHeight = $(this).parent().find('.dropdown-menu:first').height() + $(this).closest('th').height();
    	$(this).css('color','#f3f3f3');
    	$(this).closest('th').find('ul:first').css('width',thWidth+16);
    	$(this).closest('.table').css('min-height',ulHeight);
    });
    $('.dropdownFilter').on('hidden.bs.dropdown', function () {
	    $(this).closest('th').css('background-color','#f3f3f3');
    	$(this).find('a:first').css('color','#515151');    
    	$(this).closest('.table').css('min-height',0);	
	});

    $('#btnAddDocument').on('click',function(){
        $('#addDocumentModal').modal('hide');
        $('#addDocumentModal').find('input[type=file]:first').val("");
    });


    // Common
    $('#datePicker1').datepicker({format: 'dd/mm/yyyy'});
    $('#dateSubmission1').datepicker({format: 'dd/mm/yyyy'});
    $('#dateSubmission2').datepicker({format: 'dd/mm/yyyy'});
    $('#dateDecision1').datepicker({format: 'dd/mm/yyyy'});
    $('#dateDecision2').datepicker({format: 'dd/mm/yyyy'});
    

    if(window.location.href.indexOf('SUB-ProvideCaseDetail.html')!=-1){
    	DisplayByType();
    }

    function DisplayByType(){
    	// Check Agency or Private
	    var pageType = getUrlParameter('type');
	    if(pageType=='agency'){
	    	$('#form-agency').show();
	    	$('#form-private').hide();
	    }else if(pageType=='private'){
	    	$('#form-agency').hide();
	    	$('#form-private').show();
	    }
    }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgAvatar')
                .attr('src', e.target.result)
                .css('display','block')
                .css('margin-bottom','10px')
                .width(120)
                .height(140);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

// function changeContentSize(){
// 	$('#in-page-content').css('min-height',window.screen.availHeight-405);
// }

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
} 

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function ShowhideDiv()
	{
		var radios = $('#racopy:checked').val();		
				// do whatever you want with the checked radio
				if(radios=="Yes")
				{
					document.getElementById("fs-copy-check-yes").style.display = 'block';
					//document.getElementById("fs-copy-check-no").style.display = 'none';

				}else
				{
					//document.getElementById("fs-copy-check-no").style.display = 'block';
					document.getElementById("fs-copy-check-yes").style.display = 'none';
				}

				

	}
	function ShowSiteLocation()
	{
		var radios = $('#option1:checked').val();
		var radios1 = $('#option2:checked').val();
		var radios2 = $('#option3:checked').val();
		var radios3 = $('#option4:checked').val();
		
				// do whatever you want with the checked radio
				if(radios=="option1")
				{
					
					document.getElementById("fs-Add").style.display = 'block';
					document.getElementById("fs-LotNumber").style.display = 'none';
					document.getElementById("fs-CAD").style.display = 'none';


				}else if(radios1=="option2")
				{
					
					
					document.getElementById("fs-Add").style.display = 'none';
					document.getElementById("fs-LotNumber").style.display = 'block';
					document.getElementById("fs-CAD").style.display = 'none';
				}
				else if(radios2=="option3")
				{
					
					document.getElementById("fs-Add").style.display = 'none';
					document.getElementById("fs-LotNumber").style.display = 'none';
					document.getElementById("fs-CAD").style.display = 'block';
				}
				else{
					document.getElementById("fs-Add").style.display = 'none';
					document.getElementById("fs-LotNumber").style.display = 'none';
					document.getElementById("fs-CAD").style.display = 'none';
					
	}
				}
function ShowhideDiv2()
{
	//var checked = $("#cbxBeHalf").attr('checked')	
			// do whatever you want with the checked radio
			if($("#cbxbehalf").prop('checked') == true ){
					document.getElementById("fs-behalf-checked").style.display = 'block';
					
					//document.getElementById("fs-behalf-checked").style.display = 'none';
			}
			else
				document.getElementById("fs-behalf-checked").style.display = 'none';
			
			// if(radios=="Yes")
			// {
			// 	document.getElementById("fs-copy-check-yes").style.display = 'block';
			// 	document.getElementById("fs-copy-check-no").style.display = 'none';

			// }else
			// {
			// 	document.getElementById("fs-copy-check-no").style.display = 'block';
			// 	document.getElementById("fs-copy-check-yes").style.display = 'none';
			// }

			

}
	
	function ForwardAll()
	{
	$("#slcaseleft option").each(function()
		{
			$('#slcaseright').append('<option value="'+$(this).val()+' ">'+$(this).text()+'</option>');
			$(this).remove();
		});

	}
	function BackwardAll()
	{
	$("#slcaseright option").each(function()
		{
			$('#slcaseleft').append('<option value="'+$(this).val()+' ">'+$(this).text()+'</option>');
			$(this).remove();
		});

	}
	function ForwardSelect()
	{
	$("#slcaseleft option:selected").each(function()
		{
			$('#slcaseright').append('<option value="'+$(this).val()+' ">'+$(this).text()+'</option>');
			$(this).remove();
		});

	}
	function BackwardSelect()
	{
		$("#slcaseright option:selected").each(function()
		{
			$('#slcaseleft').append('<option value="'+$(this).val()+' ">'+$(this).text()+'</option>');
			$(this).remove();
		});

	}
	
	function ChoiceCaseCopy()
	{
			var choice = $("#dropcasetocopy option:selected").val();
			if(choice !="0")
			{
				$("input#lbcaseno").val("EP2015051800"+choice);
				$("textarea#txtcaseproposal").val(choice + "- Extension of Validity of Planning Decision Proposal Description Thrusday, 25 October 2012, 11:00.");
			}else
			{
				$("input#lbcaseno").val("");
				$("textarea#txtcaseproposal").val("");
			}
	}