angular.module("MyCase",['ui.grid'])
.controller("ViewControlller", function(){
	var myCase = this;
	myCase.MyCases=[
		{CaseNo:"2015/1101", CaseType:"Case Type 1", SubCaseType: "Sub Case Type 1", ConsultationAgency: "Consultation Agency 1", OIC: "Eric Swenson", Status: "Processing", ExpectedDate : "22-Mar-13"},
		{CaseNo:"2015/1102", CaseType:"Case Type 2", SubCaseType: "Sub Case Type 2", ConsultationAgency: "Consultation Agency 1", OIC: "Richeese Cake", Status: "Processing", ExpectedDate : "22-Mar-13"},
		{CaseNo:"2015/1103", CaseType:"Case Type 3", SubCaseType: "Sub Case Type 3", ConsultationAgency: "Consultation Agency 1", OIC: "Huu Zi Zuan", Status: "Processing", ExpectedDate : "22-Mar-13"},
		{CaseNo:"2015/1104", CaseType:"Case Type 4", SubCaseType: "Sub Case Type 4", ConsultationAgency: "Consultation Agency 1", OIC: "Daniel Heminway", Status: "Processing", ExpectedDate : "22-Mar-13"},
		{CaseNo:"2015/1105", CaseType:"Case Type 5", SubCaseType: "Sub Case Type 5", ConsultationAgency: "Consultation Agency 1", OIC: "Steve Job", Status: "Processing", ExpectedDate : "22-Mar-13"},
		{CaseNo:"2015/1106", CaseType:"Case Type 6", SubCaseType: "Sub Case Type 6", ConsultationAgency: "Consultation Agency 1", OIC: "Eric Cantona", Status: "Processing", ExpectedDate : "22-Mar-13"},
		{CaseNo:"2015/1107", CaseType:"Case Type 7", SubCaseType: "Sub Case Type 7", ConsultationAgency: "Consultation Agency 1", OIC: "Eric Swenson", Status: "Processing", ExpectedDate : "22-Mar-13"}
	];
	myCase.SearchResult=[
		{CaseNo:"12345-986", SubjectTitle : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",DecisionDate: "15/05/2015 12:05:00 PM", Status:"Completed", DownloadDecision: ""},
		{CaseNo:"12345-988", SubjectTitle : "Vestibulum semper lectus ante, at congue dui feugiat iaculis.",DecisionDate: "18/05/2015 12:05:00 PM", Status:"Pending", DownloadDecision: ""},
		{CaseNo:"12345-989", SubjectTitle : "Vestibulum bibendum scelerisque tempus.",DecisionDate: "20/05/2015 12:05:00 PM", Status:"Closed", DownloadDecision: ""},
		{CaseNo:"12345-990", SubjectTitle : "Suspendisse sed nisi at felis imperdiet tincidunt mattis nec urna.",DecisionDate: "17/05/2015 12:05:00 PM", Status:"Completed", DownloadDecision: ""},
		{CaseNo:"12345-991", SubjectTitle : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",DecisionDate: "15/05/2015 12:05:00 PM", Status:"Completed", DownloadDecision: ""},
		{CaseNo:"12345-992", SubjectTitle : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",DecisionDate: "15/05/2015 12:05:00 PM", Status:"Completed", DownloadDecision: ""},
		{CaseNo:"12345-993", SubjectTitle : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",DecisionDate: "16/05/2015 12:05:00 PM", Status:"Completed", DownloadDecision: ""},
		{CaseNo:"12345-994", SubjectTitle : "Suspendisse sed nisi at felis imperdiet tincidunt mattis nec urna.",DecisionDate: "17/05/2015 12:05:00 PM", Status:"Completed", DownloadDecision: ""},
		{CaseNo:"12345-995", SubjectTitle : "Vestibulum bibendum scelerisque tempus.",DecisionDate: "14/05/2015 12:05:00 PM", Status:"Completed", DownloadDecision: ""},
		{CaseNo:"12345-996", SubjectTitle : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",DecisionDate: "15/04/2015 12:05:00 PM", Status:"Pending", DownloadDecision: ""}
	];
	myCase.ListDocs0=[
		{DocName:"Document1.docx", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"},
		{DocName:"Document2.pdf", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"}
	];
	myCase.addDoc0 = function(){
		myCase.ListDocs0.push({DocName:myCase.DocName1, DocDes:myCase.DocDes1});
      	myCase.DocName1 = '';
      	myCase.DocDes1 = '';
	};
	myCase.delete0 = function(idx) {
		myCase.ListDocs0.splice(idx, 1);
	};
	myCase.ListDocs1=[
		{DocName:"Document1.docx", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"},
		{DocName:"Document2.pdf", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"}
	];
	myCase.addDoc1 = function(){
		myCase.ListDocs1.push({DocName:myCase.DocName1, DocDes:myCase.DocDes1});
      	myCase.DocName1 = '';
      	myCase.DocDes1 = '';
	};
	myCase.delete1 = function(idx) {
		myCase.ListDocs1.splice(idx, 1);
	};
	myCase.ListDocs2=[
		{DocName:"Document1.docx", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"},
		{DocName:"Document2.pdf", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"}
	];
	myCase.delete2 = function(idx) {
		myCase.ListDocs2.splice(idx, 1);
	};
	myCase.ListDocs3=[
		{DocName:"Document1.docx", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"},
		{DocName:"Document2.pdf", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"}
	];
	myCase.delete3 = function(idx) {
		myCase.ListDocs3.splice(idx, 1);
	};
	myCase.ListDocs4=[
		{DocName:"Document1.docx", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"},
		{DocName:"Document2.pdf", DocDes:"Extension of Validity of Planning Decision Proposal Description Thrusday"}
	];
	
	myCase.ListSubmitCase=[
		{FileName:"Authorisation Letter.pdf"}		
	];
	myCase.ListLocationPlan=[
		{DocName:"plan.gbd", DocType:"Plan",DocDes:"Plan description"}		
	];
	myCase.ListSiteAdded=[
		{Type:"Location", Location:"45 Maxwell Road"},		
		{Type:"Lot Number", Location:"MK04-012345A"},		
		{Type:"Site Location Plan", Location:"ObjectID: v123 (file name: ...)"},		
		{Type:"Site Location Plan", Location:"ObjectID: v124 (file name: ...)"},		
		{Type:"Site Location Plan", Location:"ObjectID: v126 (file name: ...)"},		
		{Type:"Select Polygon", Location:"ObjectID: v234"},		
	];
	myCase.addLocationPlan = function(){
		myCase.ListLocationPlan.push({DocName:myCase.DocName1, DocType:myCase.DocType1, DocDes:myCase.DocDes1});
		myCase.ListSiteAdded.push({Type:"Site Location Plan", Location:"ObjectID: v127 (file name: ...)"});
      	myCase.DocName1 = '';
      	myCase.DocDes1 = '';
      	myCase.DocType1 = '';
	};	
	myCase.addSubmitCase = function(){
		myCase.ListSubmitCase.push({FileName:myCase.DocName1});		
      	myCase.DocName1 = '';      	
	};	
	myCase.addLocation =function (){		
		myCase.ListSiteAdded.push({Type:"Location", Location:myCase.Location1});
      	myCase.Location1 = '';
      	
	};
	myCase.addLotNumber = function(){		
		myCase.ListSiteAdded.push({Type:"Lot number", Location:myCase.Lotnumber1});
      	myCase.Lotnumber1 = '';
      	
	};
	myCase.deleteLocationPlan = function(idx) {
		myCase.ListLocationPlan.splice(idx, 1);		
	};
	myCase.deleteSiteAdd = function(idx) {
		myCase.ListSiteAdded.splice(idx, 1);		
	};
	myCase.deleteSubmitCase = function(idx) {
		myCase.ListSubmitCase.splice(idx, 1);		
	};
	myCase.delete4 = function(idx) {
		myCase.ListDocs4.splice(idx, 1);
	};

	myCase.myData = [
        {"firstName": "Cox","lastName": "Carney","company":"Comveyer","employed":"false"},
        {"firstName": "Nancy","lastName": "Wise","company":"Enormo","employed":"false"}
    ];

    myCase.generateData = function(){
    	for (var i = 1000-1; i >= 0; i--) {
    		myCase.myData.push({"firstName": "Cox-"+i,"lastName": "Carney-"+i,"company":"Comveyer-"+i,"employed":"true"});
    	};
    }
    myCase.generateData ();
})
.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0].name;
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}]);