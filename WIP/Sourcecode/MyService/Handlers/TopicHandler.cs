﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;

namespace HozyService.Handlers
{
    public class TopicHandler
    {
        HozyDbContext _hozyDbContext;

        public TopicHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public TopicHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public TopicDto ConvertTopicToDto(Topic topic)
        {
            TopicDto topicDto = new TopicDto();
            topicDto.Id = topic.Id;
            topicDto.Description = topic.Description;
            topicDto.Name = topic.Name;
            topicDto.Image = topic.Image;
            if (topic.SubscribedUsers != null)
            {
                topicDto.Followcount = topic.SubscribedUsers.Count();
            }
            return topicDto;
        }

        /// <summary>
        /// Thêm một topic vào hệ thống với điều kiện Topic phải là duy nhất
        /// </summary>
        /// <param name="topicDto"></param>
        /// <returns></returns>
        public Topic AddTopic(TopicDto topicDto)
        {
            try
            {
                var topic = new Topic()
                {
                    Name = topicDto.Name,
                    Description = topicDto.Description,
                    Image = topicDto.Image 
                };
                Topic addedTopic = _hozyDbContext.Topics.Add(topic);
                _hozyDbContext.SaveChanges();
                Utility.Log4Net(1, "Topic inserted successfully in database");
                return addedTopic;
            }
            catch (Exception ex)
            {
                Utility.Log4Net(0, "Topic inserted in database FAIL");
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return null;
            }
            
        }

        /// <summary>
        /// User thêm một topic vào danh sách Topic của mình
        /// Front end sẽ gui trường UserId, TopicId
        /// Lấy UserID và Username từ Session
        /// </summary>
        /// <param name="topicDto"></param>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        public bool AddUserTopic(int id, Session currentSession)
        {
            User user = _hozyDbContext.Users.Find(currentSession.UserId);
            try
            {
                // Lấy trong DB bản ghi topic mà topicId từ frontend gửi xuống = topicId ở trong DB
                var topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == id);
                if (topic != null) topic.SubscribedUsers.Add(user);
                _hozyDbContext.SaveChanges();
                Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] add a topic [" + id + "] in topic list");
            }
            catch (Exception ex)
            {
                Utility.Log4Net(0, "User [User ID: " + currentSession.Id + "] add a topic ["+id+"] in topic list FAIL");
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Thêm một chủ đề vào cho câu hỏi 
        /// Frontend trả xuống gồm TopicId và QuestionId
        /// </summary>
        /// <param name="questionid"></param>
        /// <param name="currentSession"></param>
        /// <param name="topicid"></param>
        /// <returns></returns>
        public bool AddQuestionTopic(int topicid, int questionid, Session currentSession)
        {
            try 
            {
                var topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == topicid);
                var question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionid);
                //so sanh user co questionid voi token hien tai co trung` nhau hay khong
                var questioncheck = _hozyDbContext.Questions.FirstOrDefault(q => q.User.Id == currentSession.UserId);
                if (question != null && (questioncheck != null && ((topic != null) && (questioncheck.User.Id == question.User.Id || currentSession.RightList.Exists(right => right.RightName=="admin")))))
                {
                    topic.TaggedQuestions.Add(question);
                    topic.LastQuestionDate = DateTime.Now;
                    _hozyDbContext.SaveChanges();
                    Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] add a question [Question ID: " + questionid + "] into topic [Topic ID: "+topicid+"] successful");
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] add a question [Question ID: " + questionid + "] into topic [Topic ID: " + topicid + "] FAIL");
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
            return true;
        }

        public bool UserUnfollow(int topicid, Session currentSession) 
        {
            try
            {
                var topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == topicid);
                var user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == currentSession.UserId);
                if (topic.SubscribedUsers.Contains(user))
                {
                    topic.SubscribedUsers.Remove(user);
                }
                _hozyDbContext.SaveChanges();
                Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] remove a topic [" + topicid + "] in topic list");
            }
            catch (Exception ex)
            {
                Utility.Log4Net(0, "User [User ID: " + currentSession.Id + "] remove a topic [" + topicid + "] in topic list FAIL");
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
            return true;
        }

        public bool QuestionUnfollow(int topicid, int questionid, Session currentSession)
        {
            var session = _hozyDbContext.Sessions.FirstOrDefault(s => s.UserId == currentSession.UserId);
            var topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == topicid);
            var question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionid);
            if (session != null && (question != null && session.UserId == question.User.Id))
            {
                try
                {
                    if (topic != null && topic.TaggedQuestions.Contains(question))
                    {
                        topic.TaggedQuestions.Remove(question);
                    }
                    _hozyDbContext.SaveChanges();
                }
                catch (Exception e)
                {
                }
            }
            else return false;
            return true;
        }

        public List<Topic> GetUserTopic(int userid)
        {
            List<Topic> topics = null; 
            User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == userid);
            if(user != null){ 
                topics = user.SubscribedTopics;
            }
            Utility.Log4Net(1, "Get List Topic Of [User ID: "+userid+"]");
            return topics;
        }

        /// <summary>
        /// Lấy ra 10 câu hỏi mới nhất được gắn vào topic đã chọn
        /// </summary>
        /// <returns></returns>
        public List<Question> GetTopicNewQuestion(int topicId,int skip)
        {
            var topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == topicId);
            if (topic == null)
            {
                Utility.SetRespond(HttpStatusCode.NotFound,"Topic not found");
                return null;
            }

            return
                _hozyDbContext.Questions.Where(q => q.Topics.Select(t=>t.Id).Contains(topicId))
                    .OrderByDescending(q => q.CreationDate)
                    .Skip(skip)
                    .Take(10).ToList();
        }

        public List<Question> GetTopicTopQuestion(int topicId, int skip)
        {
            var topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == topicId);
            if (topic == null)
            {
                Utility.SetRespond(HttpStatusCode.NotFound, "Topic not found");
                return null;

            }
            return _hozyDbContext.Questions.Where(q => q.Topics.Select(t => t.Id).Contains(topicId))
                    .OrderByDescending(q => q.Rank)
                    .Skip(skip)
                    .Take(10).ToList();
        }

        public List<Topic> GetHotTopic()
        {
            return _hozyDbContext.TopTopics.OrderByDescending(t => t.CreationDate).FirstOrDefault().Topics;
        }



        public TopicDto GetDetailTopic(int topicid)
        {
            Topic topic = new Topic();
            TopicDto topicDto = new TopicDto();
            topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == topicid);
            if (topic != null)
            {
                topicDto = ConvertTopicToDto(topic);
            }
            return topicDto;
        }

        public List<Topic> GetRandomTopics(int amount)
        {
            return _hozyDbContext.Topics.OrderBy(t => Guid.NewGuid()).Take(amount).ToList();
        }

        public Topic UpdateTopic(TopicDto topicDto,int id, Session currentSession) // topic id
        {
            try
            {
                var topic = _hozyDbContext.Topics.FirstOrDefault(t => t.Id == id);
                if (topic != null)
                {
                    if (currentSession.RightList.Exists(right=>right.RightName=="admin"))
                    {
                        topic.Name = topicDto.Name;
                        topic.Description = topicDto.Description;
                        topic.Image = topicDto.Image;
                        _hozyDbContext.Entry(topic).State = EntityState.Modified;
                        _hozyDbContext.SaveChanges();
                        return topic;
                    }
                    else
                    {
                        Utility.SetRespond(HttpStatusCode.Forbidden, "Not allowed");
                        return null;
                    }
                }
                else
                {
                    Utility.SetRespond(HttpStatusCode.NotFound, "Cant find the topic");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }
    }
}
