﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Hozy;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using Microsoft.AspNet.SignalR;

namespace HozyService.Handlers
{
    public class VoteHandler
    {
        private HozyDbContext _hozyDbContext;

        public VoteHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public VoteHandler(HozyDbContext hozyDbContext)
        {
            _hozyDbContext = hozyDbContext;
        }

        /// <summary>
        /// Xử lí khi user vote Question
        /// </summary>
        /// <param name="voteDto"></param>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        public bool VoteQuestion(VoteDto voteDto,Session currentSession)
        {
            
            try
            {
                var votingUser = _hozyDbContext.Users.Where(u => u.Id == currentSession.UserId).FirstOrDefault();
                var question = _hozyDbContext.Questions.Where(q => q.Id == voteDto.VoteTargetId).FirstOrDefault();
                Notification noti = new Notification();
                // Nếu không tìm được Question trong DB
                if (question == null)
                {
                    Utility.SetRespond(HttpStatusCode.InternalServerError, "Question Not Found");
                    return false;
                }
                var foundvote = question.Votes.Where(v => v.User == votingUser).FirstOrDefault();
                
                // Nếu [question + người vote] đã tồn tại, ta Update
                if (foundvote != null)
                {
                    //noti cho nhung user co question
                    foundvote.State = voteDto.VoteValue;

                    //noti.QuestionId = question.Id;
                    //noti.Description = foundvote.User.DisplayName + " " + Constant.VoteNoti + question.QuestionTitle ;
                    //noti.IsChecked = false;
                    //noti.IsVote = true;
                    //noti.UserId = votingUser.Id;
                    //noti.TargetUserId = question.User.Id;
                    //noti.CreationDate = DateTime.Now;
                    //_hozyDbContext.Notifications.Add(noti);
                    _hozyDbContext.SaveChanges();
                    //NotificationJobs(question, votingUser, noti);
                } 
                // Nếu chưa tồn tại, ta add thêm
                else
                {
                    Vote vote = new Vote();
                    vote.User = votingUser;
                    vote.State = voteDto.VoteValue;
                    question.Votes.Add(vote);

                    noti.QuestionId = question.Id;
                    noti.Description = votingUser.DisplayName + " " + Constant.VoteNoti + question.QuestionTitle;
                    noti.IsChecked = false;
                    noti.IsVote = true;
                    noti.UserId = votingUser.Id;
                    noti.TargetUserId = question.User.Id;
                    noti.CreationDate = DateTime.Now;
                    _hozyDbContext.Notifications.Add(noti);
                    _hozyDbContext.SaveChanges();
                    NotificationJobs(question, votingUser, noti);
                }

                
                return true;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
            
        }

        public void NotificationJobs(Question question, User votingUser, Notification noti)
        {
            var notificationhub = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            var us = _hozyDbContext.Users.FirstOrDefault(u => u.Id == question.User.Id);
            var notif = _hozyDbContext.Notifications.Where(n => n.QuestionId == question.Id && n.IsVote).OrderByDescending(n => n.CreationDate).FirstOrDefault();
            var time = (Int32)(noti.CreationDate.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0))).TotalSeconds;
            //long time = unixTimestamp;
            foreach (var connectionId in ConnectionMapping<string>.GetConnections(us.Id.ToString()))
            {
                if(notif.UserId != notif.TargetUserId){
                    notificationhub.Clients.Client(connectionId)
                        .notify(notif.Description, time, notif.QuestionId, notif.Id, votingUser.Avatar);
                }
            }
        }


        /// <summary>
        /// Xử lí khi user vote Answer
        /// </summary>
        /// <param name="voteDto"></param>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        public bool VoteAnswer(VoteDto voteDto,Session currentSession)
        {
            try
            {
                var votingUser = _hozyDbContext.Users.Where(u => u.Id == currentSession.UserId).FirstOrDefault();
                var answer = _hozyDbContext.Comments.Where(c => c.Id == voteDto.VoteTargetId).FirstOrDefault();

                // Nếu không tìm được answer trong DB
                if (answer == null)
                {
                    Utility.SetRespond(HttpStatusCode.InternalServerError, "Answer Not Found");
                    return false;
                }
                var foundvote = answer.Votes.Where(v => v.User == votingUser).FirstOrDefault();
                // Nếu [answer + người vote] đã tồn tại, ta Update
                if (foundvote != null)
                {
                    foundvote.State = voteDto.VoteValue;
                }
                // Nếu chưa tồn tại, ta add thêm
                else
                {
                    Vote vote = new Vote();
                    vote.User = votingUser;
                    vote.State = voteDto.VoteValue;
                    answer.Votes.Add(vote);
                }

                _hozyDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
        }
    }
}
