﻿using System;
using System.Activities.Statements;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.Handlers;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;

namespace Hozy
{
    public class NotificationHub : Hub
    {
        private static String token = null;
        private HozyDbContext hozy;
        private Session user = new Session();

        public NotificationHub(HozyDbContext dbContext)
        {
            hozy = dbContext;
        }

        public NotificationHub()
        {
            hozy = new HozyDbContext();
        }

        public override Task OnConnected()
        {
            Clients.Caller.requestcookie();
            return base.OnConnected();
        }

        /// <summary>
        /// trong bang comment 
        /// nhiem vu bay gio la phai tim nhung answer dc insert vao co questionid x 
        /// muon tim dc questionid x thi phai map giua questionid va userid x 
        /// userid x da biet 
        /// </summary>
        /// <param name="result"></param>
        // ReSharper disable once InconsistentNaming
        public void doprocesscookie(string result)
        {
            // sau khi lay duoc token thi phai cat' chuoi~ 
            //String[] parts = result.Split();
            //String firstcut = null;
            //if (parts[0].Length > 0)
            //{
            //    firstcut = parts[0].Substring(0, parts[0].Length - 1);
            //}
            //if (!string.IsNullOrEmpty(firstcut))
            //{
            //    token = firstcut.Substring(6);test11111
            //}
            var user = hozy.Sessions.FirstOrDefault(s => s.Token == result);
            if (user != null)
            {
                ConnectionMapping<string>.Add(user.UserId.ToString(), Context.ConnectionId);
            }
        }

        /// <summary>
        /// Ham nay co nhiem vu la mot user khac muon dang ki nhan thong bao tu` mot 
        /// Questionid = 1 co userid = 1 
        /// </summary>
        /// <param name="userToken"></param>
        /// <param name="questionid"></param>
        // ReSharper disable once InconsistentNaming
        public void userfollowq(string userToken, string questionid)
        {
            User user = new User();
            Question question = new Question
            {
                Id = Int32.Parse(questionid)
            };
            var session = hozy.Sessions.FirstOrDefault(s => s.Token == userToken);
            if (session != null)
            {
                try
                {
                    question.SubscribedUsers.Add(user);
                    hozy.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new WebFaultException(HttpStatusCode.Conflict);
                }
            }
        }

        /// <summary>
        /// Nhan ham tu frontend tra ra parameter IsCheckedNoti la true hay false 
        /// </summary>
        /// <returns></returns>
        public void getchecked(string token, string notificationid) 
        {
            var session = hozy.Sessions.FirstOrDefault(s => s.Token == token);
            if(session!=null)
            {
                int notiId = 0;
                notiId = Int32.Parse(notificationid);
                var notif = hozy.Notifications.FirstOrDefault(n => n.Id == notiId);
                if (notif != null) notif.IsChecked = true;
                try
                {
                    hozy.Entry(notif).State = EntityState.Modified;
                    hozy.SaveChanges();
                }
                catch (Exception)
                {
                    throw new DbUpdateConcurrencyException("Update Question to DB failed.");
                }
            }
        }


        public override Task OnDisconnected(bool stopCalled)
        {
            // Xoa ra khoi cap gia tri userid va connid vi conn da dong
            return base.OnDisconnected(stopCalled);
        }
    }
}
