﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebSockets;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;

namespace HozyService.Handlers
{
    public class SessionHandler
    {
        private HozyDbContext _hozyDbContext;

        public SessionHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public SessionHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        /// <summary>
        /// Kiểm tra xem User có nằm trong Database
        /// </summary>
        /// <param name="sessionRequestDto"></param>
        /// <returns></returns>
        public User AuthenUser(SessionRequestDto sessionRequestDto)
        {
            try
            {
                string hashedPassword = Utility.PasswordHash(sessionRequestDto.Password);
                User user = _hozyDbContext.Users.Where(u => u.Username == sessionRequestDto.Username && u.Password == hashedPassword).FirstOrDefault();
                if (user == null)
                {
                    Utility.Log4Net(0, "Wrong Username or Password");
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Invalid Username or Password");
                    return null;
                }
                else
                {
                    return user;
                }
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Internal Server Error!");
                throw new WebFaultException<string>("", HttpStatusCode.InternalServerError);
            }
        }

        public SessionDto OpenFacebookSession(string email)
        {
            var user = _hozyDbContext.Users.FirstOrDefault(u => u.Email.Equals(email));
            Session session = new Session();
            var exist = _hozyDbContext.Sessions.FirstOrDefault(s => s.Username == user.Username);
            if (exist == null)
            {
                session.Token = Guid.NewGuid().ToString();
                session.IpAddress = GetIpAddress();
                session.Username = user.Username;
                session.RightList = user.RightList;
                session.UserId = user.Id;
                try
                {
                    _hozyDbContext.Sessions.Add(session);
                    _hozyDbContext.SaveChanges();
                    Utility.Log4Net(1, "Session inserted successfully in database");
                }
                catch (Exception)
                {
                    Utility.Log4Net(0, "Session inserted FAIL");
                    throw new WebFaultException<string>("Internal Server Error", HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                session = exist;
            }

            return new SessionDto()
            {
                Username = session.Username,
                Token = session.Token,
                RightList = session.RightList.Select(x => x.RightName).ToList(),
                UserId = session.UserId.ToString()
            };
        }

        /// <summary>
        /// Lấy địa chỉ IP của Request đang tới Server
        /// </summary>
        /// <returns></returns>
        public static string GetIpAddress()
        {
            OperationContext context = OperationContext.Current;
            MessageProperties messageProperties = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpointProperty =
                messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            if (endpointProperty != null)
            {
                return endpointProperty.Address;
            }
            else
            {
                Utility.Log4Net(0, "ERROR");
                throw new Exception();
            }
        }

        /// <summary>
        /// Kiểm tra xem có session gắn liền với token này tồn tại trong DB hay không
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public Session AuthenSession(string token)
        {
            Session session = null;
            try
            {
                session = _hozyDbContext.Sessions.FirstOrDefault(s => s.Token.Equals(token));
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Internal Server Error");
                throw new WebFaultException<string>("Internal Server Error", HttpStatusCode.InternalServerError);
            }
            if (session == null)
            {
                Utility.Log4Net(0, "Invalid Session!!");
                throw new WebFaultException<string>("No session found", HttpStatusCode.Unauthorized);
            }
            else return session;
        }

        /// <summary>
        /// Tạo 1 object Session trong Database
        /// </summary>
        /// <param name="sessionRequestDto"></param>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public SessionDto OpenSession(SessionRequestDto sessionRequestDto, string ipAddress)
        {
            User user = AuthenUser(sessionRequestDto);
            //Session sessionExist;
            Session session = new Session();
            var exist = _hozyDbContext.Sessions.FirstOrDefault(s => s.Username == sessionRequestDto.Username);
                if (exist == null)
                {
                    session.Token = Guid.NewGuid().ToString();
                    session.IpAddress = GetIpAddress();
                    session.Username = user.Username;
                    session.RightList = user.RightList;
                    session.UserId = user.Id;
                    try
                    {
                        _hozyDbContext.Sessions.Add(session);
                        _hozyDbContext.SaveChanges();
                        Utility.Log4Net(1, "Session inserted successfully in database");
                    }
                    catch (Exception)
                    {
                        Utility.Log4Net(0, "Session inserted FAIL");
                        throw new WebFaultException<string>("Internal Server Error", HttpStatusCode.InternalServerError);
                    }
                }
                else
                {
                    session = exist;
                }
            return new SessionDto()
            {
                Username = session.Username,
                Token = session.Token,
                RightList = session.RightList.Select(x => x.RightName).ToList(),
                UserId = session.UserId.ToString()
            };
        }

        public bool DeleteSession(string token)
        {
            var session = _hozyDbContext.Sessions.FirstOrDefault(s => s.Token.Equals(token));
            if (session != null)
            {
                _hozyDbContext.Sessions.Remove(session);
                _hozyDbContext.SaveChanges();
                return true;
            }
            else
            {
                Utility.SetRespond(HttpStatusCode.NotFound, "Session not found");
                return false;
            }
        }
    }
}
