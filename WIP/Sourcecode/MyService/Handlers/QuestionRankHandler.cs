﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;

namespace HozyService.Handlers
{
    public class QuestionRankHandler
    {
        private static BlockingCollection<int> _questionQueue = new BlockingCollection<int>(new ConcurrentQueue<int>(), 10000); 
        
        private static double TimePoint(DateTime creationDateTime)
        {
            return Constant.MaxTimePoint < (DateTime.Now - creationDateTime).TotalHours
                ? 0
                : Constant.MaxTimePoint - (DateTime.Now - creationDateTime).TotalHours;
        }

        private static double InteractionTimePoint(DateTime lastInteraction)
        {
            return Constant.MaxInteractionTimePoint < (DateTime.Now - lastInteraction).TotalHours
                ? 0
                : Constant.MaxInteractionTimePoint - (DateTime.Now - lastInteraction).TotalHours; 
        }

        private static double AnswerCommentPoint(List<Comment> Answers)
        {
            int answerPoint = 0;
            foreach (var answer in Answers)
            {
                answerPoint += answer.Votes.Where(vote => vote.State).Count();
            }
            return Constant.MaxInteractionPoint > answerPoint ? answerPoint : Constant.MaxInteractionPoint;
        }

        private static double QuestionPoint(Question question)
        {
            var plusPoint = 2*question.Votes.Where(vote => vote.State).Count() - question.Votes.Count();
            foreach (var answer in question.CommentList)
            {
                plusPoint += 2* answer.Votes.Where(vote => vote.State).Count() - question.Votes.Count();
            }
            return Constant.MaxQuestionPoint > plusPoint ? plusPoint : Constant.MaxQuestionPoint;
        }

        private static int Rank(Question question)
        {
            var timePoint = TimePoint(question.CreationDate) * Constant.TimeModifier;
            var interactionTimePoint = InteractionTimePoint(question.LastInteraction) * Constant.InteractionTimeModifier;
            var answerCommentPoint = AnswerCommentPoint(question.CommentList) * Constant.InteractionModifier;
            var questionPoint = QuestionPoint(question) * Constant.QuestionModifier;
            Utility.Log4Net(1, "Return Rank Of Question");
            return (int)(timePoint + interactionTimePoint + answerCommentPoint + questionPoint);
        }

        private void FillQueueBackground()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(DoFill);
            worker.RunWorkerAsync();
        }

        /// <summary>
        /// Đưa vào Queue tối đa 1000 record mỗi 30 giây
        /// Các record được đưa vào là các record chưa được rank trong 15 phút gần nhất
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static async void DoFill(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int delay = Constant.QuestionFillDelay; 
            while (!worker.CancellationPending)
            {
                await Task.Delay(TimeSpan.FromMinutes(delay));
                HozyDbContext dbcontext = new HozyDbContext();
                List<int> currentToRank = dbcontext.Questions.Where(question => SqlFunctions.DateDiff("minute",question.LastRanked,DateTime.Now) > 15)
                    .Take(Constant.QuestionToFill) // Lấy ra 1000 Id record đầu tiên thỏa mãn
                    .Select(q => q.Id)
                    .ToList();
                if (currentToRank != null)
                {
                    foreach (int id in currentToRank) // Lần lượt add vào Queue 
                    {
                        _questionQueue.Add(id);
                    }
                }
            }
            e.Cancel = true;
        }

        private void TakeQueueBackground()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(DoTake);
            worker.RunWorkerAsync();
        }
        
        private static async void DoTake(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int delay = Constant.QuestionTakeDelay;
            while (!worker.CancellationPending)
            {
                await Task.Delay(TimeSpan.FromSeconds(delay));
                HozyDbContext dbcontext = new HozyDbContext();
                for (int i = 0; i < Constant.QuestionToTake; i++)
                {
                    if (_questionQueue.Count == 0)
                    {
                        break;
                    }
                    int questionId = _questionQueue.Take();
                    var question = dbcontext.Questions.Find(questionId);
                    question.Rank = Rank(question);
                    question.LastRanked = DateTime.Now;
                }
				Utility.Log4Net(1, "Calculate User's Feed");
                dbcontext.SaveChanges();
            }
            e.Cancel = true;
        }

        public void StartQuestionRanking()
        {
            FillQueueBackground();
            TakeQueueBackground();
        }

        public void LogTopQuestions()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(DoLogTopQuestion);
            worker.RunWorkerAsync();
        }

        private static async void DoLogTopQuestion(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int delay = Constant.TopQuestionLogDelay;
            while (!worker.CancellationPending)
            {
                await Task.Delay(TimeSpan.FromMinutes(delay));
                HozyDbContext dbcontext = new HozyDbContext();
                List<Question> topQuestions = dbcontext.Questions.OrderByDescending(question => question.Rank).Take(Constant.TopQuestionToLogNumber).ToList();
                int QuestionCount = dbcontext.Questions.Count();
                int CommentAnswerCount = dbcontext.Comments.Count();
                TopQuestion TopQuestionLog = new TopQuestion()
                {
                    CreationDate = DateTime.Now,
                    QuestionCountToDate = QuestionCount,
                    CommentAnswerCountToDate = CommentAnswerCount,
                    Questions = topQuestions
                };
                dbcontext.TopQuestions.Add(TopQuestionLog);
                dbcontext.SaveChanges();
                foreach (var topQuestion in topQuestions.Take(10))
                {
                    var processed = dbcontext.QuestionTextAnalytics.FirstOrDefault(p => p.Question.Id == topQuestion.Id);
                    if (processed==null)
                    {
                        AnalyticHandler analyticHandler = new AnalyticHandler();
                        var processedQuestion = analyticHandler.GetQuestionProcessed(topQuestion);
                        dbcontext.QuestionTextAnalytics.Add(processedQuestion);
                        dbcontext.SaveChanges();
                    }
                }
            }
            e.Cancel = true;
        }

    }
}
