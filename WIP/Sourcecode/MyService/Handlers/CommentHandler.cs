﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.DTOs;
using HozyDb.Utilities;

namespace HozyService.Handlers
{
    public class CommentHandler
    {
        private HozyDbContext _hozyDbContext;

        public CommentHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public CommentHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public CommentDto ConvertCommentToDto(Comment comment)
        {
            if (comment.isDeleted)
            {
                return null;
            }
            var commentDto = new CommentDto();
            commentDto.UserId = comment.User.Id;
            commentDto.UserName = comment.User.Username;
            commentDto.DisplayName = comment.User.DisplayName;
            commentDto.Avatar = comment.User.Avatar;
            commentDto.CommentContent = comment.Content;
            commentDto.CommentId = comment.Id;
            commentDto.CreationDate = comment.CreationDate.ToString("G");

            return commentDto;
        }

        public List<Comment> GetCommentList(int id)
        {
            try
            {
                Comment Answer = _hozyDbContext.Comments.FirstOrDefault(c => c.Id == id);
                return Answer.ChildComments;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
            
        }

        public Comment AddComment(CommentDto commentDto, Session session)
        {
            Comment comment = new Comment();
            Comment cmt = _hozyDbContext.Comments.Where(u => u.Id == commentDto.ParentCmtId).FirstOrDefault();
            User user = _hozyDbContext.Users.Where(u => u.Id == session.UserId).FirstOrDefault();
            comment.Content = commentDto.CommentContent;
            comment.ParentComment = cmt;
            comment.User = user;
            comment.CreationDate = DateTime.Now;
            try
            {
                _hozyDbContext.Comments.Add(comment);
                _hozyDbContext.SaveChanges();
                Utility.Log4Net(1, "Comment [Comment content: " + commentDto.CommentContent + ", Username: " + user.Username + " ] inserted successfully in database");
                return comment;
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Comment [Comment content: " + commentDto.CommentContent + ", Username: " + user.Username + " ] inserted FAIL!!!");
                throw new WebFaultException<string>("Can not add new commet", HttpStatusCode.Forbidden);
            }
            
        }

        public Comment UpdateComment(CommentDto commentDto, int id, Session session)
        {
            try
            {
                var comment = _hozyDbContext.Comments.FirstOrDefault(c => c.Id == id);

                if (comment != null)
                {
                    if (session.UserId == comment.User.Id || session.RightList.Exists(right => right.RightName=="admin"))
                    {
                        comment.Content = commentDto.CommentContent;
                        _hozyDbContext.Entry(comment).State = EntityState.Modified;
                        _hozyDbContext.SaveChanges();
                        Utility.Log4Net(1, "Update Comment [Comment Content: " + comment.Content + "] ==> [CommentContent: " + commentDto.CommentContent + "]");
                        return comment;
                    }
                    else
                    {
                        Utility.SetRespond(HttpStatusCode.Forbidden, "Not allowed");
                        return null;
                    }
                    
                }
                else
                {
                    Utility.SetRespond(HttpStatusCode.NotFound, "");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        public bool DeleteComment(int commentId,Session currentSession)
        {
            try
            {
                var comment = _hozyDbContext.Comments.Include(c => c.User).FirstOrDefault(c => c.Id == commentId);
                if (comment.User.Id == currentSession.UserId ||
                    currentSession.RightList.Exists(right => right.RightName.Equals("admin")))
                {
                    comment.isDeleted = true;
                    _hozyDbContext.SaveChanges();
                    return true;
                }
                else
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Forbidden");
                    return false;
                }
            }
            catch (Exception exception)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, exception.Message);
                return false;
            }
        }
    }
}
