﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using HozyDb.EfDbServices;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;
using System.Data.Entity;
using HozyDb.Entities;
using HozyDb.Utilities;

namespace HozyService.Handlers
{
    public class LuceneHandler
    {
        private HozyDbContext _hozyDbContext;

        public LuceneHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public LuceneHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public void CreateFullIndex()
        {
            var dir = FSDirectory.Open(new DirectoryInfo(@"C:/Lucene"));
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            var writer = new IndexWriter(dir, analyzer, IndexWriter.MaxFieldLength.UNLIMITED);
            writer.DeleteAll();
            foreach (var question in _hozyDbContext.Questions.Include(q=>q.CommentList))
            {
                var document = GetDocumentFromQuestion(question);
                writer.AddDocument(document);
                Debug.WriteLine("Document Added " + question.QuestionTitle);
            }
            writer.Optimize();
            writer.Commit();
            writer.Dispose();
        }

        public void UpdateQuestionIndex(Question question)
        {
            var dir = FSDirectory.Open(new DirectoryInfo(@"C:/Lucene"));
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            var writer = new IndexWriter(dir, analyzer, IndexWriter.MaxFieldLength.UNLIMITED);
            writer.UpdateDocument(new Term("QuestionId",question.Id.ToString()),GetDocumentFromQuestion(question));
            writer.Optimize();
            writer.Commit();
            writer.Dispose();
        }

        private Document GetDocumentFromQuestion(Question question)
        {
            string commentcontent = "";
            if (question.CommentList != null)
            {
                foreach (var comment in question.CommentList)
                {
                    commentcontent += comment.Content;
                    commentcontent += " ";
                }
            }
            var document = CreateDocument(question.Id.ToString(), question.QuestionTitle, question.QuestionContent, commentcontent);
            return document;
        }

        public Document CreateDocument(string questionId, string questionTitle,string questionContent,string comment)
        {
            var doc = new Document();
            doc.Add(new Field("QuestionId", questionId, Field.Store.YES, Field.Index.NOT_ANALYZED));
            var titleField = new Field("questionTitle", questionTitle, Field.Store.YES, Field.Index.ANALYZED);
            titleField.Boost = 3;
            doc.Add(titleField);
            var contentField = new Field("questionContent", questionContent, Field.Store.YES, Field.Index.ANALYZED);
            contentField.Boost = 2;
            doc.Add(contentField);
            var commentField = new Field("comment", comment, Field.Store.YES, Field.Index.ANALYZED);
            doc.Add(commentField);
            string unsigned = Utility.ConvertToUnSign(questionTitle + questionContent);
            var unsignedQuestionField = new Field("unsignedquestion", unsigned, Field.Store.YES, Field.Index.ANALYZED);
            unsignedQuestionField.Boost = 0.6f;
            doc.Add(unsignedQuestionField);
            var unsignedCommentField = new Field("unsignedcomment", unsigned, Field.Store.YES, Field.Index.ANALYZED);
            unsignedCommentField.Boost = 0.4f;
            doc.Add(unsignedCommentField);
            return doc;
        }
        
    }
}
