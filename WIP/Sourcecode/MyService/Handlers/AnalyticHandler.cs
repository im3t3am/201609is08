﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using Facebook;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;

namespace HozyService.Handlers
{
    public class AnalyticHandler
    {
        private HozyDbContext _hozyDbContext;

        public AnalyticHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public List<TopTopicLogDto> GetTopTopicLog(int amount, int skip,TopicHandler topicHandler)
        {
            var TopTopicLog = _hozyDbContext.TopTopics.OrderByDescending(topic => topic.CreationDate).Skip(skip).Take(amount).ToList();

            List<TopTopicLogDto> topTopicLogDtos = new List<TopTopicLogDto>();

            foreach (var topTopic in TopTopicLog)
            {
                var topTopicLogDto = new TopTopicLogDto();
                topTopicLogDto.TopTopic = new List<TopicDto>();
                topTopicLogDto.CreationDate = topTopic.CreationDate.ToString("G");
                foreach (var topic in topTopic.Topics)
                {
                    TopicDto topicDto = topicHandler.ConvertTopicToDto(topic);
                    topTopicLogDto.TopTopic.Add(topicDto);
                }
                topTopicLogDtos.Add(topTopicLogDto);
            }
            return topTopicLogDtos;
        }

        public List<TopQuestion> GetTopQuestionLog(int amount, int skip)
        {
            var TopQuestion =
                _hozyDbContext.TopQuestions.OrderByDescending(question => question.CreationDate)
                    .Skip(skip)
                    .Take(amount)
                    .ToList();
            return TopQuestion;
        }

        public TopQuestionLogDto ConvertTopQuestionToDtos(TopQuestion topQuestion, QuestionHandler questionHandler)
        {
            try
            {
                TopQuestionLogDto topQuestionLogDto = new TopQuestionLogDto();
                topQuestionLogDto.CreationDate = topQuestion.CreationDate.ToString("G");
                topQuestionLogDto.QuestionCountToDate = topQuestion.QuestionCountToDate;
                topQuestionLogDto.CommentAnswerCountToDate = topQuestion.CommentAnswerCountToDate;
                topQuestionLogDto.topQuestionDtos = new List<TopQuestionDto>();
                foreach (var question in topQuestion.Questions.OrderByDescending(question => question.Rank))
                {
                    
                    topQuestionLogDto.topQuestionDtos.Add(new TopQuestionDto()
                    {
                        QuestionDto = questionHandler.ConvertQuestionToDto(question, new TopicHandler(),new AnswerHandler()),
                        TextAnalyticDto = GetTextAnalyticDto(question)
                    });
                }
                return topQuestionLogDto;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private TextAnalyticDto GetTextAnalyticDto(Question question)
        {
            try
            {
                TextAnalyticDto textAnalyticDto = new TextAnalyticDto();
                var analytic = _hozyDbContext.QuestionTextAnalytics.FirstOrDefault(q => q.Question.Id == question.Id);
                if (analytic != null)
                {
                    textAnalyticDto.Agreement = analytic.Agreement;
                    textAnalyticDto.Subjectivity = analytic.Subjectivity;
                    textAnalyticDto.Sentiment = analytic.Sentiment;
                }
                return textAnalyticDto;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private string GetQuestionInEnglish(Question question)
        {
            string content ="";
            content += GetEnglishTranslation(question.QuestionTitle + " " + question.QuestionContent);
            if (question.CommentList != null)
            {
                foreach (var comment in question.CommentList)
                {
                    content += GetEnglishTranslation(" " + comment.Content);
                }
            }
            
            return content;;
        }

        public QuestionTextAnalytic GetQuestionProcessed(Question question)
        {
            try
            {
                string text = GetQuestionInEnglish(question);
                QuestionTextAnalytic processedQuestion = new QuestionTextAnalytic();
                string processedText = ProcessText(text);
                dynamic json = Json.Decode(processedText);
                processedQuestion.Sentiment = json.score_tag;
                processedQuestion.Agreement = json.agreement;
                processedQuestion.Subjectivity = json.subjectivity;
                processedQuestion.CreationDate = DateTime.Now;
                processedQuestion.Question = question;
                return processedQuestion;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string GetEnglishTranslation(string viText)
        {
            try
            {
                WebRequest request = WebRequest.Create("https://www.googleapis.com/language/translate/v2?key=" + Constant.GoogleKey + "&source=vi&target=en");
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Headers["X-HTTP-Method-Override"] = "GET";
                string content = HttpUtility.UrlEncode(viText);
                byte[] data = Encoding.UTF8.GetBytes("q=" + content);

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                dynamic json = Json.Decode(responseString);
                string result = json.data.translations[0].translatedText;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
           
        }

        private string ProcessText(string text)
        {
            WebRequest request = WebRequest.Create("https://api.meaningcloud.com/sentiment-2.1");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers["X-HTTP-Method-Override"] = "GET";
            string content = "lang=en&key=" + Constant.MeaningCloudKey+"&txt="+text;
            byte[] data = Encoding.UTF8.GetBytes(content);

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }

    }
}
