﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;

namespace HozyService.Handlers
{
    public class TopicRankHandler
    {
        public void StartTopicRanking()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(RankTopic);
            worker.RunWorkerAsync();
        }
        private static async void RankTopic(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int delay = Constant.TopicRankDelay;
            while (!worker.CancellationPending)
            {
                Dictionary<Topic,int> TopicDictionary = new Dictionary<Topic, int>();
                await Task.Delay(TimeSpan.FromMinutes(delay));
                HozyDbContext dbcontext = new HozyDbContext();
                var Topics =
                    dbcontext.Topics.Where(
                        topic => SqlFunctions.DateDiff("hour", topic.LastQuestionDate, DateTime.Now) < Constant.NewTopicThreshold).ToList();
                foreach (var topic in Topics)
                {
                    TopicDictionary.Add(topic, RankSingleTopic(topic));
                }
                var myList = TopicDictionary.ToList();
                myList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
                var RankedTopics = myList.OrderByDescending(p=>p.Value).Select(pair => pair.Key).Take(5).ToList();

                TopTopic topTopic = new TopTopic();
                topTopic.CreationDate = DateTime.Now;
                topTopic.Topics = RankedTopics;

                dbcontext.TopTopics.Add(topTopic);
                dbcontext.SaveChanges();
            }
            e.Cancel = true;
        }
        private static int RankSingleTopic(Topic topic)
        {
            HozyDbContext dbContext = new HozyDbContext();
            int NewQuestionCount = dbContext.Questions
                .Where(question => SqlFunctions.DateDiff("hour", question.CreationDate, DateTime.Now) < Constant.NewTopicThreshold)
                .Count(question => question.Topics.Select(t => t.Id).Contains(topic.Id));
            int NewQuestionPoint = NewQuestionCount*Constant.TopicRankNewQuestionModifier;

            //Debug.WriteLine("Point of " + topic.Name + " From New : " + NewQuestionPoint);

            var RecentQuestions = dbContext.Questions
                .Where(
                    question =>
                        SqlFunctions.DateDiff("day", question.CreationDate, DateTime.Now) <
                        Constant.RecentQuestionInTopicThreshold)
                .Where(question => question.Topics.Select(t => t.Id).Contains(topic.Id)).ToList();
            int RecentQuestionPoint = RecentQuestions.Sum(question => question.Rank);

            //Debug.WriteLine("Point of " + topic.Name + " From Questions : " + RecentQuestionPoint);
            int x = NewQuestionPoint + RecentQuestionPoint;
            Debug.WriteLine("Point of " + topic.Name + " is " + x);
            return NewQuestionPoint + RecentQuestionPoint;
        }
    }
}
