﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;

namespace HozyService.Handlers
{
    public class ReportHandler
    {
        private HozyDbContext _hozyDbContext;

        public ReportHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public ReportHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public ReportDto ConvertReportToDto(Report report)
        {
            ReportDto reportDto = new ReportDto();
            reportDto.Id = report.Id;
            reportDto.ReportContent = report.ReportContent;
            reportDto.CreationDate = report.CreationDate.ToString("G");
            if (report.User != null)
            {
                reportDto.User = new UserDto
                {
                    Id = report.User.Id,
                    Username = report.User.Username,
                    DisplayName = report.User.DisplayName,
                    Avatar = report.User.Avatar
                };
            }
            return reportDto;
        }
        
        public ReportDto AddReport(ReportCreateDto reportCreateDto, Session currentSession)
        {
            Report report = new Report();
            report.ReportContent = reportCreateDto.ReportContent;
            report.CreationDate = DateTime.Now;

            Question question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == reportCreateDto.QuestionId);
            report.Question = question;
            question.IsReported = true;
            User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == currentSession.UserId);
            report.User = user;
            try
            {
                _hozyDbContext.Reports.Add(report);
                _hozyDbContext.Entry(question).State = EntityState.Modified;
                _hozyDbContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                
                throw new DbUpdateConcurrencyException("Add report to DB is failed");
            }
            return new ReportDto()
            {
                ReportContent = report.ReportContent,
                Id = report.Id,
                
            };
        }

        public List<ReportDto> GetReport(int questionid, Session currentSession)
        {
            List<ReportDto> listReport = new List<ReportDto>();
            Question question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionid);
            foreach (var r in _hozyDbContext.Reports.Where(r => r.Question.Id == question.Id).OrderByDescending(r => r.CreationDate).ToList())
            {
                ReportDto reportDto = ConvertReportToDto(r);
                listReport.Add(reportDto);

            }
            return listReport;
        }
    }
}
