﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;

namespace HozyService.Handlers
{
    public class AmaHandler
    {
        private HozyDbContext _hozyDbContext;

        public AmaHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public AmaSession CreateAma(AmaCreationDto amaCreationDto,Session currentSession)
        {
            try
            {
                if (currentSession.RightList.Exists(r => r.RightName == "admin"))
                {
                    DateTime StartDate = Utility.UnixTimeStampToDateTime(double.Parse(amaCreationDto.UnixDateTime));
                    if (StartDate.Date < DateTime.Today)
                    {
                        Utility.SetRespond(HttpStatusCode.BadRequest, "Invalid Date");
                        return null;
                    }
                    var amaOwner = _hozyDbContext.Users.FirstOrDefault(u => u.Id == amaCreationDto.Userid);
                    if (amaOwner == null)
                    {
                        Utility.SetRespond(HttpStatusCode.NotFound, "User Not Found");
                        return null;
                    }
                    AmaSession amaSession = new AmaSession();
                    amaSession.AmaOwner = amaOwner;
                    amaSession.CreationDate = DateTime.Now;
                    amaSession.OpenDate = StartDate.Date;
                    amaSession.AmaImage = amaCreationDto.AmaImage;    
                    _hozyDbContext.AmaSessions.Add(amaSession);
                    _hozyDbContext.SaveChanges();
                    return amaSession;
                }
                else
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Forbidden");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
            
        }

        public AmaSession GetAma(int id)
        {
            var amaSession = _hozyDbContext.AmaSessions.Find(id);
            if (amaSession == null)
            {
                Utility.SetRespond(HttpStatusCode.NotFound, "Ama Session Not Found");
                return null;
            }
            return amaSession;
        }

        public AmaDto ConnvertAmaToDto(AmaSession amaSession, UserHandler userHandler)
        {
            AmaDto amaDto = new AmaDto();
            amaDto.Id = amaSession.Id;
            amaDto.AmaOwner = userHandler.ConvertUserSimpleDto(amaSession.AmaOwner);
            amaDto.AmaQuestionDtos = new List<AmaQuestionDto>();
            amaDto.OpenDate = amaSession.OpenDate.ToString("d");
            amaDto.AmaImage = amaSession.AmaImage;
            foreach (var amaQuestion in amaSession.AmaQuestions)
            {
                amaDto.AmaQuestionDtos.Add(ConvertToAmaQuestionDto(amaQuestion,userHandler));
            }
            return amaDto;
        }

        public AmaQuestionDto ConvertToAmaQuestionDto(AmaQuestion amaQuestion, UserHandler userHandler)
        {
            try
            {
                AmaQuestionDto amaQuestionDto = new AmaQuestionDto();
                amaQuestionDto.Id = amaQuestion.Id;
                amaQuestionDto.AskerDto = userHandler.ConvertUserSimpleDto(amaQuestion.AskUser);
                amaQuestionDto.QuestionContent = amaQuestion.Content;
                amaQuestionDto.QuestionCreationDate = amaQuestion.CreationDate.ToString("G");
                if (amaQuestion.AmaAnswer != null)
                {
                    amaQuestionDto.AnswerId = amaQuestion.AmaAnswer.Id;
                    amaQuestionDto.AnswerContent = amaQuestion.AmaAnswer.Content;
                    amaQuestionDto.AnswerCreationDate = amaQuestion.AmaAnswer.CreationDate.ToString("G");
                }
                
                return amaQuestionDto;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        public AmaQuestion AddAmaQuestion(AmaQuestionDto amaQuestionDto,Session currentSession)
        {
            try
            {
                AmaSession amaSession = _hozyDbContext.AmaSessions.Find(amaQuestionDto.SessionId);
                if (amaSession == null)
                {
                    Utility.SetRespond(HttpStatusCode.NotFound, "Ama Session Not Found");
                    return null;
                }
                if (amaSession.OpenDate != DateTime.Today)
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Closed");
                    return null;
                }

                AmaQuestion amaQuestion = new AmaQuestion();
                amaQuestion.AskUser = _hozyDbContext.Users.Find(currentSession.UserId);
                amaQuestion.Content = amaQuestionDto.QuestionContent;
                amaQuestion.AmaSession = _hozyDbContext.AmaSessions.Find(amaQuestionDto.SessionId);
                amaQuestion.CreationDate = DateTime.Now;
                _hozyDbContext.AmaQuestions.Add(amaQuestion);
                _hozyDbContext.SaveChanges();
                return amaQuestion;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        public AmaQuestion UpdateAmaQuestion(int questionId, AmaQuestionDto amaQuestionDto, Session currentSession)
        {
            try
            {
                AmaQuestion amaQuestion =
                    _hozyDbContext.AmaQuestions.Include(question => question.AmaSession)
                        .FirstOrDefault(question => question.Id == questionId);
                if (amaQuestion == null)
                {
                    Utility.SetRespond(HttpStatusCode.NotFound, "Question Not Found");
                    return null;
                }
                if (amaQuestion.AskUser.Id != currentSession.UserId)
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Not Allowed");
                    return null;
                }
                if (amaQuestion.AmaSession.OpenDate != DateTime.Today)
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Closed");
                    return null;
                }
                amaQuestion.Content = amaQuestionDto.QuestionContent;
                _hozyDbContext.Entry(amaQuestion).State = EntityState.Modified;
                _hozyDbContext.SaveChanges();
                return amaQuestion;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return null;
            }

        }

        public AmaAnswer AddAmaAnswer(AmaAnswerCreationDto amaAnswerCreationDto, Session currentSession)
        {
            try
            {
                var amaQuestion = _hozyDbContext.AmaQuestions.Find(amaAnswerCreationDto.AmaQuestionId);
                if (amaQuestion == null)
                {
                    Utility.SetRespond(HttpStatusCode.NotFound, "Ama Question Not Found");
                    return null;
                }
                if(amaQuestion.AmaSession.AmaOwner.Id!= currentSession.UserId)
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Not Ama Owner");
                    return null;
                }
                if (amaQuestion.AmaSession.OpenDate != DateTime.Today)
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Closed");
                    return null;
                }
                AmaAnswer amaAnswer = new AmaAnswer();
                amaAnswer.CreationDate = DateTime.Now;
                amaAnswer.Content = amaAnswerCreationDto.AnswerContent;

                amaQuestion.AmaAnswer = amaAnswer;
                _hozyDbContext.SaveChanges();
                return amaQuestion.AmaAnswer;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        public List<AmaSession> ListAmaSessions(int skip)
        {
            return _hozyDbContext.AmaSessions.OrderByDescending(session => session.OpenDate).Skip(skip).Take(10).ToList();
        }
    }
}
