﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using Lucene.Net.Support;

namespace HozyService.Handlers
{
    public class QuestionHandler
    {
        private HozyDbContext _hozyDbContext;

        public bool DeleteQuestion(int questionId, Session session)
        {
            var question = _hozyDbContext.Questions.Find(questionId);
            if (question == null)
            {
                Utility.SetRespond(HttpStatusCode.NotFound, "Question Not Found");
            }
            if (session.RightList.Exists(right => right.RightName.Equals("admin")) || session.UserId == question.User.Id)
            {
                question.isDeleted = true;
                _hozyDbContext.SaveChanges();
                return true;
            }
            else
            {
                Utility.SetRespond(HttpStatusCode.Forbidden, "Not Allowed");
                return false;
            }
        }

        public QuestionHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public QuestionHandler(HozyDbContext hozyDbContext)
        {
            _hozyDbContext = hozyDbContext;
        }

        public QuestionDto ConvertQuestionToDto(Question question, TopicHandler topicHandler, AnswerHandler answerHandler)
        {
            QuestionDto questionDto = new QuestionDto();
            if (question.isDeleted)
            {
                questionDto.isDeleted = true;
                return questionDto;
            }
            questionDto.Id = question.Id;
            questionDto.QuestionTitle = question.QuestionTitle;
            questionDto.QuestionContent = question.QuestionContent;
            questionDto.CreationDate = question.CreationDate.ToString("G");
            var votes = question.Votes.ToList();
            questionDto.VoteCountUp = votes.Count(vote => vote.State);
            questionDto.VoteCountDown = votes.Count() - questionDto.VoteCountUp;
            if (question.User != null)
            {
                questionDto.User = new UserDto
                {
                    Id = question.User.Id,
                    Username = question.User.Username,
                    DisplayName = question.User.DisplayName,
                    Avatar = question.User.Avatar
                };
            }
            questionDto.Topics = new List<TopicDto>();
            foreach (var topic in question.Topics)
            {
                TopicDto topicDto = new TopicDto();
                topicDto = topicHandler.ConvertTopicToDto(topic);
                questionDto.Topics.Add(topicDto);
            }
            questionDto.ViewCount = question.ViewCount;
            questionDto.FollowCount = question.SubscribedUsers.Count;
            questionDto.BestAnswer = new AnswerDto();
            if (question.CommentList != null)
            {
                Comment c = question.CommentList.OrderByDescending(
                    comment => comment.Votes.Count(v => v.State) - comment.Votes.Count(v => !v.State)).FirstOrDefault();
                questionDto.BestAnswer = answerHandler.ConvertAnswerToDto(c);
            }
            IncreaseViewCount(question.Id);
            return questionDto;
        }

        public bool IncreaseViewCount(int questionid)
        {
            var question = _hozyDbContext.Questions.Find(questionid);
            if (question != null)
            {
                question.ViewCount += 1;
                _hozyDbContext.Entry(question).State = EntityState.Modified;
                _hozyDbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public List<Question> GetRelatedQuestions(int id)
        {
            Question q = _hozyDbContext.Questions.Find(id);
            if (q == null)
            {
                return null;
            }
            List<Question> relatedQuestions = new List<Question>();
            if (q.Topics == null)
            {
                return null;
            }
            foreach (var topic in q.Topics)
            {
                var relates = (_hozyDbContext.Questions.OrderByDescending(question => question.Rank)
                    .Where(x => x.Topics.Select(t=>t.Id).Contains(topic.Id)).Take(5));
                foreach (var relatedQuestion in relates)
                {
                    if (!relatedQuestions.Contains(relatedQuestion) && relatedQuestion != null && relatedQuestion.Id != id)
                    {
                        relatedQuestions.Add(relatedQuestion);
                    }
                }
            }
            return relatedQuestions.OrderByDescending(r=>r.Rank).Take(5).ToList();
        }

        //request question from DB
        public QuestionDto GetQuestion(int id)
        {
            var question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == id);
            var questionDto = new QuestionDto();
            questionDto = ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
            Utility.Log4Net(1, "Get question [Question title: " + question.QuestionTitle + "]");
            return questionDto;
        }

        /// <summary>
        /// Hỏi một câu hỏi từ một UserID
        /// </summary>
        /// <param name="questionDto"></param>
        /// <returns></returns>
        public QuestionDto AddQuestion(QuestionDto questionDto, Session currentSession)
        {
            Question question = new Question();
            question.QuestionTitle = questionDto.QuestionTitle;
            question.QuestionContent = questionDto.QuestionContent;
            User user = _hozyDbContext.Users.Where(u => u.Id == currentSession.UserId).FirstOrDefault();
            question.CreationDate = DateTime.Now;
            question.LastRanked = DateTime.Now.AddMinutes(-15);
            question.User = user;
            try
            {
                _hozyDbContext.Questions.Add(question);
                _hozyDbContext.SaveChanges();
                LuceneHandler luceneHandler = new LuceneHandler();
                luceneHandler.UpdateQuestionIndex(question);
                Utility.Log4Net(1, "Question [Question title: " + questionDto.QuestionTitle + ", Question content: " + questionDto.QuestionContent + " ] inserted successfully in database");
            }
            catch (DbUpdateConcurrencyException)
            {
                Utility.Log4Net(0, "Question [Question title: " + questionDto.QuestionTitle + ", Question content: " + questionDto.QuestionContent + " ] inserted FAIL");
                throw new DbUpdateConcurrencyException("Add Question to DB failed.");
            }
            return new QuestionDto()
            {
                QuestionTitle = question.QuestionTitle,
                QuestionContent = question.QuestionContent,
                Id = question.Id
            };
        }

        /// <summary>
        /// Update a question with QuestionId
        /// </summary>
        /// <param name="questionDto"></param>
        /// <param name="currentSession"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UpdateQuestion(QuestionDto questionDto, int id, Session currentSession)
        {
            User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == currentSession.UserId);
            var question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == id);
            if (question == null)
            {
                Utility.SetRespond(HttpStatusCode.NotFound, "Question Not Found");
                return false;
            }
            if (currentSession.UserId == question.User.Id || currentSession.RightList.Exists(right => right.RightName=="admin"))
            {
                if (questionDto.QuestionTitle != null)
                {
                    question.QuestionTitle = questionDto.QuestionTitle;
                }
                question.QuestionContent = questionDto.QuestionContent;
                try
                {
                    _hozyDbContext.Entry(question).State = EntityState.Modified;
                    _hozyDbContext.SaveChanges();
                    LuceneHandler luceneHandler = new LuceneHandler();
                    luceneHandler.UpdateQuestionIndex(question);
                    Utility.Log4Net(1,
                        "Update Question [Question title: " + question.QuestionTitle + ", Question content: " +
                        question.QuestionContent + " ] ==> [Question title: " + questionDto.QuestionTitle +
                        ", Question content: " + questionDto.QuestionContent + " ]");
                }
                catch (Exception)
                {
                    Utility.Log4Net(0,
                        "Update Question [Question title: " + question.QuestionTitle + ", Question content: " +
                        question.QuestionContent + " ] ==> [Question title: " + questionDto.QuestionTitle +
                        ", Question content: " + questionDto.QuestionContent + " ] FAIL");
                    throw new DbUpdateConcurrencyException("Update Question to DB failed.");
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Lấy ra top Questions, 30 cái 1 lần, bắt đầu từ index của skip
        /// </summary>
        /// <param name="skip"></param>
        /// <returns></returns>
        public List<QuestionDto> GetTopQuestion(int skip)
        {
            List<QuestionDto> topQuestions = new List<QuestionDto>();
            foreach (var question in _hozyDbContext.Questions.OrderByDescending(q => q.Rank).Skip(skip).Take(10).ToList())
            {
                QuestionDto questionDto = ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
                topQuestions.Add(questionDto);
            }
            Utility.Log4Net(1, "Get Top Questions");
            return topQuestions;
        }

        public List<QuestionDto> GetNewQuestion(int skip)
        {
            List<QuestionDto> newQuestions = new List<QuestionDto>();
            foreach (var question in _hozyDbContext.Questions.OrderByDescending(q => q.CreationDate).Skip(skip).Take(10).ToList())
            {
                QuestionDto questionDto = ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
                newQuestions.Add(questionDto);
            }
            Utility.Log4Net(1, "Get New Questions");
            return newQuestions;
        }

        public bool AddQuestionSubscribe(int questionid, Session currentSession)
        {
            User user = _hozyDbContext.Users.Find(currentSession.UserId);
            Question question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionid);
            try
            {
                if (user != null && question != null) question.SubscribedUsers.Add(user);
                _hozyDbContext.SaveChanges();
                Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] add a question [" + questionid + "] in subscription list");
            }
            catch (Exception ex)
            {
                Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] add a question [" + questionid + "] in subscription list FAIL");
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
            return true;
        }

        public bool RemoveQuestionSubscribe(int questionid, Session currentSession)
        {

            User user = _hozyDbContext.Users.Find(currentSession.UserId);
            Question question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionid);
            try
            {
                if (user != null && question != null && question.SubscribedUsers.Contains(user)) question.SubscribedUsers.Remove(user);
                _hozyDbContext.SaveChanges();
                Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] remove a question [Question ID: " + questionid + "] in subscription list");
            }
            catch (Exception ex)
            {
                Utility.Log4Net(1, "User [User ID: " + currentSession.Id + "] remove a question [Question ID: " + questionid + "] in subscription list FAIL");
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
            return true;
        }

         public List<Question> GetQuestionReported(Session currentSession)
        {
             List<Question> newQuestion =
                 _hozyDbContext.Questions.OrderByDescending(q => q.CreationDate)
                 .Where(question => question.IsReported == true)
                 .ToList();
            return newQuestion;
        }

        public List<QuestionDto> ListQuestionFollowed(Session currentSession)
        {
            var session = _hozyDbContext.Sessions.FirstOrDefault(s => s.Token == currentSession.Token);
            //lay tat ca question cua 1 thang
            var questionList = _hozyDbContext
                .Questions.Where(q => q
                    .SubscribedUsers.Select(u => u.Id).Contains(session.UserId)).ToList();
            return (from cursor in questionList let questionDto = new QuestionDto() select ConvertQuestionToDto(cursor, new TopicHandler(), new AnswerHandler())).ToList();
        }

        public List<UserDto> ListUserFollowed(int qid) 
        {   
            var question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == qid);
            var userList = question.SubscribedUsers;
            List<UserDto> userDtoList = new List<UserDto>(); 
            foreach (var user in userList)
            {
                var userDto = new UserDto();
                userDto.Id = user.Id;
                userDto.Avatar = user.Avatar;
                userDto.DisplayName = user.DisplayName;
                userDto.Employment = user.Employment;
                userDto.Education = user.Education;
                userDtoList.Add(userDto);
            }   
            return userDtoList;
        }

        /// <summary>
        /// User is following question or not ( except user own that question
        /// </summary>
        /// <param name="questionid"></param>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        public bool IsFollowing(int questionid, Session currentSession)
        {
            var question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionid);
            var result = new User();
            if (question != null)
            {
                result = question.SubscribedUsers.FirstOrDefault(user => user.Id == currentSession.UserId);
                //Select(s => session != null && s.Id == session.UserId);
            }
            return result != null;
        }
        public bool IsReported(int questionid, Session currentSession)
        {
            var question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionid);
            var result = new Report();
            if (question != null)
            {
                result = question.Reports.FirstOrDefault(u => u.User.Id == currentSession.UserId);
            }

            return result != null;
        }
    }
}
