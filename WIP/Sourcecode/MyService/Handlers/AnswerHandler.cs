﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Hozy;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using Microsoft.AspNet.SignalR;

namespace HozyService.Handlers
{
    public class AnswerHandler
    {
        private HozyDbContext _hozyDbContext;

        public AnswerHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public AnswerHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        public AnswerDto ConvertAnswerToDto(Comment answer)
        {
            if (answer == null)
            {
                return null;
            }
            if (answer.isDeleted)
            {
                return null;
            }
            AnswerDto answerDto = new AnswerDto();
            answerDto.CommentId = answer.Id;
            answerDto.CommentContent = answer.Content;
            answerDto.CreationDate = answer.CreationDate.ToString("G");
            answerDto.UserName = answer.User.Username;
            answerDto.DisplayName = answer.User.DisplayName;
            answerDto.Avatar = answer.User.Avatar;
            answerDto.UserId = answer.User.Id;
            if (answer.Votes != null)
            {
                var Votes = answer.Votes.ToList();
                answerDto.VoteCountUp = Votes.Count(vote => vote.State);
                answerDto.VoteCountDown = Votes.Count() - answerDto.VoteCountUp;
            }
            answerDto.QuestionSimple = new QuestionSimpleDto();
            answerDto.QuestionSimple.QuestionTitle = answer.Question.QuestionTitle;
            answerDto.QuestionSimple.Id = answer.Question.Id;
            answerDto.isHelpful = answer.MarkedHelpful;
            return answerDto;
        }

        /// <summary>
        /// Lấy ra một list Answer với dữ liệu đầu vào là QuestionId
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public List<Comment> GetAnswer(int questionId) //questionid
        {
            Question question = _hozyDbContext.Questions.FirstOrDefault(q => q.Id == questionId);
            if (question == null)
            {
                Utility.SetRespond(HttpStatusCode.NotFound, "Question not found");
                return null;
            }
            List<Comment> AnswerList = question.CommentList;
            Utility.Log4Net(1, "Get answer list of [Question Title: " + question.QuestionTitle + " ]");
            return AnswerList;
        }

        /// <summary>
        /// Thêm một answer vào bảng Comment. Đầu vào là Commentcontent, Userid & Questionid
        /// </summary>
        /// <param name="answerCreateDto"></param>
        /// <returns></returns>
        public AnswerDto AddAnswer(AnswerCreateDto answerCreateDto, Session session)
        {
            Comment answer = new Comment();
            var noti = new Notification();
            answer.Content = answerCreateDto.CommentContent;
            answer.CreationDate = DateTime.Now;
            Question q = _hozyDbContext.Questions.Where(x => x.Id == answerCreateDto.QuestionId).FirstOrDefault();
            answer.Question = q;
            User user = _hozyDbContext.Users.Where(u => u.Id == session.UserId).FirstOrDefault();
            answer.User = user;
            q.LastInteraction = DateTime.Now;
            try
            {
                _hozyDbContext.Comments.Add(answer);
                _hozyDbContext.SaveChanges();
                LuceneHandler luceneHandler = new LuceneHandler();
                luceneHandler.UpdateQuestionIndex(q);
                Utility.Log4Net(1, "Answer [Answer content: " + answer.Content + ", Username: " + answer.User.Username + " ] inserted successfully in database");
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Answer [Answer content: " + answerCreateDto.CommentContent + ", UseriD: " + session.UserId + "] inserted FAIL!!!");
                throw new WebFaultException<string>("", HttpStatusCode.Forbidden);
            }
            AnswerDto answerDto = new AnswerDto();
            answerDto = ConvertAnswerToDto(answer);

            //Them 1 truong` Link cua question o trong AnswerCreateDto
            //them moi mot noti
            //if(q.User.Id != answer.User.Id){
                noti.CommentId = answerDto.CommentId;
                noti.QuestionId = answerDto.QuestionSimple.Id;
                noti.Description = answerDto.DisplayName + " " + Constant.AnswerNoti + answer.Question.QuestionTitle;
                noti.IsChecked = false;
                noti.IsVote = false;
                noti.UserId = answerDto.UserId;
                noti.TargetUserId = q.User.Id;
                noti.CreationDate = DateTime.Now;
                _hozyDbContext.Notifications.Add(noti);
                _hozyDbContext.SaveChanges();
                NotificationJobs(q, answerDto, noti);
            //}        
            Utility.Log4Net(1, "Notification inserted successfully in database!!!");
            
            AddUserFollowQ(q, answerDto);
            return answerDto;
        }

        //Tu dong them 1 user khi answer vao 1 cau hoi vao bang QuestionSubcription
        public void AddUserFollowQ(Question q, AnswerDto answer)
        {
            var user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == answer.UserId);      
            try
            {
                q.SubscribedUsers.Add(user);
                _hozyDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new WebFaultException(HttpStatusCode.InternalServerError);
            }
        }

        public void NotificationJobs(Question q, AnswerDto answerDto, Notification noti) 
        { 
            // thong bao đến cho User có questionId là q.Id
            var notificationhub = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            var us = _hozyDbContext.Users.FirstOrDefault(u => u.Id == q.User.Id);
            var notif = _hozyDbContext.Notifications.FirstOrDefault(n => n.CommentId == answerDto.CommentId);
            //lay ra avatar cua thang answer vao question
            var ans = _hozyDbContext.Comments.FirstOrDefault(c => c.Id == noti.CommentId);

            //foreach (var key in ConnectionMapping<string>._connections.Keys)
            //{
            //    Debug.WriteLine(ConnectionMapping<string>.GetConnections(key).ToString());
            //}

            var qe = _hozyDbContext.Questions.FirstOrDefault(qes => qes.Id == notif.QuestionId);

            //Notify cho nhung thang so huu cai Question khi co mot Answer moi
            //foreach (var connectionId in ConnectionMapping<string>.GetConnections(us.Id.ToString()))
            //{
            //    notificationhub.Clients.Client(connectionId).notify(notif.Description, time, notif.QuestionId, notif.Id, ans.User.Avatar);
            //}
            var user = _hozyDbContext.Notifications.FirstOrDefault(n => n.TargetUserId == q.User.Id);
            var time = (int)(noti.CreationDate.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0))).TotalSeconds;
            // time = noti.CreationDate.Ticks;
            //time = unixTimestamp;
            foreach (var connectionId in ConnectionMapping<string>.GetConnections(us.Id.ToString(user.TargetUserId.ToString())))
            {
                if(q.User.Id != ans.User.Id){
                    notificationhub.Clients.Client(connectionId).notify(notif.Description, time, notif.QuestionId, notif.Id, ans.User.Avatar);
                }
            }
            //Notify cho nhung thang dang ki nhan Notification cua Question,
            //GetConnections(userid trong bang QuestionSubcription)
            // Sau khi da co ban ghi o trong QuestionSubcription thi so sanh de notify

            //lay tat ca nhung question cua thang user de notify
            //((qe.SubscribedUsers.Any(u=> u.Id == ans.User.Id))
            foreach (var innn in qe.SubscribedUsers)
            {
                foreach (var connectionId in ConnectionMapping<string>.GetConnections(innn.Id.ToString()))
                {
                    if (innn.Id != ans.User.Id)
                    {
                        notificationhub.Clients.Client(connectionId)
                            .notify(notif.Description, time, notif.QuestionId, notif.Id, ans.User.Avatar);
                    }
                }
            }
            Utility.Log4Net(1, "Notification was sent to User [UserID: " + q.User.Id + "]");
        }

        public Comment UpdateAnswer(AnswerDto answerDto, Session currentSession)
        {
            try
            {
                var answer = _hozyDbContext.Comments.FirstOrDefault(a => a.Id == answerDto.CommentId);
                if (answer != null)
                {
                    if (currentSession.UserId == answer.User.Id ||
                        currentSession.RightList.Exists(right => right.RightName == "admin"))
                    {
                        answer.Content = answerDto.CommentContent;
                        _hozyDbContext.Entry(answer).State = EntityState.Modified;
                        _hozyDbContext.SaveChanges();
                        Utility.Log4Net(1, "Update Answer ==> [Answer Content: " + answerDto.CommentContent + "]");
                        LuceneHandler luceneHandler = new LuceneHandler();
                        luceneHandler.UpdateQuestionIndex(answer.Question);
                        return answer;
                    }
                    else
                    {
                        Utility.SetRespond(HttpStatusCode.Forbidden, "");
                        return null;
                    }
                    
                }
                else
                {
                    Utility.SetRespond(HttpStatusCode.NotFound, "");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Utility.Log4Net(0, "Updated Answer ==> [Answer Content: " + answerDto.CommentContent + "] FAIL!!!");
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        public bool MarkHelpful(int answerId, Session currentSession)
        {
            try
            {
                var answer = _hozyDbContext.Comments.Find(answerId);
                var question = _hozyDbContext.Questions.Include(q=>q.CommentList).FirstOrDefault(q=>q.Id== answer.Question.Id);
                if (question.User.Id == currentSession.UserId)
                {
                    foreach (var answerInQuestion in question.CommentList)
                    {
                        answerInQuestion.MarkedHelpful = false;
                    }
                    answer.MarkedHelpful = true;
                    _hozyDbContext.SaveChanges();
                }
                else
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden,"");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return false;
            }
        }

        public bool DeleteAnswer(int answerId, Session currentSession)
        {
            try
            {
                var answer = _hozyDbContext.Comments.Include(c => c.User).FirstOrDefault(c => c.Id == answerId);
                if (answer.User.Id == currentSession.UserId ||
                    currentSession.RightList.Exists(right => right.RightName.Equals("admin")))
                {
                    answer.isDeleted = true;
                    _hozyDbContext.SaveChanges();
                    return true;
                }
                else
                {
                    Utility.SetRespond(HttpStatusCode.Forbidden, "Forbidden");
                    return false;
                }
            }
            catch (Exception exception)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, exception.Message);
                return false;
            }
        }
    }
}
