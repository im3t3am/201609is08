﻿using System;
using System.Activities.Expressions;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Security.Policy;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Facebook;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;


namespace HozyService.Handlers
{
    public class UserHandler
    {
        private HozyDbContext _hozyDbContext;
        private Session _session;

        public UserHandler()
        {
            _hozyDbContext = new HozyDbContext();
            _session = new Session();
        }

        public UserHandler(HozyDbContext hozyDbContext)
        {
            _hozyDbContext = hozyDbContext;
        }

        public UserHandler(Session session)
        {
            this._session = session;
        }

        /// <summary>
        /// Lấy được thông tin về 1 user đơn lẻ nếu có ID của nó
        /// </summary>
        /// <param name="id_string"></param>
        /// <returns></returns>
        /// 
        ///

        public UserDto ConvertUserToDto(User user)
        {
            UserDto userDto = new UserDto();
            userDto.Id = user.Id;
            userDto.Username = user.Username;
            userDto.DisplayName = user.DisplayName;
            userDto.Description = user.Description;
            userDto.DateOfBirth = user.DateOfBirth;
            userDto.Email = user.Email;
            userDto.Address = user.Address;
            userDto.Website = user.Website;
            userDto.Employment = user.Employment;
            userDto.Education = user.Education;
            userDto.Avatar = user.Avatar;
            userDto.CreatedDate = user.CreatedDate;
            userDto.LastLogin = user.LastLogin;
            userDto.FacebookUrl = user.FacebookUrl;
            userDto.Cover = user.Cover;
            return userDto;
        }

        public User GetUser(int id)
        {
            try
            {
                User user = _hozyDbContext.Users.Where(u => u.Id == id).FirstOrDefault();
                if (user != null)
                {
                    return user;
                }
                else
                {
                    return null;
                }
                
            }
            catch (Exception ex)
            {
                if (ex is FormatException)
                {
                    throw new WebFaultException<string>("", HttpStatusCode.InternalServerError);
                }
                throw new WebFaultException<string>("", HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Trong hàm tạo User, tạm thời chỉ nhận vào 2 giá trị Username và Password
        /// Add User mới với quyền "User". Tạm thời code Create chưa hoàn chỉnh
        /// </summary>
        /// <param name="user"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// 
        public User CreateUser(UserDto user)
        {
            try
            {

                var checkuser = _hozyDbContext.Users.FirstOrDefault(u => u.Username.Equals(user.Username));
                if (checkuser == null)
                {
                    List<Right> userRightList = new List<Right>();

                    var userRight = _hozyDbContext.Rights.Where(right => right.RightName == "user").First() ?? new Right()
                    {
                        RightName = "user"
                    };
                    userRightList.Add(userRight);
                    User autoPw = new User()
                    {
                        // auto generate password( 12 character)
                        Password = Membership.GeneratePassword(6, 1)
                    };

                    var fromAddress = new MailAddress("dqkpro@gmail.com");
                    var toAddress = new MailAddress(user.Username);
                    MailMessage message = new MailMessage(new MailAddress("admin@hozy.info"), toAddress);
                    const string fromPassword = "kthq gnpx ivkr hmzi";
                    message.Subject = "Tài khoản Hozy của bạn";
                    message.Body = "Bạn đã đăng ký tài khoản với Email:"
                                   + "<br/>"
                                   + user.Username
                                   + "<br/>"
                                   + "Sử dụng mật khẩu dưới đây để đăng nhập Hozy:"
                                   + "<br/>"
                                   + autoPw.Password
                                   + "<br/>"
                                   + "Chào mừng quay lại trang web của chúng tôi!"
                                   +"<br/> https://hozy.info" ;
                    message.IsBodyHtml = true;
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        Timeout = 10000
                    };

                    {
                        smtp.Send(message);
                        Utility.Log4Net(1, "Email was sent successfully");
                    }

                    User userFromDto = new User()
                    {
                        Password = Utility.PasswordHash(autoPw.Password),
                        Username = user.Username,
                        Avatar = Constant.DefaultAvatar,
                        DisplayName = user.DisplayName,
                        RightList = userRightList,
                    };
                    User addUser = _hozyDbContext.Users.Add(userFromDto);
                    _hozyDbContext.SaveChanges();
                    return addUser;
                }
                else
                {
                   Utility.SetRespond(HttpStatusCode.NotAcceptable, "Fail");
                    return checkuser;
                }
                }
                
            catch (Exception ex)
            {
                if (ex.InnerException.InnerException.ToString().Contains("UsernameIndex"))
                {
                    throw new WebFaultException<string>("UsernameIndex", HttpStatusCode.Conflict);
                }
                throw new WebFaultException<string>("", HttpStatusCode.InternalServerError);
            }
        }

        public User ChangePassword(UserDto userDto,int id,Session currentSession)
        {

            User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == currentSession.UserId);
            var changePass = _hozyDbContext.Users.FirstOrDefault(c => c.Id == id);
            try
            {  
            if (changePass.Password.Equals(Utility.PasswordHash(userDto.Password)))
                {
                    changePass.Password = Utility.PasswordHash(userDto.NewPassword);
                    _hozyDbContext.Entry(changePass).State = EntityState.Modified;
                    _hozyDbContext.SaveChanges();
                Utility.SetRespond(HttpStatusCode.Accepted, "OK!");
                }
                else
            {
                 Utility.SetRespond(HttpStatusCode.NotAcceptable, "Wrong!");
            }
                
                }
                catch (Exception)
                {
                    throw new DbUpdateConcurrencyException("Add new PassWord to DB failed.");
                }
            return user;

        }

        public User ResetPassword(UserDto userDto)
        {
            try
            {

                User user = _hozyDbContext.Users.FirstOrDefault(u => u.Username.Equals(userDto.Username));
                if (user != null)
                {
                    user.Password = Membership.GeneratePassword(6, 1);
                }

                var fromAddress = new MailAddress("dqkpro@gmail.com");
                var toAddress = new MailAddress(user.Username);
                MailMessage message = new MailMessage(new MailAddress("admin@hozy.info"), toAddress);
                const string fromPassword = "kthq gnpx ivkr hmzi";
                message.Subject = "Đặt lại mật khẩu";
                message.Body = "Bạn đã đặt lại thành công mật khẩu của tài khoản:" + userDto.Username
                    + "</br>"
                    + "Đây là mật khẩu mới của bạn: " + " " + user.Password + " "
                    + "Đăng nhập lại trang web tại đây:"
                    +"<br/> https://hozy.info";
                // thiếu link đăng nhập or link thay đổi password
                message.IsBodyHtml = true;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 10000
                };

                {
                    smtp.Send(message);
                    Utility.Log4Net(1, "Email was sent successfully");
                }


                    user.Password = Utility.PasswordHash(user.Password);
                    _hozyDbContext.Entry(user).State = EntityState.Modified;
                    _hozyDbContext.SaveChanges();
                return user;
            }
                catch (Exception)
                {
                    throw new DbUpdateConcurrencyException("Add new password to DB failed.");
                }
            
        }

        public User UpdateUserProfile(UserDto userDto, int id, Session currentSession)
        {
            User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == currentSession.UserId);
            if (user != null)
            {
                user.Description = userDto.Description;
                user.Address = userDto.Address;
                user.DateOfBirth = userDto.DateOfBirth;
                user.Education = userDto.Education;
                user.Employment = userDto.Employment;
                user.Website = userDto.Website;
                user.Avatar = userDto.Avatar;
                user.Cover = userDto.Cover;
                user.DisplayName = userDto.DisplayName;
                try
                {
                    _hozyDbContext.Entry(user).State = EntityState.Modified;
                    _hozyDbContext.SaveChanges();
                }
                catch (Exception)
                {
                    throw new DbUpdateConcurrencyException("Add UserProfile to DB failed.");
                }
            }
            return user;
            
        }

        public User GetUserProfile(int id)
        {
            User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == id);
            UserDto userDto = new UserDto();
            if (user != null)
            {
                return user;
            }
            else
            {
                return null;    
            }
            
        }
        
        public User GetFacebookUrl()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = Constant.ClientId,
                client_secret = Constant.ClientSecret,
                
                redirect_uri = Constant.FaceBookRedirectUrl,
                response_type = "code",
                scope = "email,user_birthday,user_about_me"
            });
            return new User()
            {
                FacebookUrl = loginUrl.AbsoluteUri,
            };

        }

        public UserDto FacebookCallBack(String token)
        {
           
                var fb = new FacebookClient();
            dynamic result = fb.Get("oauth/access_token?", new
            {
                client_id = Constant.ClientId,
                client_secret = Constant.ClientSecret,
                
                redirect_uri = Constant.FaceBookRedirectUrl,
                code = token
            });
            var accessToken = result.access_token;
            _session.Token = accessToken;
            fb.AccessToken = accessToken;
            dynamic me = fb.Get("/me?fields=id,name,gender,link,birthday,email");
            string facebookId = me.id;
            string email = me.email;
            string name = me.name;
            string dob = me.birthday;
            string gender = me.gender;
            string link = me.link;
            if (string.IsNullOrEmpty(email))
            {
                email = facebookId + "@facebook.com";
            }
            me.email = email;
           User user = _hozyDbContext.Users.FirstOrDefault(i => i.Email == email);
           
           List<Right> useRightList = new List<Right>();
           var userRight = _hozyDbContext.Rights.Where(right => right.RightName == "user").First() ?? new Right()
           {
               RightName = "user"
           };
            
           useRightList.Add(userRight);
           User addUser = new User();
            if (user == null)
            {

                User newuser = new User()
                {

                    Email = email,
                    DisplayName = name,
                    CreatedDate = DateTime.UtcNow,
                    Password = string.Empty,
                    LastLogin = DateTime.UtcNow,
                    Username = "fb." + email.Split(new string[] {"@"}, StringSplitOptions.None)[0],
                    FacebookUrl = me.link,
                    Avatar = "https://graph.facebook.com/" + me.id + "/picture?type=large",
                    RightList = useRightList,
                };
               addUser = _hozyDbContext.Users.Add(newuser);
               _hozyDbContext.SaveChanges();
            }
            else
            {
                addUser = new User()
                {
                    Username = user.Username,
                    DisplayName = user.DisplayName,
                    Email = user.Email,
                    DateOfBirth = user.DateOfBirth,
                    CreatedDate = user.CreatedDate,
                    LastLogin = user.LastLogin,
                    Avatar = user.Avatar,
                    RightList = user.RightList,
                    Id = user.Id,
                };

            }
            return new UserDto()
            {
                Username = addUser.Email,
                DisplayName = addUser.DisplayName,
                Email = addUser.Email,
                DateOfBirth = addUser.DateOfBirth,
                Id = addUser.Id,
                Avatar = addUser.Avatar,
                CreatedDate = addUser.CreatedDate,
                LastLogin = addUser.LastLogin,
                RightList = addUser.RightList.Select(x => x.RightName).ToList()
            };
        }

        private Uri RedirectUri
        {
            get
            {
                var Request = HttpContext.Current.Request;
                var uriBuilder = new UriBuilder(Request.Url)
                {
                    Query = null,
                    Fragment = null,
                };
                return uriBuilder.Uri;
            }
        }

        /// <summary>
        /// Tạo Feed cho User nếu có thể. Trả ra feed mới được cập nhật
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Feed GenerateFeed(int id)
        {
            
            User feedUser = _hozyDbContext.Users.Find(id);
            feedUser.Feed = feedUser.Feed ?? new Feed();

            // Nếu feed quá cũ, update
            if ((DateTime.Now - feedUser.Feed.CreationDate).TotalMinutes > Constant.UserFeedCreateDelay)
            {
                foreach (var subscribedTopic in feedUser.SubscribedTopics)
                {
                    foreach (var taggedQuestion in subscribedTopic.TaggedQuestions.OrderBy(question => question.Rank).Take(100))
                    {
                        if (!feedUser.Feed.QuestionFeed.Contains(taggedQuestion))
                        {
                            feedUser.Feed.QuestionFeed.Add(taggedQuestion);
                        }
                    }
                }
                feedUser.Feed.CreationDate = DateTime.Now;
                _hozyDbContext.SaveChanges();
            }
            
            return feedUser.Feed;
        }

        public List<Question> GetQuestionOfUser(int id)
        {
            try
            {
                User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == id);
                List<Question> questionOfUser =
                    _hozyDbContext.Questions.OrderByDescending(question => question.CreationDate)
                    .Where(question => question.User.Id == user.Id && question.isDeleted == false) 
                    .ToList();
                return questionOfUser;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<Comment> GetAnswerOfUser(int id)
        {
            try
            {
                User user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == id);
                List<Comment> answerOfUser =
                    _hozyDbContext.Comments.OrderByDescending(answer => answer.CreationDate)
                        .Where(answer => answer.User.Id == user.Id && answer.isDeleted == false && answer.Question!=null)
                        .ToList();
                return answerOfUser;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<Question> UserGetFeed(int id, int skip)
        {
           
            Feed feed = GenerateFeed(id);
            return feed.QuestionFeed.OrderByDescending(question => question.Rank).Skip(skip).Take(10).ToList();
            
        }

        public List<Question> UserGetNewQuestion(int id, int skip)
        {
            List<Question> NewQuestions = new List<Question>();
            try
            {
                User user = _hozyDbContext.Users.Find(id);
                foreach (var subscribedTopic in user.SubscribedTopics)
                {
                    List<Question> topicNewQuestion = _hozyDbContext.Questions
                        .OrderByDescending(question => question.CreationDate).Take(10000) // Chỉ lấy ra 10000 record mới nhât: Để đảm bảo tốc độ xử lí
                        .Where(question => question.Topics
                            .Select(topic => topic.Id)
                            .Contains(subscribedTopic.Id))
                        .ToList();
                    foreach (var question in topicNewQuestion)
                    {
                        if (!NewQuestions.Contains(question) && NewQuestions.Count < 200) // Chi lấy ra 200 record đầu tiên
                        {
                            NewQuestions.Add(question);
                        }
                    }
                }
                NewQuestions =
                    NewQuestions.OrderByDescending(question => question.CreationDate).Skip(skip).Take(10).ToList();
                return NewQuestions;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public List<User> GetTopUsers()
        {
            if ((DateTime.Now - TopUser.CreationDate).TotalHours > Constant.UserRankingDelay)
            {
                Dictionary<User,int> topUserDictionary = new Dictionary<User,int>();
                var questions =
                    _hozyDbContext.Questions.Where(
                        question =>
                            SqlFunctions.DateDiff("day", question.CreationDate, DateTime.Now) <
                            Constant.UserRankingDateRange).ToList();
                foreach (var question in questions)
                {
                    if (!topUserDictionary.ContainsKey(question.User))
                    {
                        topUserDictionary.Add(question.User,0);
                    }
                    topUserDictionary[question.User] += question.Rank;
                    foreach (var comment in question.CommentList)
                    {
                        if (!topUserDictionary.ContainsKey(question.User))
                        {
                            topUserDictionary.Add(question.User,0);
                        }
                        topUserDictionary[question.User] += comment.Votes.Count(vote => vote.State);
                    }
                }
                TopUser.TopUsers = topUserDictionary.OrderByDescending(x => x.Value).Select(y => y.Key).Take(10).ToList();
                TopUser.CreationDate = DateTime.Now;

            }
            return TopUser.TopUsers;
        }

        public UserSimpleDto ConvertUserSimpleDto(User user)
        {
            UserSimpleDto userDto = new UserSimpleDto();
            userDto.Id = user.Id;
            userDto.Username = user.Username;
            userDto.DisplayName = user.DisplayName;
            userDto.Avatar = user.Avatar;
            return userDto;
        }

        public List<Question> GetFollowingQuestion(int id)
        {
            List<Question> FollowingQuestion = new List<Question>();
            try
            {
                FollowingQuestion = _hozyDbContext.Questions.Where(question => question.SubscribedUsers.Select(u=>u.Id).Contains(id)).ToList();
                return FollowingQuestion;
            }
            catch (Exception ex)
            {
                Utility.SetRespond(HttpStatusCode.InternalServerError, ex.Message);
                return null;
            }
        }
    }
}

