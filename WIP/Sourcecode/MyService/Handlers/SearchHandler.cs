﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;

namespace HozyService.Handlers
{
    public class SearchHandler
    {
        private HozyDbContext _hozyDbContext;

        public SearchHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public SearchHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }
        

        public List<Topic> SearchTopic(string query)
        {
            List<Topic> resultTopics = _hozyDbContext.Topics.Where(t => t.Name.Contains(query)).ToList();
            return resultTopics;
        }

        public List<Question> SearchQuestions(string querystr)
        {
            List<Question> resultQuestions = new List<Question>();

            var directory = FSDirectory.Open(new DirectoryInfo(@"C:/Lucene"));
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);

            var parser = new MultiFieldQueryParser(Version.LUCENE_30, new[] { "questionTitle", "questionContent", "comment", "unsignedquestion", "unsignedcomment" }, analyzer); // (1)
            Query query = parser.Parse(querystr);                           // (2)
            var searcher = new IndexSearcher(directory, true);          // (3)
            TopDocs topDocs = searcher.Search(query, 10);               // (4)

            int results = topDocs.ScoreDocs.Length;

            Console.WriteLine("Found {0} results", results);

            for (int i = 0; i < results; i++)
            {
                ScoreDoc scoreDoc = topDocs.ScoreDocs[i];
                float score = scoreDoc.Score;
                int docId = scoreDoc.Doc;
                Document doc = searcher.Doc(docId);

                Debug.WriteLine("{0}. score {1}", i + 1, score);
                Debug.WriteLine("ID: " + doc.Get("QuestionId"));
                Debug.WriteLine("Text found: \r\n" + doc.Get("questionTitle"));
                string questionId = doc.Get("QuestionId");

                resultQuestions.Add(_hozyDbContext.Questions.FirstOrDefault(question => question.Id.ToString().Equals(questionId)));
            }

            
            
            //resultQuestions = hozyDbContext.Questions.Where(
            //    question => question.QuestionContent.Contains(query) || question.QuestionTitle.Contains(query)).ToList();
            return resultQuestions;
        }

        public List<User> SearchUser(string query)
        {
            List<User> resultUsers = _hozyDbContext.Users.Where(user => user.DisplayName.Contains(query)).ToList();
            return resultUsers;
        }
    }
}
