﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;

namespace HozyService.Handlers
{
    public class NotificationHandler
    {
        private HozyDbContext _hozyDbContext;

        public NotificationHandler(HozyDbContext dbContext)
        {
            _hozyDbContext = dbContext;
        }

        public NotificationHandler()
        {
            _hozyDbContext = new HozyDbContext();
        }

        /// <summary>
        /// lay ra tat ca cac notification cua mot user
        /// </summary>
        /// <param name="currentSession"></param>
        /// <returns></returns>
        public List<NotificationDto> GetNotification(Session currentSession)
        {
            var notiList = _hozyDbContext.Notifications.Where(n => n.TargetUserId == currentSession.UserId).OrderByDescending(x => x.CreationDate).Take(10).ToList();
            var notificationListDto = new List< NotificationDto>();

            //lay ra DisplayName va Avatar cua User co cau tra loi
            foreach (var noti in notiList)
            { 
                var user = _hozyDbContext.Users.FirstOrDefault(u => u.Id == noti.UserId);
                var notificationDto = new NotificationDto();
                notificationDto.Id = noti.Id;
                notificationDto.DisplayName = user.DisplayName;
                notificationDto.Avatar = user.Avatar;
                notificationDto.Description = noti.Description;
                var unixTimestamp = (int)(noti.CreationDate.Subtract(new DateTime(1970, 1, 1, 0 ,0 ,0, 0))).TotalSeconds;
                //var time = unixTimestamp;
                notificationDto.CreationDate = unixTimestamp.ToString();
                notificationDto.IsChecked = noti.IsChecked;
                notificationDto.QuestionId = noti.QuestionId;
                notificationListDto.Add(notificationDto);
            }

            return notificationListDto;
        }

        public bool Test(int qid,int uid)
        {
            var hozy = new HozyDbContext();
            var question = hozy.Questions.FirstOrDefault(q => q.Id == qid);
            var user = hozy.Users.FirstOrDefault(u => u.Id == uid);

            if (question == null) return false;
            question.SubscribedUsers.Add(user);
            hozy.SaveChanges();
            return true;
        }
    }
    }
