﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using MyService.DTOs;

namespace MyService.QuestionServices
{
    public interface IQuestionService
    {
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "question/{id}"
            )]
        QuestionDTO GetQuestion(String id);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "question"
            )]
        QuestionDTO AddQuestion(String id);
    }
}
