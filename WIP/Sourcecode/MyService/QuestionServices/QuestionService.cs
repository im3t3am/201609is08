﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using MyService.DTOs;
using MyService.Handlers;

namespace MyService.QuestionServices
{
    public class QuestionService : IQuestionService
    {
        private QuestionHandler questionHandler = new QuestionHandler();

        //get a question
        public QuestionDTO GetQuestion(String id)
        {
            QuestionDTO q = new QuestionDTO();
            q.QuestionContent = "";
            q.QuestionTitle = "";
            return q;
        }

        /// <summary>
        /// Add a question from Front-end
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QuestionDTO AddQuestion(string id)
        {
            QuestionHandler questionHandler = new QuestionHandler();
            QuestionDTO qe = new QuestionDTO();
            qe.QuestionId = 15;
            qe.QuestionTitle = "Saiyan";
            qe.QuestionContent = "Saiyan69";
            questionHandler.AddQuestion(qe);
        }
    }
}
