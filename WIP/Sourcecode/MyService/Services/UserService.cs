﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class UserService : IUserService
    {
        private UserHandler userHandler = new UserHandler();
        private SessionHandler sessionHandler = new SessionHandler();
        private QuestionHandler questionHandler = new QuestionHandler();
        private AnswerHandler answerHandler = new AnswerHandler();

        public UserDto GetUser(UserDto userDto,string string_id)
        {
            int id = 0;
            try
            {
                id = Int32.Parse(string_id);
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Wrong Input");
                throw new WebFaultException<string>("WrongInput", HttpStatusCode.BadRequest);
            }
            var user = userHandler.GetUser(id);
            return userHandler.ConvertUserToDto(user);
        }


        public UserDto CreateUser(UserDto user)
        {
            var newuser = userHandler.CreateUser(user);
            return userHandler.ConvertUserToDto(newuser);
        }

        public UserDto UpdateUserProfile(UserDto userDto, string id)
        {
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            if (currentSession.UserId != int.Parse(id))
            {
                Utility.Log4Net(0, "No authorization to edit profile!");
                Utility.SetRespond(HttpStatusCode.Unauthorized, "Can't edit to other id");
                return null;
            }
            var updateprofile = userHandler.UpdateUserProfile(userDto, int.Parse(id), currentSession);
            return userHandler.ConvertUserToDto(updateprofile);
        }
        public UserDto GetUserProfile(string id)
        {
            if (!string.IsNullOrEmpty(id) && id != "undefined")
            {
                var getuserpf = userHandler.GetUserProfile(int.Parse(id));
                return userHandler.ConvertUserToDto(getuserpf);
            }
            return null;
        }

        public UserDto GetFacebookUrl()
        {
            var getfburl = userHandler.GetFacebookUrl();
            return userHandler.ConvertUserToDto(getfburl);
        }

        public UserDto ChangePassword(UserDto userDto, string id)
        {
            String token = Utility.GetToken();
            
            Session session = sessionHandler.AuthenSession(token);
            var changepw = userHandler.ChangePassword(userDto, int.Parse(id), session);
            return userHandler.ConvertUserToDto(changepw);
        }

        public UserDto ResetPassword(UserDto userDto)
        {
            var resetpw = userHandler.ResetPassword(userDto);
            return userHandler.ConvertUserToDto(resetpw);
        }

        public Tuple<UserDto, SessionDto> FacebookCallBack(string token)
        {
            UserDto userDto = userHandler.FacebookCallBack(token);
            SessionDto sessionDto = new SessionDto();
            if (userDto != null)
            {
                if (userDto.Email != null)
                {
                    sessionDto = sessionHandler.OpenFacebookSession(userDto.Email);
                }
            }
            return new Tuple<UserDto, SessionDto>(userDto, sessionDto);
        }

        public List<QuestionDto> GetFeed(string string_id,string string_skip)
        {
            int id = 0,skip=0;
            try
            {
                id = Int32.Parse(string_id);
                skip = Int32.Parse(string_skip);
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Wrong Input");
                throw new WebFaultException<string>("WrongInput", HttpStatusCode.BadRequest);
            }
            var QuestionList = userHandler.UserGetFeed(id,skip);
            List<QuestionDto> QuestionDtoList = new List<QuestionDto>();
            foreach (var question in QuestionList)
            {
                QuestionDto questionDto = new QuestionDto();
                questionDto = questionHandler.ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
                QuestionDtoList.Add(questionDto);
            }
            return QuestionDtoList;
        }

        public List<QuestionDto> GetQuestionOfUser(string id)
        {
            var QuestionList = userHandler.GetQuestionOfUser(int.Parse(id));
            List<QuestionDto> QuestionDtoList = new List<QuestionDto>();
            foreach (var question in QuestionList)
            {
                QuestionDto questionDto = new QuestionDto();
                questionDto = questionHandler.ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
                QuestionDtoList.Add(questionDto);
            }
            return QuestionDtoList;
        }

        public List<AnswerDto> GetAnswerOfUser(string id)
        {
            var answerList = userHandler.GetAnswerOfUser(int.Parse(id));
            List<AnswerDto> answerDtoList = new List<AnswerDto>();
            foreach (var answer in answerList)
            {
                AnswerDto answerDto = new AnswerDto();
                answerDto = answerHandler.ConvertAnswerToDto(answer);
                answerDtoList.Add(answerDto);
            }
            return answerDtoList;
        }
        
        public List<QuestionDto> UserGetNewQuestion(string string_id, string string_skip)
        {
            int id = 0, skip = 0;
            try
            {
                id = Int32.Parse(string_id);
                skip = Int32.Parse(string_skip);
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Wrong Input");
                throw new WebFaultException<string>("WrongInput", HttpStatusCode.BadRequest);
            }
            var QuestionList = userHandler.UserGetNewQuestion(id, skip);
            List<QuestionDto> QuestionDtoList = new List<QuestionDto>();
            foreach (var question in QuestionList)
            {
                QuestionDto questionDto = new QuestionDto();
                questionDto = questionHandler.ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
                QuestionDtoList.Add(questionDto);
            }
            return QuestionDtoList;
        }

        public List<UserSimpleDto> GetTopUser()
        {
            List<UserSimpleDto> userDtos = new List<UserSimpleDto>();
            var TopUsers = userHandler.GetTopUsers();
            foreach (var topUser in TopUsers)
            {
                if (topUser != null)
                {
                    UserSimpleDto userDto = userHandler.ConvertUserSimpleDto(topUser);
                    userDtos.Add(userDto);
                }
                
            }
            return userDtos;
        }


        public List<QuestionDto> GetFollowingQuestion(string id)
        {
            var QuestionList = userHandler.GetFollowingQuestion(Int32.Parse(id));
            if (QuestionList == null)
            {
                return null;
            }
            List<QuestionDto> QuestionDtoList = new List<QuestionDto>();
            foreach (var question in QuestionList)
            {
                QuestionDto questionDto = new QuestionDto();
                questionDto = questionHandler.ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
                QuestionDtoList.Add(questionDto);
            }
            return QuestionDtoList;
        }
    }
}
