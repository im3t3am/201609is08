﻿using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class SessionService : ISessionService
    {
        private SessionHandler sessionHandler = new SessionHandler();

        public SessionDto OpenSession(SessionRequestDto sessionRequestDto)
        {
            string ipAddress = Utility.getIp();
            Utility.Log4Net(1, "Get IP Address of the incoming request.");
            return sessionHandler.OpenSession(sessionRequestDto, ipAddress);
        }


        public bool DeleteSession()
        {
            string token = Utility.GetToken();
            return sessionHandler.DeleteSession(token);
        }
    }
}
