﻿using System;
using System.Collections.Generic;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class AnalyticService : IAnalyticService
    {
        AnalyticHandler analyticHandler = new AnalyticHandler();
        QuestionHandler questionHandler = new QuestionHandler();

        public List<TopTopicLogDto> TopicLog(string amount,string skip)
        {
            var topTopicLog = analyticHandler.GetTopTopicLog(Int32.Parse(amount), Int32.Parse(skip), new TopicHandler());
            return topTopicLog;
        }

        public List<TopQuestionLogDto> QuestionLog(string amount, string skip)
        {
            var topQuestionLog = analyticHandler.GetTopQuestionLog(Int32.Parse(amount), Int32.Parse(skip));
            List<TopQuestionLogDto> topQuestionLogDtos = new List<TopQuestionLogDto>();
            foreach (var topQuestion in topQuestionLog)
            {
                var topQuestionDto = analyticHandler.ConvertTopQuestionToDtos(topQuestion, questionHandler);
                topQuestionLogDtos.Add(topQuestionDto);
            }
            return topQuestionLogDtos;
        }
    }
}
