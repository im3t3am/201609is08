﻿using System;
using System.Collections.Generic;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class TopicService : ITopicService
    {
        private  QuestionHandler questionHandler = new QuestionHandler();
        private TopicHandler topicHandler = new TopicHandler();
        private SessionHandler sessionHandler = new SessionHandler();

        public TopicDto GetDetailTopic(string topicid)
        {
            return topicHandler.GetDetailTopic(Int32.Parse(topicid));
        }

        public bool AddUserTopic(string id) 
        {
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            return topicHandler.AddUserTopic(Int32.Parse(id),currentSession);
        }

        public TopicDto AddTopic(TopicDto topicDto)
        {
            var topic = topicHandler.AddTopic(topicDto);
            return topicHandler.ConvertTopicToDto(topic);
        }

        public bool AddQuestionTopic(string topicid, string questionid)
        {
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            return topicHandler.AddQuestionTopic(int.Parse(topicid), int.Parse(questionid),currentSession);
        }

        public bool UserUnfollowTopic(string topicid)
        {
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            return topicHandler.UserUnfollow(int.Parse(topicid),currentSession);
        }

        public bool QuestionUnfollowTopic(string topicid, string questionid)
        {
            string token = Utility.GetToken();
            Session currentSession = sessionHandler.AuthenSession(token);
            return topicHandler.QuestionUnfollow(int.Parse(topicid), int.Parse(questionid), currentSession);
        }

        public List<TopicDto> GetUserTopic(string userid)
        {
            int id;
            bool result = Int32.TryParse(userid, out id);
            var topics = topicHandler.GetUserTopic(id);
            List<TopicDto> topicdto = new List<TopicDto>();
            if(topics !=null){
            foreach (var topic in topics)
            {
                TopicDto topicDto = new TopicDto();
                topicDto = topicHandler.ConvertTopicToDto(topic);
                topicdto.Add(topicDto);
            }
            }
            return topicdto;
        }

        public List<QuestionDto> GetTopicNewQuestion(string str_topicId, string str_skip)
        {
            int topicId, skip;
            topicId = Int32.Parse(str_topicId);
            skip = Int32.Parse(str_skip);
            List<QuestionDto> questionDtos = new List<QuestionDto>();
            var QuestionList = topicHandler.GetTopicNewQuestion(topicId, skip);
            if (QuestionList != null)
            {
                foreach (var question in QuestionList)
                {
                    questionDtos.Add(questionHandler.ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler()));
                }
            }
            return questionDtos;
        }

        public List<QuestionDto> GetTopicTopQuestion(string str_topicId, string str_skip)
        {
            int topicId, skip;
            topicId = Int32.Parse(str_topicId);
            skip = Int32.Parse(str_skip);
            List<QuestionDto> questionDtos = new List<QuestionDto>();
            var QuestionList = topicHandler.GetTopicTopQuestion(topicId, skip);
            if (QuestionList != null)
            {
                foreach (var question in QuestionList)
                {
                    questionDtos.Add(questionHandler.ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler()));
                }
            }
            return questionDtos;
        }

        public List<TopicDto> GetHotTopic()
        {
            List<TopicDto> HotTopic = new List<TopicDto>();
            foreach (var topic in topicHandler.GetHotTopic())
            {
                var topicDto = new TopicDto();
                topicDto = topicHandler.ConvertTopicToDto(topic);
                HotTopic.Add(topicDto);
            }
            return HotTopic;
        }

        public List<TopicDto> GetRandomTopic(string amount)
        {
            List<TopicDto> RandomTopics = new List<TopicDto>();
            foreach (var topic in topicHandler.GetRandomTopics(Int32.Parse(amount)))
            {
                var topicDto = new TopicDto();
                topicDto = topicHandler.ConvertTopicToDto(topic);
                RandomTopics.Add(topicDto);
            }
            return RandomTopics;
        }

        public TopicDto UpdateTopic(TopicDto topicDto, string id)
        {
            string token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            var topic = topicHandler.UpdateTopic(topicDto, int.Parse(id), currentSession);
            return topicHandler.ConvertTopicToDto(topic);
        }
    }
}
