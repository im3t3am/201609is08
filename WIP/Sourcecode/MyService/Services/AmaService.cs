﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class AmaService : IAmaService
    {
        private SessionHandler sessionHandler = new SessionHandler();
        private AmaHandler amaHandler = new AmaHandler();

        public int CreateAma(AmaCreationDto amaCreationDto)
        {
            String token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            return amaHandler.CreateAma(amaCreationDto, currentSession).Id;
        }


        public AmaDto GetAma(string id)
        {
            AmaSession amaSession = amaHandler.GetAma(Int32.Parse(id));
            return amaHandler.ConnvertAmaToDto(amaSession, new UserHandler());
        }


        public int AddQuestion(AmaQuestionDto amaQuestionDto)
        {
            String token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            return amaHandler.AddAmaQuestion(amaQuestionDto, currentSession).Id;
        }


        public int AddAnswer(AmaAnswerCreationDto amaAnswerCreationDto)
        {
            String token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            return amaHandler.AddAmaAnswer(amaAnswerCreationDto, currentSession).Id;
        }


        public int UpdateQuestion(AmaQuestionDto amaQuestionDto, string id)
        {
            String token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            return amaHandler.UpdateAmaQuestion(Int32.Parse(id), amaQuestionDto, currentSession).Id;
        }

        public List<AmaDto> GetAmaList(string skip)
        {
            List<AmaSession> amaSessions = amaHandler.ListAmaSessions(Int32.Parse(skip));
            List<AmaDto> amaDtos = new List<AmaDto>();
            foreach (var amaSession in amaSessions)
            {
                AmaDto amaDto = new AmaDto();
                amaDto = amaHandler.ConnvertAmaToDto(amaSession, new UserHandler());
                amaDtos.Add(amaDto);
            }
            return amaDtos;
        }
    }
}
