﻿using System;
using System.Collections.Generic;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CommentList" in both code and config file together.
    public class CommentService : ICommentService
    {
        private CommentHandler commentHandler = new CommentHandler();
        private SessionHandler sessionHandler = new SessionHandler();

        public List<CommentDto> GetComment(string id)
        {
            List<CommentDto> commentDtos = new List<CommentDto>();
            var comments = commentHandler.GetCommentList(Int32.Parse(id));
            foreach (var comment in comments)
            {
                CommentDto commentDto = commentHandler.ConvertCommentToDto(comment);
                if (commentDto != null)
                {
                    commentDtos.Add(commentDto);
                }
            }
            return commentDtos;
        }

        public CommentDto AddComment(CommentDto comment)
        {
            String token = Utility.GetToken();            
            Session currentSession = sessionHandler.AuthenSession(token);
            CommentHandler commentHandler = new CommentHandler();
            var addedcomment = commentHandler.AddComment(comment, currentSession);
            return commentHandler.ConvertCommentToDto(addedcomment);
        }

        public CommentDto UpdateComment(CommentDto commentDto, string id)
        {
            String token = Utility.GetToken();
            Session session = sessionHandler.AuthenSession(token);
            var comment = commentHandler.UpdateComment(commentDto, int.Parse(id), session);
            return commentHandler.ConvertCommentToDto(comment);
        }

        public bool DeleteComment(string commentId)
        {
            String token = Utility.GetToken();
            Session session = sessionHandler.AuthenSession(token);
            return commentHandler.DeleteComment(Int32.Parse(commentId),session);
        }

    }
}
