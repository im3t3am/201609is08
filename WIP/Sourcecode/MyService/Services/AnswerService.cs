﻿using System;
using System.Collections.Generic;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class AnswerService : IAnswerService
    {
        private AnswerHandler answerHandler = new AnswerHandler();
        private SessionHandler sessionHandler = new SessionHandler();

        public List<AnswerDto> GetAnswer(string questionId)
        {
            var answerList = answerHandler.GetAnswer(int.Parse(questionId));
            List<AnswerDto> AnswerDtoList = new List<AnswerDto>();
            foreach (var answer in answerList)
            {
                AnswerDto answerDto = new AnswerDto();
                answerDto = answerHandler.ConvertAnswerToDto(answer);
                if (answerDto != null)
                {
                    AnswerDtoList.Add(answerDto);
                }
            }
            return AnswerDtoList;
        }

        public AnswerDto AddAnswer(AnswerCreateDto answerCreateDto)
        {
            String token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            return answerHandler.AddAnswer(answerCreateDto, currentSession);
        }

        public AnswerDto UpdateAnswer(AnswerDto answerDto)
        {
            String token = Utility.GetToken();
            
            Session session = sessionHandler.AuthenSession(token);
            
            var answer = answerHandler.UpdateAnswer(answerDto, session);

            return answerHandler.ConvertAnswerToDto(answer);
        }

        public bool MarkHelpful(string answerId)
        {
            String token = Utility.GetToken();

            Session session = sessionHandler.AuthenSession(token);

            return answerHandler.MarkHelpful(Int32.Parse(answerId), session);
        }

        public bool DeleteAnswer(string answerId)
        {
            String token = Utility.GetToken();

            Session session = sessionHandler.AuthenSession(token);

            return answerHandler.DeleteAnswer(Int32.Parse(answerId), session);
        }
    }
}
