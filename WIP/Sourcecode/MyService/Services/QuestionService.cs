﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class QuestionService : IQuestionService
    {
        private QuestionHandler questionHandler = new QuestionHandler();
        private SessionHandler sessionHandler = new SessionHandler();
        /// <summary>
        /// get a question
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QuestionDto GetQuestion(string id)
        {
            return questionHandler.GetQuestion(Int32.Parse(id));     
        }

        /// <summary>
        /// Add a question from Front-end
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QuestionDto AddQuestion(QuestionDto questionDto)
        {
            string token = Utility.GetToken();

            // Tại sao cần Session Handler ở đây : Vì ta có SSHandler giúp làm nhiệm vụ giao tiếp với DB Session
            // Lí do không đưa session handler vào bên trong question handler : không thể để cùng 1 lúc 2 DbContext đọc db
            // ( bên trong question hander có 1 db context )
            Session currentSession = sessionHandler.AuthenSession(token);
            
            QuestionHandler questionHandler = new QuestionHandler();
            
            return questionHandler.AddQuestion(questionDto, currentSession);
        }

        public bool UpdateQuestion(QuestionDto questionDto, string id)
        { 
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            return questionHandler.UpdateQuestion(questionDto, int.Parse(id),currentSession);
        }

        public List<QuestionDto> GetTopQuestion(string string_skip)
        {
            int skip = 0;
            try
            {
                skip = Int32.Parse(string_skip);
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Wrong Input!!!");
                throw new WebFaultException<string>("WrongInput", HttpStatusCode.BadRequest);
            }
            return questionHandler.GetTopQuestion(skip);
        }

        public List<QuestionDto> GetNewQuestion(string string_skip)
        {
            int skip = 0;
            try
            {
                skip = Int32.Parse(string_skip);
            }
            catch (Exception)
            {
                Utility.Log4Net(0, "Wrong Input!!!");
                throw new WebFaultException<string>("WrongInput", HttpStatusCode.BadRequest);
            }
            return questionHandler.GetNewQuestion(skip);
        }

        public bool AddQuestionSubscription(string questionid)
        {
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            return questionHandler.AddQuestionSubscribe(int.Parse(questionid), currentSession);
        }

        public bool RemoveQuestionSubscription(string questionid)
        {
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            return questionHandler.RemoveQuestionSubscribe(int.Parse(questionid), currentSession);
        }

        public List<QuestionDto> ListQuestionFollowed()
        {
            string token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            return questionHandler.ListQuestionFollowed(currentSession);
        }

        public List<UserDto> ListUserFollowed(string qid)
        {
            return questionHandler.ListUserFollowed(int.Parse(qid));
        }

        public List<QuestionDto> GetQuestionReported()
        {
            String token = Utility.GetToken();
            Session currentSession = sessionHandler.AuthenSession(token);

            var questionList = questionHandler.GetQuestionReported(currentSession);
            List<QuestionDto> QuestionDtoList = new List<QuestionDto>();
            foreach (var question in questionList)
            {
                QuestionDto questionDto = new QuestionDto();
                questionDto = questionHandler.ConvertQuestionToDto(question, new TopicHandler(), new AnswerHandler());
                QuestionDtoList.Add(questionDto);
            }
            return QuestionDtoList;
        }

        public bool IsFollow(string questionid)
        {
            string token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            return questionHandler.IsFollowing(int.Parse(questionid), currentSession);
        }

        public bool IsReported(string questionid)
        {
            string token = Utility.GetToken();

            Session currentSession = sessionHandler.AuthenSession(token);

            return questionHandler.IsReported(int.Parse(questionid), currentSession);
        }

        public bool DeleteQuestion(string questionId)
        {
            string token = Utility.GetToken();
            Session currentSession = sessionHandler.AuthenSession(token);
            return questionHandler.DeleteQuestion(Int32.Parse(questionId), currentSession);
        }


        public List<QuestionDto> GetRelatedQuestion(string id)
        {
            List<QuestionDto> relatedQuestions = new List<QuestionDto>();
            var questions = questionHandler.GetRelatedQuestions(Int32.Parse(id));
            foreach (var question in questions)
            {
                QuestionDto questionDto = questionHandler.ConvertQuestionToDto(question, new TopicHandler(),new AnswerHandler());
                relatedQuestions.Add(questionDto);
            }
            return relatedQuestions;
        }
    }
}
