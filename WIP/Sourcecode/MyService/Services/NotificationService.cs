﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Helpers;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    public class NotificationService : INotificationService
    {
        private readonly NotificationHandler _notificationHandler = new NotificationHandler();
        private readonly SessionHandler _sessionHandler = new SessionHandler();

        public List<NotificationDto> GetNotification()
        {
            string token = Utility.GetToken();
            Session currentSession = _sessionHandler.AuthenSession(token);
            return _notificationHandler.GetNotification(currentSession);
        }

        public static void LoadSliderItem()
        {
            if (Slider.SliderItems.Count == 0)
            {
                string file = Utility.GetAssemblyLocation() + "\\Slide.json";
                string json = File.ReadAllText(file);
                dynamic data = Json.Decode(json);
                foreach (var item in data.Items)
                {
                    Slider.SliderItem slider = new Slider.SliderItem();
                    slider.SliderImage = item.SliderImage;
                    slider.SliderLink = item.SliderLink;
                    slider.SliderText = item.SliderText;
                    Slider.SliderItems.Add(slider);
                }
            }
        }

        public List<Slider.SliderItem> GetSlide()
        {
            LoadSliderItem();
            return Slider.SliderItems;
        }


        public bool PutSlide(List<Slider.SliderItem> sliderItems)
        {
            String token = Utility.GetToken();

            Session session = _sessionHandler.AuthenSession(token);

            if (session.RightList.Exists(right => right.RightName == "admin"))
            {
                Slider.SliderItems = sliderItems;
                return true;
            }
            return false;
        }
    }
}
