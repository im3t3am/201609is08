﻿using System.Collections.Generic;
using System.Web.WebSockets;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;
using Lucene.Net.Support;

namespace HozyService.Services
{
    public class SearchService : ISearchService
    {
        private SearchHandler searchHandler;
        private TopicHandler topicHandler;
        private QuestionHandler questionHandler;
        private UserHandler userHandler;

        public SearchService()
        {
            userHandler = new UserHandler();
            topicHandler = new TopicHandler();
            searchHandler = new SearchHandler();
            questionHandler = new QuestionHandler();
        }

        public List<TopicDto> SearchTopic(string query)
        {
            List<Topic> resultTopics = searchHandler.SearchTopic(query);
            List<TopicDto> resultTopicDtos = new List<TopicDto>();
            foreach (var topic in resultTopics)
            {
                TopicDto topicDto = new TopicDto();
                topicDto = topicHandler.ConvertTopicToDto(topic);
                resultTopicDtos.Add(topicDto);
            }
            Utility.Log4Net(1, "Return Search Result!!!");
            return resultTopicDtos;
        }

        public List<QuestionDto> SearchQuestion(string query)
        {
            List<QuestionDto> resultQuestionDtos = new List<QuestionDto>();
            List<Question> resultQuestions = searchHandler.SearchQuestions(query);
            foreach (var resultQuestion in resultQuestions)
            {
                QuestionDto questionDto = new QuestionDto();
                questionDto = questionHandler.ConvertQuestionToDto(resultQuestion, topicHandler,new AnswerHandler());
                resultQuestionDtos.Add(questionDto);
            }
            return resultQuestionDtos;;
        }

        public List<UserSimpleDto> SearchUser(string query)
        {
            List<User> resultUsers = new List<User>();
            List<UserSimpleDto> resultUserDtos = new List<UserSimpleDto>();
            resultUsers = searchHandler.SearchUser(query);
            foreach (var resultUser in resultUsers)
            {
                UserSimpleDto userDto = new UserSimpleDto();
                userDto = userHandler.ConvertUserSimpleDto(resultUser);
                resultUserDtos.Add(userDto);
            }
            return resultUserDtos;
        }
    }
}
