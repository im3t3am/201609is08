﻿using System;
using System.Collections.Generic;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;


namespace HozyService.Services
{
    public class ReportService : IReportService
    {
        private SessionHandler sessionHandler = new SessionHandler();
        private ReportHandler reportHandler = new ReportHandler();
      
        public ReportDto AddReport(ReportCreateDto reportCreateDto)
        {
            String token = Utility.GetToken();
            Session currentSession = sessionHandler.AuthenSession(token);
            return reportHandler.AddReport(reportCreateDto, currentSession);
        }

        public List<ReportDto> GetReport(string id)
        {
            String token = Utility.GetToken();
            Session currentSession = sessionHandler.AuthenSession(token);
            return reportHandler.GetReport(int.Parse(id), currentSession);
        }
    }
}
