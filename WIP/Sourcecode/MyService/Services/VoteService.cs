﻿using System.Net;
using HozyDb.Entities;
using HozyDb.Utilities;
using HozyService.DTOs;
using HozyService.Handlers;

namespace HozyService.Services
{
    /// <summary>
    /// Service cho hệ thống voting
    /// </summary>
    public class VoteService : IVoteService
    {
        enum VoteTargets
        {
            Question = 1,
            Answer = 2
        }

        SessionHandler sessionHandler = new SessionHandler();
        VoteHandler voteHandler = new VoteHandler();

        public bool AddVote(VoteDto voteDto)
        {
            string token = Utility.GetToken();
            
            Session currentSession = sessionHandler.AuthenSession(token);
            
            if (voteDto.VoteTarget == (int) VoteTargets.Question)
            {
                return voteHandler.VoteQuestion(voteDto,currentSession);
            }
            if (voteDto.VoteTarget == (int) VoteTargets.Answer)
            {
                return voteHandler.VoteAnswer(voteDto, currentSession);
            }
            else
            {
                Utility.Log4Net(0, "Vote to Question or Answer FAIL!");
                Utility.SetRespond(HttpStatusCode.BadRequest, "WrongInputFormat");
                return false;
            }
        }
    }
}
