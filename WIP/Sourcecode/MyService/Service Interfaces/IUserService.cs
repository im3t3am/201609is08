﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyDb.Entities;
using HozyService.DTOs;

namespace HozyService.Services
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "topuser"
            )]
        List<UserSimpleDto> GetTopUser();

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user/{id}"
            )]
        UserDto GetUserProfile(string id);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user"
            )]
        UserDto CreateUser(UserDto user);

        [OperationContract]
        [WebInvoke(

            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "changepw/{id}"
            )]
        UserDto ChangePassword(UserDto userDto, string id);

        [OperationContract]
        [WebInvoke(

            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "resetpw"
            )]
        UserDto ResetPassword(UserDto userDto);

        [OperationContract]
        [WebInvoke(

            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "user/{id}"
            )]
        UserDto UpdateUserProfile(UserDto userDto, string id);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "feed/{id}/{skip}"
            )]
        List<QuestionDto> GetFeed(string id,string skip);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "newquestion/{id}/{skip}"
            )]
        List<QuestionDto> UserGetNewQuestion(string id, string skip);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getquestionofuser/{id}"
            )]
        List<QuestionDto> GetQuestionOfUser(string id);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getanswerofuser/{id}"
            )]
        List<AnswerDto> GetAnswerOfUser(string id);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "followingquestion/{id}"
            )]
        List<QuestionDto> GetFollowingQuestion(string id);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "facebook"
            )]
        UserDto GetFacebookUrl();

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "newUserFb?code={token}"
            )]
        Tuple<UserDto,SessionDto> FacebookCallBack(string token);
    }


}
