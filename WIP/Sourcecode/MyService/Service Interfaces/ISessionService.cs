﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;

namespace HozyService.Services
{
    [ServiceContract]
    public interface ISessionService
    {

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "session"
            )]
        SessionDto OpenSession(SessionRequestDto sessionRequestDto);

        [OperationContract]
        [WebInvoke(
            Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "session"
            )]
        bool DeleteSession();
    }
}
