﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;

namespace HozyService.Services
{
    /// <summary>
    /// Interface được tạo ra để định nghĩa các hàm cho việc voting
    /// </summary>

    [ServiceContract]
    public interface IAnalyticService
    {
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "topiclog/{amount}/{skip}"
            )]
        List<TopTopicLogDto> TopicLog(string amount, string skip);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "questionlog/{amount}/{skip}"
            )]
        List<TopQuestionLogDto> QuestionLog(string amount, string skip);
    }
}
