﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;

namespace HozyService.Services
{
    /// <summary>
    /// Interface được tạo ra để định nghĩa các hàm cho việc voting
    /// </summary>

    [ServiceContract]
    public interface IAmaService
    {
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "ama"
            )]
        int CreateAma(AmaCreationDto amaCreationDto);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "ama/question"
            )]
        int AddQuestion(AmaQuestionDto amaQuestionDto);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "ama/question/{id}"
            )]
        int UpdateQuestion(AmaQuestionDto amaQuestionDto,string id);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "ama/answer"
            )]
        int AddAnswer(AmaAnswerCreationDto amaAnswerCreationDto);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "ama/{id}"
            )]
        AmaDto GetAma(string id);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "amalist/{skip}"
            )]
        List<AmaDto> GetAmaList(string skip);
    }
}
