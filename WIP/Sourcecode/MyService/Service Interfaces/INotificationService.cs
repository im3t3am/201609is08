﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using HozyDb.Entities;
using HozyService.DTOs;

namespace HozyService.Services
{
    // ReSharper disable once InconsistentNaming
    [ServiceContract]
    public interface INotificationService
    {
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "notification"
            )]
        List<NotificationDto> GetNotification();

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "slide"
            )]
        List<Slider.SliderItem> GetSlide();

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "slide"
            )]
        bool PutSlide(List<Slider.SliderItem> sliderItems);
    }
}
