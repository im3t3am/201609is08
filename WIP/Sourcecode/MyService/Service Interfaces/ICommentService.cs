﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;

namespace HozyService.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICommentList" in both code and config file together.
    [ServiceContract]
    public interface ICommentService
    {
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "comment/{id}"
            )]
        List<CommentDto> GetComment(string id);
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "comment"
            )]
        CommentDto AddComment(CommentDto commentDto);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "updatecomment/{id}"
            )]
        CommentDto UpdateComment(CommentDto commentDto, string id);

        [OperationContract]
        [WebInvoke(
            Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "comment/{id}"
            )]
        bool DeleteComment(string id);
    }
}
