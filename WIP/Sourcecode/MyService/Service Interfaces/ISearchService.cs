﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;

namespace HozyService.Services
{
    [ServiceContract]
    public interface ISearchService
    {
        /// <summary>
        /// Tạm thời viết 1 hàm search (chưa implement Full text để ghép với Front)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "searchtopic/{query}"
            )]
        List<TopicDto> SearchTopic(string query);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "searchquestion/{query}"
            )]
        List<QuestionDto> SearchQuestion(string query);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "searchuser/{query}"
            )]
        List<UserSimpleDto> SearchUser(string query);

    }
}