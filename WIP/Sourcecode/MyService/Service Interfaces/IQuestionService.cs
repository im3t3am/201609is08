﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Script.Services;
using HozyDb.Entities;
using HozyService.DTOs;

namespace HozyService.Services
{
    [ServiceContract]
    public interface IQuestionService
    {
        /// <summary>
        /// Get a Single Question with Id, Detail of question with Comment List
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "question/{id}"
            )]
        QuestionDto GetQuestion(string id);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "related/{id}"
            )]
        List<QuestionDto> GetRelatedQuestion(string id);

        /// <summary>
        /// Post a Single question and return it.
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "question"
            )]
        QuestionDto AddQuestion(QuestionDto questionDto);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "updatequestion/{id}"
            )]  
        bool UpdateQuestion(QuestionDto questionDto, string id);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "topquestions/{skip}"
            )]
        List<QuestionDto> GetTopQuestion(string skip);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "newquestions/{skip}"
            )]
        List<QuestionDto> GetNewQuestion(string skip);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "addquestionsubscription/{questionid}"
            )]
        bool AddQuestionSubscription(string questionid);

        [OperationContract]
        [WebInvoke(
            Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "removequestionsubscription/{questionid}"
            )]
        bool RemoveQuestionSubscription(string questionid);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "listquestionfollowed" 
            )] 
        List<QuestionDto> ListQuestionFollowed();  // cua 1 thang user 

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "listuserfollowed/{qid}" 
            )]
        List<UserDto> ListUserFollowed(string qid);   
        
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "questionreported"
            )]
        List<QuestionDto> GetQuestionReported();

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "isfollow/{questionid}"
            )] 
        bool IsFollow(string questionid);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "isreported/{questionid}"
            )] 
        bool IsReported(string questionid);

        [OperationContract]
        [WebInvoke(
            Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "question/{id}"
            )]
        bool DeleteQuestion(string id);
    }
}
