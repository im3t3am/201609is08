﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using HozyDb.Entities;
using HozyService.DTOs;

namespace HozyService.Services
{
    [ServiceContract]
    public interface ITopicService
    {

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "gettopic/{topicid}"
            )]
        TopicDto GetDetailTopic(string topicid);    

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "topic?topicid={topicid}"
            )]
        bool AddUserTopic(string topicid);   

        
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "addtopic"
            )]
        TopicDto AddTopic(TopicDto topicDto); 
         

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "addquestiontopic?topicid={topicid}&questionid={questionid}"
            )] 
        bool AddQuestionTopic(string topicid,string questionid);


        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getusertopic/{userid}"
            )]
        List<TopicDto> GetUserTopic(string userid); 

        /// <summary>
        /// Test method
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "unfollow?topicid={topicid}"
            )]
        bool UserUnfollowTopic(string topicid);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "questionunfollow?topicid={topicid}&questionid={questionid}"
            )]
        bool QuestionUnfollowTopic(string topicid, string questionid);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "newquestion/{topicId}/{skip}"
            )]
        List<QuestionDto> GetTopicNewQuestion(string topicId, string skip);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "topquestion/{topicId}/{skip}"
            )]
        List<QuestionDto> GetTopicTopQuestion(string topicId, string skip);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "hottopic"
            )]
        List<TopicDto> GetHotTopic();

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "random/{amount}"
            )]
        List<TopicDto> GetRandomTopic(string amount);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "updatetopic/{id}"
            )]
        TopicDto UpdateTopic(TopicDto topicDto,string id);
    }
}
