﻿using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;

namespace HozyService.Services
{
    /// <summary>
    /// Interface được tạo ra để định nghĩa các hàm cho việc voting
    /// </summary>

    [ServiceContract]
    public interface IVoteService
    {
        /// <summary>
        /// Để có thể tạo 1 vote ta cần gì? - Người tạo vote đó. Giá trị của Vote, Vote được gắn với Q/A nào?
        /// Ta luôn get được người tạo vote thông qua Token/session
        /// Các input khác, xem VoteDto
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "vote"
            )]
        bool AddVote(VoteDto voteDto);
    }
}
