﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;


namespace HozyService.Services
{
    [ServiceContract]
    public interface IReportService
    {
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "report"
            )]
         ReportDto AddReport(ReportCreateDto reportCreateDto);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "listreport/{id}"
            )]
        List<ReportDto> GetReport(string id);
    }
}
