﻿
 using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using HozyService.DTOs;

namespace HozyService.Services
{
    [ServiceContract]
    public interface IAnswerService
    {
        /// <summary>
        /// Get list of a Answer when have QuestionId 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "answer/{id}"
            )]
        List<AnswerDto> GetAnswer(string id);

        /// <summary>
        /// Add An Answer when have QuestionId, UserID and AnswerContent
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "addanswer"
            )]
        AnswerDto AddAnswer(AnswerCreateDto answerCreateDto);

        [OperationContract]
        [WebInvoke(
            Method = "DELETE",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "answer/{id}"
            )]
        bool DeleteAnswer(string id);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "updateanswer"
            )]
        AnswerDto UpdateAnswer(AnswerDto answerDto);

        [OperationContract]
        [WebInvoke(
            Method = "PUT",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "markhelpful/{answerid}"
            )]
        bool MarkHelpful(string answerId);
    }
}
