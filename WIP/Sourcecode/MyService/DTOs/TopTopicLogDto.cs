﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract(Name = "toptopiclog")]
    public class TopTopicLogDto
    {
        [DataMember(Name = "createtime", EmitDefaultValue = false)]
        public String CreationDate { get; set; }

        [DataMember(Name = "toptopics", EmitDefaultValue = false)]
        public List<TopicDto> TopTopic { get; set; } 
    }
}
