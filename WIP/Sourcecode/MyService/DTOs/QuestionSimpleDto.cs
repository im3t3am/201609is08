﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract(Name = "question")]
    public class QuestionSimpleDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "title", EmitDefaultValue = false)]
        public String QuestionTitle { get; set; }
    }
}
