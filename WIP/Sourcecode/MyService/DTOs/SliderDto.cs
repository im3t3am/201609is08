﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    [DataContract(Name = "slider")]
    public static class Slider
    {
        [DataMember(Name = "sliders", EmitDefaultValue = false)]
        public static List<SliderItem> SliderItems = new List<SliderItem>();

        [DataContract(Name = "slideritem")]
        public class SliderItem
        {
            [DataMember(Name = "sliderimage", EmitDefaultValue = false)]
            public string SliderImage { get; set; }
            [DataMember(Name = "sliderlink", EmitDefaultValue = false)]
            public string SliderLink { get; set; }
            [DataMember(Name = "slidertext", EmitDefaultValue = false)]
            public string SliderText { get; set; }
        }
    }
}
