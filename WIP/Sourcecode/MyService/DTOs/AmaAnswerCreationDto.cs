﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class AmaAnswerCreationDto
    {
        [DataMember(Name = "amaquestionid", EmitDefaultValue = false)]
        public int AmaQuestionId { get; set; }

        [DataMember(Name = "answercontent", EmitDefaultValue = false)]
        public string AnswerContent { get; set; }
    }
}
