﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using HozyDb.Entities;

namespace HozyService.DTOs
{
    [DataContract(Name = "user")]
    public class UserSimpleDto
    {
        [DataMember(Name = "username", EmitDefaultValue = false)]
        public string Username { get; set; }

        [DataMember(Name = "displayname", EmitDefaultValue = false)]
        public string DisplayName { get; set; }

        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "avatar", EmitDefaultValue = false)]
        public String Avatar { get; set; }

    }
}
