﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class AmaCreationDto
    {
        [DataMember(Name = "userid", EmitDefaultValue = false)]
        public int Userid { get; set; }

        [DataMember(Name = "startdate", EmitDefaultValue = false)]
        public string UnixDateTime { get; set; }

        [DataMember(Name = "amaimage", EmitDefaultValue = false)]
        public string AmaImage { get; set; }
    }
}
