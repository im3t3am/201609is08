﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract(Name = "topquestion")]
    public class TopQuestionDto
    {
        [DataMember(Name = "question", EmitDefaultValue = false)]
        public QuestionDto QuestionDto;

        [DataMember(Name = "textanalytic", EmitDefaultValue = false)]
        public TextAnalyticDto TextAnalyticDto;
    }
}
