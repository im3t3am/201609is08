﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Security.Policy;
using HozyDb.Entities;

namespace HozyService.DTOs
{
    [DataContract(Name = "report")]
    public class ReportDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "reportcontent", EmitDefaultValue = false)]
        public String ReportContent { get; set; }

        [DataMember(Name = "creationdate", EmitDefaultValue = false)]
        public String CreationDate { get; set; }

        [DataMember(Name = "user", EmitDefaultValue = false)]
        public UserDto User { get; set; } 


    }
}
