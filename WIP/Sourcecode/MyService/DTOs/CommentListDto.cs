﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace VnaService.DTOs
{
    [DataContract]
      public class CommentListDto
    {
        [DataMember(Name = "commentid")]
        public int commentId { get; set; }

        [DataMember(Name = "commentcontent")]
        public String commentContent { get; set; }

        [DataMember(Name = "username")]
        public String userName { get; set; }

        [DataMember(Name = "userid")]
        public int userId { get; set; }

        [DataMember(Name = "ParentCmtId")]
        public int parentCmtId { get; set; }

        [DataMember(Name = "commentlist")]
        public List<CommentDto> CommentList { get; set; }

        [DataMember(Name = "user")]
        public UserDto User { get; set; } 
    }
}
