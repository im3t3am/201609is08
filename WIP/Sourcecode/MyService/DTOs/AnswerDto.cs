﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class AnswerDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int CommentId { get; set; }

        [DataMember(Name = "commentcontent", EmitDefaultValue = false)]
        public String CommentContent { get; set; }

        [DataMember(Name = "userid", EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(Name = "createtime", EmitDefaultValue = false)]
        public String CreationDate { get; set; }

        [DataMember(Name = "username", EmitDefaultValue = false)]
        public String UserName { get; set; }

        [DataMember(Name = "displayname", EmitDefaultValue = false)]
        public string DisplayName { get; set; }

        [DataMember(Name = "avatar", EmitDefaultValue = false)]
        public String Avatar { get; set; }

        [DataMember(Name = "question", EmitDefaultValue = false)]
        public QuestionSimpleDto QuestionSimple { get; set; }

        [DataMember(Name = "votecountup", EmitDefaultValue = true)]
        public int VoteCountUp { get; set; }

        [DataMember(Name = "votecountdown",EmitDefaultValue=true)]
        public int VoteCountDown { get; set; }

        [DataMember(Name = "helpful", EmitDefaultValue = true)]
        public bool isHelpful { get; set; }
    }
}
