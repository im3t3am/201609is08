﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using HozyDb.Entities;


namespace HozyService.DTOs
{
    [DataContract(Name = "topic")]
    public class TopicDto
    {
        [DataMember(Name = "topicid", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "topicimage", EmitDefaultValue = false)]
        public String Image { get; set; } 

        [DataMember(Name = "topicname", EmitDefaultValue = false)]
        public String Name { get; set; }

        [DataMember(Name = "topicdescription", EmitDefaultValue = false)]
        public String Description { get; set; }

        [DataMember(Name = "followcount", EmitDefaultValue = true)]
        public int Followcount { get; set; }
    }
}
