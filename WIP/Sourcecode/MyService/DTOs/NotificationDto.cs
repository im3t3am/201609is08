﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract(Name = "notification")]
    public class NotificationDto
    {
        [DataMember(Name = "notificationid", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "notificationdescription", EmitDefaultValue = false)]
        public String Description { get; set; }

        //mark that user is read this noti or not 
        [DataMember(Name = "ischecked", EmitDefaultValue = true)]
        public bool IsChecked { get; set; }

        [DataMember(Name = "creationdate", EmitDefaultValue = false)]
        public String CreationDate { get; set; }

        [DataMember(Name = "questionid", EmitDefaultValue = false)]
        public int QuestionId { get; set; }

        //userid cua thang co answer/upvote
        [DataMember(Name = "userid", EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(Name = "displayname", EmitDefaultValue = false)]
        public string DisplayName { get; set; }
         
        [DataMember(Name = "avatar", EmitDefaultValue = false)]
        public string Avatar { get; set; }
    }
}
