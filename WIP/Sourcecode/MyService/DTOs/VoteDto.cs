﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class VoteDto
    {
        // Vote chỉ có thể được đưa vào Question (1) hoặc Answer (2)
        [DataMember(Name = "target", EmitDefaultValue = false)]
        public int VoteTarget { get; set; }

        // Id của mục tiêu vote : Kết hợp với Vote Target sẽ có được QuestionId hoặc VoteId
        [DataMember(Name = "targetid", EmitDefaultValue = false)]
        public int VoteTargetId { get; set; }

        // Giá trị của Vote : True = +1 , False = -1
        [DataMember(Name = "value", EmitDefaultValue = false)]
        public bool VoteValue { get; set; }
    }
}
