﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class AmaDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "owner", EmitDefaultValue = false)]
        public UserSimpleDto AmaOwner { get; set; }

        [DataMember(Name = "opendate", EmitDefaultValue = false)]
        public string OpenDate { get; set; }

        [DataMember(Name = "amaquestions", EmitDefaultValue = false)]
        public List<AmaQuestionDto> AmaQuestionDtos { get; set; }

        [DataMember(Name = "amaimage", EmitDefaultValue = false)]
        public string AmaImage { get; set; }
    }
}
