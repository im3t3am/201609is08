﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract(Name = "textanalytic")]
    public class TextAnalyticDto
    {
        [DataMember(Name = "sentiment", EmitDefaultValue = false)] 
        public string Sentiment;

        [DataMember(Name = "agreement", EmitDefaultValue = false)]
        public string Agreement;

        [DataMember(Name = "subjectivity", EmitDefaultValue = false)]
        public string Subjectivity;
    }
}
