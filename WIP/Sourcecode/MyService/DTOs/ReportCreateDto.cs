﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class ReportCreateDto
    {
        [DataMember(Name = "reportcontent", EmitDefaultValue = false)]
        public String ReportContent { get; set; }

        [DataMember(Name = "questionid", EmitDefaultValue = false)]
        public int QuestionId { get; set; }

    }
}
