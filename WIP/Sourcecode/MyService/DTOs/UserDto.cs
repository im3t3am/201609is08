﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using HozyDb.Entities;

namespace HozyService.DTOs
{
    [DataContract(Name="user")]
    public class UserDto
    {
        [DataMember(Name = "username", EmitDefaultValue = false)]
        public string Username { get; set; }

        [DataMember(Name = "displayname", EmitDefaultValue = false)]
        public string DisplayName { get; set; }

        [DataMember(Name = "password", EmitDefaultValue = false)]
        public string Password { get; set; }

        [DataMember(Name = "newpassword", EmitDefaultValue = false)]
        public string NewPassword { get; set; }

        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "rights", EmitDefaultValue = false)]
        public List<String> RightList { get; set; }

        [DataMember(Name = "description", EmitDefaultValue = false)]
        public String Description { get; set; }

        [DataMember(Name = "dateofbirth", EmitDefaultValue = false)]
        public String DateOfBirth { get; set; }

        [DataMember(Name = "email", EmitDefaultValue = false)]
        public String Email { get; set; }

        [DataMember(Name = "address", EmitDefaultValue = false)]
        public String Address { get; set; }

        [DataMember(Name = "website", EmitDefaultValue = false)]
        public String Website { get; set; }

        [DataMember(Name = "employment", EmitDefaultValue = false)]
        public String Employment { get; set; }

        [DataMember(Name = "education", EmitDefaultValue = false)]
        public String Education { get; set; }

        [DataMember(Name = "avatar", EmitDefaultValue = false)]
        public String Avatar { get; set; }

        [DataMember(Name = "createddate", EmitDefaultValue = false)]
        public DateTime CreatedDate { get; set; }

        [DataMember(Name = "lastlogin", EmitDefaultValue = false)]
        public DateTime LastLogin { get; set; }


        [DataMember(Name = "facebookurl", EmitDefaultValue = false)]
        public String FacebookUrl { get; set; }

        [DataMember(Name = "cover", EmitDefaultValue = false)]
        public String Cover { get; set; }

    }
}
