﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using HozyDb.Entities;

namespace HozyService.DTOs
{
    [DataContract(Name = "clientsession")]
    public class SessionDto
    {
        [DataMember(Name = "userid", EmitDefaultValue = false)]
        public String UserId { get; set; }
        [DataMember(Name = "username", EmitDefaultValue = false)]
        public String Username { get; set; }
        [DataMember(Name = "token", EmitDefaultValue = false)]
        public String Token { get; set; }
        [DataMember(Name = "rights", EmitDefaultValue = false)]
        public List<string> RightList { get; set; } 
    }
}
