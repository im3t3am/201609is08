﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class AnswerCreateDto
    {
        [DataMember(Name = "commentcontent", EmitDefaultValue = false)]
        public String CommentContent { get; set; }

        [DataMember(Name = "questionid", EmitDefaultValue = false)]
        public int QuestionId { get; set; }
    }
}
