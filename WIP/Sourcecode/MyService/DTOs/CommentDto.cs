﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class CommentDto
    {
            [DataMember(Name = "id", EmitDefaultValue = false)]
            public int CommentId { get; set; }

            [DataMember(Name = "commentcontent", EmitDefaultValue = false)]
            public String CommentContent { get; set; }

            [DataMember(Name = "userid", EmitDefaultValue = false)]
            public int UserId { get; set; }

            [DataMember(Name = "username", EmitDefaultValue = false)]
            public String UserName { get; set; }

            [DataMember(Name = "displayname", EmitDefaultValue = false)]
            public string DisplayName { get; set; }

            [DataMember(Name = "avatar", EmitDefaultValue = false)]
            public String Avatar { get; set; }

            [DataMember(Name = "parentcmtid", EmitDefaultValue = false)]
            public int ParentCmtId { get; set; }

            [DataMember(Name = "creatime", EmitDefaultValue = false)]
            public String CreationDate { get; set; }


    }
}
