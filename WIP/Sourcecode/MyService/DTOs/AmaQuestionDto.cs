﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract]
    public class AmaQuestionDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "amasessionid", EmitDefaultValue = false)]
        public int SessionId { get; set; }

        [DataMember(Name = "questioncreationdate", EmitDefaultValue = false)]
        public string QuestionCreationDate { get; set; }

        [DataMember(Name = "answercreationdate", EmitDefaultValue = false)]
        public string AnswerCreationDate { get; set; }

        [DataMember(Name = "asker", EmitDefaultValue = false)]
        public UserSimpleDto AskerDto { get; set; }

        [DataMember(Name = "content", EmitDefaultValue = false)]
        public string QuestionContent { get; set; }

        [DataMember(Name = "answerid", EmitDefaultValue = false)]
        public int AnswerId { get; set; }

        [DataMember(Name = "answercontent", EmitDefaultValue = false)]
        public string AnswerContent { get; set; }
    }
}
