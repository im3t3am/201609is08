﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyService.DTOs
{
    [DataContract(Name = "topquestionlog")]
    public class TopQuestionLogDto
    {
        [DataMember(Name = "questions", EmitDefaultValue = false)]
        public List<TopQuestionDto> topQuestionDtos;

        [DataMember(Name = "createtime", EmitDefaultValue = false)]
        public string CreationDate;

        [DataMember(Name = "questioncount", EmitDefaultValue = false)]
        public int QuestionCountToDate;

        [DataMember(Name = "commentanswercount", EmitDefaultValue = false)]
        public int CommentAnswerCountToDate;
    }
}
