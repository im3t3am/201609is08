﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Security.Policy;
using HozyDb.Entities;

namespace HozyService.DTOs
{
    [DataContract(Name = "question")]
    public class QuestionDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int Id { get; set; }

        [DataMember(Name = "title", EmitDefaultValue = false)]
        public String QuestionTitle { get; set; }

        [DataMember(Name = "content", EmitDefaultValue = false)]
        public String QuestionContent { get; set; }

        [DataMember(Name = "createtime", EmitDefaultValue = false)]
        public String CreationDate { get; set; }

        [DataMember(Name = "isreported", EmitDefaultValue = false)]
        public Boolean IsReported { get; set; }

        //Bỏ CommentList bởi vì sẽ được trả ra từ AnswerService
        [DataMember(Name = "commentlist", EmitDefaultValue = false)]
        public List<CommentDto> CommentList { get; set; }

        [DataMember(Name = "user", EmitDefaultValue = false)]
        public UserDto User { get; set; } 

        [DataMember(Name = "votecountup", EmitDefaultValue = true)]
        public int VoteCountUp { get; set; }

        [DataMember(Name = "votecountdown", EmitDefaultValue = true)]
        public int VoteCountDown { get; set; }

        [DataMember(Name = "topics", EmitDefaultValue = false)]
        public List<TopicDto> Topics { get; set; }

        [DataMember(Name = "viewcount", EmitDefaultValue = true)]
        public int ViewCount { get; set; }

        [DataMember(Name = "isdeleted", EmitDefaultValue = false)]
        public bool isDeleted { get; set; }

        //count how many users are following this question
        [DataMember(Name = "followcount")]
        public int FollowCount { get; set; }

        [DataMember(Name = "bestanswer")]
        public AnswerDto BestAnswer { get; set; }
    }
}
