﻿var app = angular.module("hozy", ['ui.router', 'ngMessages', 'ui.bootstrap', 'lgm', 'ngCookies', 'hozy.config','ng-fusioncharts'])
.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    //flowFactoryProvider.defaults = {
    //    target: 'upload.php',
    //    permanentErrors: [404, 500, 501],
    //    maxChunkRetries: 1,
    //    chunkRetryInterval: 5000,
    //    simultaneousUploads: 4
    //};
    //flowFactoryProvider.on('catchAll', function (event) {
    //    console.log('catchAll', arguments);
    //});
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
    $urlRouterProvider.otherwise('/index');
    $stateProvider
     .state('index', {
         url: '/index',
         templateUrl: 'homepage.html',
          resolve: {
          delay: function($q, $timeout) {
            var delay = $q.defer();
            $timeout(delay.resolve, 500);
            return delay.promise;
          }
        }

     })
     .state('questionDetail', {
        url: '/questionDetail/:qid',
        templateUrl: 'questionDetail.html',
        resolve: {
          delay: function($q, $timeout) {
            var delay = $q.defer();
            $timeout(delay.resolve, 500);
            return delay.promise;
          }
        }

     }).state('facebook', {
        url: '/facebook',
        controller:'loginFacebookController',
        templateUrl: ''

     })
     .state('topicDetail', {
        url:'/topicDetail/:tid',
        templateUrl: 'topic.html',
        resolve: {
          delay: function($q, $timeout) {
            var delay = $q.defer();
            $timeout(delay.resolve, 500);
            return delay.promise;
          }
        }
     })
     .state('ama', {
        url:'/ama/:amaid',
        templateUrl: 'AMA.html',
        resolve: {
          delay: function($q, $timeout) {
            var delay = $q.defer();
            $timeout(delay.resolve, 500);
            return delay.promise;
          }
        }
     })
     .state('profile', {
        url:'/profile/:uid',
        templateUrl: 'profile.html',
        resolve: {
          delay: function($q, $timeout) {
            var delay = $q.defer();
            $timeout(delay.resolve, 500);
            return delay.promise;
          }
        }
     })
    .state('changepassword', {
        url: '/changepassword/:uid',
        controller: 'changePassController',
        templateUrl: 'changepassword.html',
        resolve: {
            delay: function ($q, $timeout) {
                var delay = $q.defer();
                $timeout(delay.resolve, 500);
                return delay.promise;
            }
        }
    })
    .state('analytics', {
        url: '/analytics',
        templateUrl: 'analytics.html',
        resolve: {
            delay: function ($q, $timeout) {
                var delay = $q.defer();
                $timeout(delay.resolve, 500);
                return delay.promise;
            }
        }
    })
    .state('changepwsuccess', {
        url: '/changesuccess',
        controller: 'changePassController',
        templateUrl: 'successpage.html'

    })
    .state('errorPage', {
        url: '/errorPage',
        templateUrl: 'ErrorPage.html'

    })
    .state('amaList', {
        url: '/amaList',
        templateUrl: 'amaList.html'

    })
    .state('search', {
        url: '/search/:keyword',
        templateUrl: 'searchResult.html',
        resolve: {
        delay: function ($q, $timeout) {
            var delay = $q.defer();
            $timeout(delay.resolve, 500);
            return delay.promise;
            }
        }
    });

}])
.run(['$rootScope', '$state',function($rootScope, $state){

  $rootScope.$on('$stateChangeStart',function(){
      $rootScope.stateIsLoading = true;
 });


  $rootScope.$on('$stateChangeSuccess',function(){
      $rootScope.stateIsLoading = false;
 });

}])
.service('loginService', function () {
    this.getValue = function () {
        return this.token;
    };

    this.setValue = function (newValue) {
        this.token = newValue;
    }
})

.controller("loginController", ['$window', '$timeout', '$scope', '$http','$state','$cookies', 'loginService', 'hozy.config', function ($window, $timeout, $scope, $http, $state, $cookies,loginService,hozyConfig) {
    //loginform
    $scope.errorMsg = "invalid";
    $scope.showInvalid = false; 
    $scope.goRegister = function () {
       $('body').css('padding-right', '0');
       $('#login').modal('hide');
    }
    $scope.login = function () {
        $scope.submitted = true;
        $scope.invalidUser = false;
        var url =  "../../Services/SessionService.svc/session";
        var data = { "password": $scope.password, "username": $scope.username };
        var dataJSON = JSON.stringify(data);
        $http.post(url, dataJSON).
       success(function (data, status, headers, config) {
           $cookies.put('token', data.token);
           $cookies.put('rights', 'user');
           angular.forEach(data.rights, function (value, key) {
               if (value == 'admin') {
                   $cookies.put('rights', 'admin');
               }
           })
           
           $cookies.put('id', data.userid);
           $cookies.put('username', data.username);
           $('#login').modal('hide');
           $timeout(function () {
               location.reload();
           }, 1000);
       }).
       error(function (data, status, headers, config) {
           $scope.invalidUser = true; 
       });
        
    }
    //login fb
    $scope.loginFacebook = function () {
        var url = "../../Services/userService.svc/facebook";
        $http.get(url).
            success(function (data, status, headers, config) {
                var fbUrl = data.facebookurl;
                $window.location.href = fbUrl;
            }).
            error(function (data, status, headers, config) {

            });
    }
    

}])

.controller("register", ['$window', '$timeout', '$scope', '$http', '$cookies', 'hozy.config', function ($window, $timeout, $scope, $http, $cookies, hozyConfig) {
    
    $scope.demo = "aaaaaa";

    $scope.signUp = function () {
        // alert($scope.email);

        var jsonRegister = {
            "name": $scope.name,
            "email": $scope.email,
            "password": $scope.password
        }
       
        $timeout(function () {
            $window.location.href = '../CssTranning/test.html';
        }, 1000);

    }
    $scope.goLogin = function () {
        $('#register').modal('hide');
        $('body').css('padding-right', '0');
    }
    //call register sv
    $scope.register = function () {
        var urlRegister = "../../Services/UserService.svc/user";
        var urlLogin = "../../Services/SessionService.svc/session";
        var data = { "username": $scope.id, "displayname":$scope.name};
        var dataJSON = JSON.stringify(data);
        $http.post(urlRegister, dataJSON).
        success(function (data, status, headers, config) {
            alert('Đăng kí thành công, hãy kiểm tra email để nhận mật khẩu của bạn.');
            $('#register').modal('hide');

        }).
        error(function (data, status, headers, config) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
            $scope.showInvalid = true;
            $scope.isExist = true;
            $('#errMsg').delay(1000).fadeOut();
        });
    }
    $scope.resetPass = function () {
        var url = "../../Services/UserService.svc/resetpw";
        var data = { username: $scope.emailrs };
        var dataJson = JSON.stringify(data);
        $http.put(url,dataJson).
            success(function (data, status, headers, config) {
                alert("Chúng tôi đã gửi mật khẩu mới đến email của bạn. Vui lòng kiểm tra để nhận mật khẩu mới.");
                $('#resetpass').modal('hide');
                $('#login').modal('show');
            }).
            error(function (data, status, headers, config) {
                alert("Bạn đã nhập sai email. Vui lòng nhập lại.");
            });
    }
}])
.controller("questionDetailController", ['$scope', '$rootScope', '$cookies', '$state', '$http', '$stateParams', 'hozy.config', 'AvatarPhoto', '$timeout', function ($scope, $rootScope, $cookies, $state, $http, $stateParams, hozyConfig, AvatarPhoto, $timeout) {
    //alert($stateParams);
    // $scope.getContent = function(){
    //   var a = $('#edit').froalaEditor('html.get');
    //   alert(a);
    // }
    var url = "https://hozy.cf/froalaapi/s3signature";
    $http.get(url).
        success(function (data, status, headers, config) {
            var configFroala = data;
                $('.edit').froalaEditor({
                theme: 'gray',
                heightMin: 100,
                toolbarButtons: ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'align', 'outdent', 'indent', 'html'],
                toolbarButtonsXS: ['undo', 'redo', '-', 'bold', 'italic', 'underline'],
                imageUploadToS3: configFroala
            });
        }).
        error(function (data, status, headers, config) {

        });
    $scope.showDetail = false;
    $scope.mouseover = function (id) {
        var url = "../../Services/TopicService.svc/gettopic/" + id;
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.tooltip = data.topicdescription;
            }).
            error(function (data, status, headers, config) {

            });
    };

    $scope.mouseout = function () {
        $scope.tooltip = "";
    };
    var configHeader = {
        headers: {
            'Token':$cookies.get('token')
        }
    };
    $scope.uid = parseInt($cookies.get('id'));
    var Qid = $stateParams.qid;
    $scope.qid = $stateParams.qid;
    $scope.OwnOrAdmin = function(userId){
        if(userId === $scope.uid || $cookies.get('rights') == "admin"){
            return true;
        }
        else{
            return false;
        } 
    }
    

    $scope.linkTopicDetail = function(topicId){
       $state.go('topicDetail', {tid:topicId});
    }
    if (typeof ($cookies.get('id')) != 'undefined' && $cookies.get('id') != null) {
        $scope.logged = true;
        $scope.displayName = AvatarPhoto.getName();
        $scope.Avatar = AvatarPhoto.getAvatar();
        var urlIsFollow = "../../Services/QuestionService.svc/isfollow/" + Qid;
        $http.get(urlIsFollow,configHeader).
            success(function (data, status, headers, config) {
                    $scope.isFollowed = data;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        var urlIsReported = "../../Services/QuestionService.svc/isreported/" + Qid;
        $http.get(urlIsReported, configHeader).
            success(function (data, status, headers, config) {
                $scope.isReported = data;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

    } else {
        $scope.logged = false;
    }
    
    $scope.commentList = [];
    $scope.editMode = [];
    $scope.listTopic = [];
    $scope.showTopicTooltip = false;
    var url =  "../../Services/QuestionService.svc/question/" + Qid;
    $http.get(url).
        success(function (data, status, headers, config) {
            if (typeof (data.isdeleted) == 'undefined') {
                $scope.questionInfo = data;
                $scope.userid = data.user.id;
                $scope.userid === $scope.uid ? $scope.isOwn = true : $scope.isOwn = false;
                $cookies.get('rights') == "admin" ? $scope.isAdmin = true : $scope.isAdmin = false;
                        
                
                $scope.totalQuestionVoted = data.votecountup - data.votecountdown;
                $("#questionContent").html($scope.questionInfo.content);
                $scope.listTopic = data.topics;
                if($scope.listTopic.length == 0){
                    $scope.showTopicTooltip = true;
                }
                else {
                    $scope.showTopicTooltip = false;
                }
                $scope.showDetail = true;
            } else {
                $state.go('errorPage');
            }
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            $state.go('errorPage');
        });
    
    $scope.isAnswered = false;
    $scope.isBadAnswer = [];
    var url2 = "../../Services/AnswerService.svc/answer/" + Qid;
    $http.get(url2).
        success(function (data2, status, headers, config) {
            
            $scope.data = data2;
            $scope.data.sort(compare);
            if($scope.data.length > 100){
                $scope.answerCount = "100+";
            }else{
                $scope.answerCount = $scope.data.length;
            }
            
            angular.forEach(data2, function (value, key) {
                if (value.userid == $scope.uid) {
                    $scope.isAnswered = true;
                }
                if(value.votecountup - value.votecountdown < -3){
                    $scope.isBadAnswer.push(true);
                }else{
                    $scope.isBadAnswer.push(false);
                }
                $scope.commentList.push([]);
                $scope.editMode.push(false);
            })
        }).
        error(function (data2, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    function compare(a, b) {
        if ((a.votecountup - a.votecountdown) < (b.votecountup - b.votecountdown)) 
            return 1;
       
        if ((a.votecountup - a.votecountdown) > (b.votecountup - b.votecountdown))
            return -1;
        return 0;
    }
    $scope.cutImg = function (obj) {
        if (obj != undefined) {
            if (obj.indexOf("img") == -1) {
                return 0;
            } else {
                var v = obj.substring(obj.indexOf("src") + 5, obj.length)
                var imgLink = v.substring(0, v.indexOf(" ") - 1);
                return imgLink;
            }
        }
    }
    //$scope.cutString = function ($index,str) {
    //    if(
    //}
    $scope.recursive = function (str) {
        if (str.indexOf("</p>") > str.indexOf("img") && str.indexOf("img") != -1) {
            var returnstr = str.substring(str.indexOf("</p>") + 4, str.length);
            return $scope.recursive(returnstr);
        } else {
            var returnstr = str.substring(0, str.indexOf("</p>") + 4);
            var havesm;
            str.indexOf("</p>") + 4 < str.length ? havesm = true : havesm = false;
            return { st: returnstr, havesm: havesm };
        }
    }
    $scope.imgFeed = [];
    $scope.strFeed = [];
    var url = "../../Services/QuestionService.svc/topquestions/0";
    $http.get(url).
        success(function (datafeed, status, headers, config) {
            $scope.feed = [];
            angular.forEach(datafeed, function (value, key) {
                if (value.isdeleted == undefined) {
                    $scope.feed.push(value);
                }
            })
            angular.forEach($scope.feed, function (value, key) {
                $scope.imgFeed.push($scope.cutImg(value.content));
                $scope.strFeed.push($scope.recursive(value.content));
            })
            $("#feed1").html($scope.strFeed[0].st);
            $("#feed2").html($scope.strFeed[1].st);
            $("#feed3").html($scope.strFeed[2].st);
            //angular.forEach($scope.json, function (value, key) {
            //    $scope.imgArr.push($scope.cutImg(value.content));
            //    value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
            //})
            
        }).
        error(function (data, status, headers, config) {

        });
    //relate question
    var url = "../../Services/QuestionService.svc/related/" + Qid;
    $http.get(url).
        success(function (datarelated, status, headers, config) {
            $scope.related = [];
            angular.forEach(datarelated, function (value, key) {
                if (value.isdeleted == undefined) {
                    $scope.related.push(value);
                }
            })
            //angular.forEach($scope.json, function (value, key) {
            //    $scope.imgArr.push($scope.cutImg(value.content));
            //    value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
            //})

        }).
        error(function (data, status, headers, config) {

        });
    $scope.openListCmt = function (index, id) {
        $scope.commentClick[index] = !$scope.commentClick[index];
        var url = "../../Services/CommentService.svc/comment/" + id;
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.commentList[index] = data;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
    $scope.voteCmtArr = [];
    $scope.commentClick = [];
    $scope.cmtCount = [];
    var i = 0;
    $scope.upvoteArr = [];
    angular.forEach($scope.data, function (value, key) {
        $scope.upvoteArr.push(true);
        $scope.commentClick.push(false);
        var arr = [];
        var count = 0;
        angular.forEach(value.comment, function (values, keys) {
            arr.push(true);
            count++;
        })
        $scope.voteCmtArr[i] = arr;
        $scope.cmtCount.push(count);
        i++;
    });
    $scope.cmt = [];
    $scope.submitCmt = function (index, aId) {
        var data = { "commentcontent": $scope.cmt[index], "parentcmtid": aId };
        var dataJSON = JSON.stringify(data);
        var config = {
            headers: {
                'Token': $scope.name = $cookies.get('token')
            }
        };
        var url = "../../Services/CommentService.svc/comment";
        $http.post(url, dataJSON, config).
            success(function (data, status, headers, config) {
                var url2 = "../../Services/CommentService.svc/comment/" + aId;
                $http.get(url2).
                    success(function (data2, status, headers, config) {
                        $scope.commentList[index] = data2;
                        $scope.cmt[index] = "";
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        // var id = $scope.data[index].comment.length;

        // $scope.data[index].comment.push(cmt);
        // $scope.voteCmtArr[index].push(true);
        // $scope.cmtCount[index]++;

    }
    //$scope.upvoteCmt = function (x, y) {
    //    $scope.voteCmtArr[x][y] = false;
    //    $scope.data[x].comment[y].voteQuantity++;
    //}
    //$scope.downvoteCmt = function (x, y) {
    //    $scope.voteCmtArr[x][y] = true;
    //    $scope.data[x].comment[y].voteQuantity--;
    //}
    $scope.functiona = function (id) {
        angular.forEach($scope.data, function (value, key) {
            if (value.id == id) {
                $("#content" + id).html(value.commentcontent);
            }
        })
    }
    $scope.voteQ = function (id,bool) {
        var data = { "target": 1, "targetid": id, "value": bool };
        var dataJSON = JSON.stringify(data);
        var url = "../../Services/VoteService.svc/vote";
        var url2 = "../../Services/QuestionService.svc/question/" + Qid;
        $http.post(url, dataJSON,configHeader).
           success(function (data, status, headers, config) {
               $http.get(url2).
                   success(function (data, status, headers, config) {
                       // $scope.questionInfo = data;
                       $scope.questionInfo.votecountup = data.votecountup;
                       $scope.questionInfo.votecountdown = data.votecountdown;
                       $scope.totalQuestionVoted = data.votecountup - data.votecountdown;
                   }).
                   error(function (data, status, headers, config) {
                       // called asynchronously if an error occurs
                       // or server returns response with an error status.
                   });
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
    }

    $scope.voteA = function (id, bool) {
        var data = { "target": 2, "targetid": id, "value": bool };
        var dataJSON = JSON.stringify(data);
        var url = "../../Services/VoteService.svc/vote";
        var url2 = "../../Services/AnswerService.svc/answer/" + Qid;
        $http.post(url, dataJSON, configHeader).
           success(function (data, status, headers, config) {
               $http.get(url2).
                   success(function (data, status, headers, config) {
                       // $scope.questionInfo = data;
                       var count = 0;
                       var x;
                       angular.forEach(data, function (value,key) {
                           if (id == value.id) {
                               x = value;
                           }
                       })
                       angular.forEach($scope.data, function (value, key) {
                           if (id == value.id) {
                               $scope.data[count] = x;
                           }
                           count++;
                       })
                   }).
                   error(function (data, status, headers, config) {
                       // called asynchronously if an error occurs
                       // or server returns response with an error status.
                   });
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
    }
    
    $scope.submitAnswer = function () {
        // var content = "5";
        var Qid = $stateParams.qid;
        var Uid = $cookies.get('id');
        var content = $('#edit').froalaEditor('html.get');
        var data = { "commentcontent": content, "questionid": Qid, "userid": Uid };
        var dataJSON = JSON.stringify(data);
        var url = "../../Services/AnswerService.svc/addanswer";
        $http.post(url, dataJSON,configHeader).
            success(function (data, status, headers, config) {
                $scope.answerClick = !$scope.answerClick;
                $scope.txtAnswer = "";
                var url2 = "../../Services/AnswerService.svc/answer/" + Qid;
                $scope.isAnswered = true;
                $http.get(url2).
                    success(function (data2, status, headers, config) {
                        $scope.answerCount = data2.length;
                        $scope.data = data2;
                        $scope.commentList.push([]);
                        $scope.editMode.push(false);
                        $('#edit').froalaEditor('html.set', '');
                            

                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });


    }
    $scope.editQuestionF = function () {
        // var content = "5";
        var Qid = $stateParams.qid;
        var content = $('#rteditQ').froalaEditor('html.get');
        var data = { "content": content };
        var dataJSON = JSON.stringify(data);
        var url = "../../Services/QuestionService.svc/updatequestion/" + Qid;
        $http.put(url, dataJSON, configHeader).
            success(function (data, status, headers, config) {
                $scope.editQuestion = !$scope.editQuestion;
                var url2 = "../../Services/QuestionService.svc/question/" + Qid;
                $http.get(url2).
                    success(function (data2, status, headers, config) {
                        $scope.questionInfo = data2;
                        $scope.totalQuestionVoted = data2.votecountup - data2.votecountdown;
                        $("#questionContent").html($scope.questionInfo.content);
                        $('#rteditQ').froalaEditor('html.set', '');

                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });


    }
    $scope.editQ = function () {
        $("#rteditQ").froalaEditor({
            theme: 'gray',
            heightMin: 100,
            toolbarButtons: ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'align', 'outdent', 'indent', 'html'],
            toolbarButtonsXS: ['undo', 'redo', '-', 'bold', 'italic', 'underline']
        });
        $scope.editQuestion = true;
        $('#rteditQ').froalaEditor('html.set', $scope.questionInfo.content);
    }
    $scope.editAnswer = function (index, id) {
        $("#edit"+id).froalaEditor({
            theme: 'gray',
            heightMin: 100,
            toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic', 'underline', 'align', 'outdent', 'indent','html'],
            toolbarButtonsXS: ['undo', 'redo' , '-', 'bold', 'italic', 'underline']
        });
        $scope.editMode[index] = true;
        angular.forEach($scope.data, function (value, key) {
           if (value.id == id) {
               $('#edit'+id).froalaEditor('html.set', value.commentcontent);
           }
        })
    }
    $scope.updateAnswer = function (index, id) {
        $scope.editMode[index] = false;
        var Qid = $stateParams.qid;
        var content = $('#edit'+id).froalaEditor('html.get');
        var data = { "commentcontent": content, "id": id};
        var dataJSON = JSON.stringify(data);
        var url = "../../Services/AnswerService.svc/updateanswer";
        $http.put(url, dataJSON,configHeader).
            success(function (data, status, headers, config) {
                var url2 = "../../Services/AnswerService.svc/answer/" + Qid;
                $http.get(url2).
                    success(function (data2, status, headers, config) {
                        $scope.data = data2;
                        $scope.commentList.push([]);
                        $scope.editMode.push(false);
                        $('#edit').froalaEditor('html.set', '');
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
    $scope.$watch('txtSearchTopic', function () {
        
        if ($scope.txtSearchTopic != "" && typeof($scope.txtSearchTopic) !='undefined') {
            $scope.isCreating = false;
            $scope.check = false;
            var searchTopicUrl = "../../Services/SearchService.svc/searchtopic/" + $scope.txtSearchTopic;
            $http.post(searchTopicUrl).
            success(function (data, status, headers, config) {
                data.splice(5, data.length);
                $scope.rs = [];
                angular.forEach(data, function (key, value) {
                    $scope.rs.push({ name: key.topicname, topicid: key.topicid, image: key.topicimage,followcount:key.followcount});
                })
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.rs = [];
            });
        } else {
            $scope.rs = [];
        }
    })
    $scope.addQuestionTopic = function (id,name) {
        $scope.check = false;
        var Qid = $stateParams.qid;
        angular.forEach($scope.listTopic, function (value, key) {
            if (name == value.topicname) {
                $scope.check = true;
                $scope.isCreating = false;
            }
        })
        if (!$scope.check) {
            var url = "../../Services/TopicService.svc/addquestiontopic?topicid="+id+"&questionid="+Qid;
            var data = {};
            var dataJSON = JSON.stringify(data);
            $http.post(url, dataJSON, configHeader).
                success(function (data, status, headers, config) {
                    $scope.txtSearchTopic = "";
                    $scope.listTopic.push({"topicid":id,"topicname":name});
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        }
    }
    
    $scope.questionUnfollowTopic = function (id,index) {
        var check = false;
        var Qid = $stateParams.qid;
        var url = "../../Services/TopicService.svc/questionunfollow?topicid=" + id + "&questionid=" + Qid;
        var data = {};
        var dataJSON = JSON.stringify(data);
        $http.put(url, dataJSON, configHeader).
            success(function (data, status, headers, config) {
                $scope.listTopic.splice(index, 1);
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        
    }
    $scope.deleteQShow = function (index, id) {
        confirm('Bạn có chắc chắn muốn xóa câu hỏi này?');
        $("#okBtn").click(function () {
            $scope.deleteQ(index,id);
        });
    }
    $scope.deleteQ = function () {
        var Qid = $stateParams.qid;
        var url = "../../Services/QuestionService.svc/question/" + Qid;
        $http.delete(url, configHeader).
           success(function (data, status, headers, config) {
               alert("Xóa thành công");
               $state.go("index");
           }).
       error(function (data, status, headers, config) {
           // called asynchronously if an error occurs
           // or server returns response with an error status.
       });

    }
    $scope.followQ = function () {
        if(!$scope.isOwn){
            var Qid = $stateParams.qid;
            var url = "../../Services/QuestionService.svc/addquestionsubscription/" + Qid;
            var data = "";
            var dataJson = JSON.stringify(data);
            $http.post(url, dataJson ,configHeader).
                success(function (data, status, headers, config) {
                    $scope.isFollowed = true;
                    $scope.questionInfo.followcount++;
                }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            })
        }
    }
    $scope.unfollowQ = function () {
        if (!$scope.isOwn) {
            var Qid = $stateParams.qid;
            var url = "../../Services/QuestionService.svc/removequestionsubscription/" + Qid;
            var data = "";
            var dataJson = JSON.stringify(data);
            $http.delete(url, configHeader).
                success(function (data, status, headers, config) {
                    $scope.isFollowed = false;
                    $scope.questionInfo.followcount--;
                }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            })
        }
    }
    $scope.markasHelpful = function (id) {
        var url = "../../Services/AnswerService.svc/markhelpful/" + id;
        var data = "";
        var dataJson = JSON.stringify(data);
        $http.put(url,dataJson, configHeader).
            success(function (data, status, headers, config) {
                var url2 = "../../Services/AnswerService.svc/answer/" + Qid;
                $http.get(url2).
                    success(function (data2, status, headers, config) {
                        var count = 0;
                        angular.forEach($scope.data, function (value, key) {
                            if (id == value.id) {
                                $scope.data[count].helpful = true;
                            } else {
                                $scope.data[count].helpful = false;
                            }
                            count++;
                        })
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        })
    }
    $scope.editComment = function (id,parentindex,index,answerid) {
        var url = "../../Services/CommentService.svc/updatecomment/" + id;
        var inputid = 'commentinput-' + index + '-' + id;
        var inputp = 'commentp-' + index + '-' + id
        var content = document.getElementById(inputid).value;
        var data = {"commentcontent": content};
        var dataJson = JSON.stringify(data);
        $http.put(url, dataJson, configHeader).
            success(function (data, status, headers, config) {
                //document.getElementById(inputid).style.display = "none";
                //document.getElementById(inputp).style.display = "block";
                var url1 = "../../Services/CommentService.svc/comment/" + answerid;
                $http.get(url1).
                    success(function (data, status, headers, config) {
                        $scope.commentList[parentindex] = data;
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        })
    }
    $scope.reportQ = function () {
        var url = "../../Services/ReportService.svc/report";
        var data = {"questionid": Qid, "reportcontent": $scope.reason};
        var dataJson = JSON.stringify(data);
        $http.post(url, dataJson, configHeader).
            success(function (data, status, headers, config) {
            }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        })
    }

    $scope.addTopic = function () {
        $scope.ngan = false;
        $scope.dai = false;
        if ($scope.txtSearchTopic != undefined && $scope.txtSearchTopic != "" && $scope.txtSearchTopic.length >= 3 && $scope.txtSearchTopic.length < 50) {
            var url = "../../Services/TopicService.svc/addtopic";
            var data = { "topicname": $scope.txtSearchTopic };
            var dataJson = JSON.stringify(data);
            $scope.isCreating = true;
            $scope.ngan = false;
            $scope.dai = false;
            $http.post(url, dataJson, configHeader).
                success(function (data, status, headers, config) {

                    $scope.success = true;
                    $scope.txtSearchTopic = "";
                    //$scope.listTopic.push({ "topicid": data.topicid, "topicname": data.topicname });
                    $scope.addQuestionTopic(data.topicid,data.topicname);
                }).
            error(function (data, status, headers, config) {
                $scope.success = false;
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            })
        }else if($scope.txtSearchTopic.length < 3){
            $scope.ngan = true;
        }else if($scope.txtSearchTopic.length > 50){
            $scope.dai = true;
        }
    }
    $scope.listUserFollow = [];
    $scope.followClick = function () {
        var url = "../../Services/QuestionService.svc/listuserfollowed/" + Qid;
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.listUserFollow = data;
            }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        })
    }
    $scope.goProfile = function (id) {
        $state.go("profile",{uid:id});
    }
    $scope.deleteAnswerShow = function (index, id) {
        confirm('Bạn có thực sự muốn xóa câu trả lời này?');
        $("#okBtn").click(function () {
            $scope.deleteAnswer(index,id);
        });
    }
    $scope.deleteAnswer = function (index, id) {
        var url = "../../Services/AnswerService.svc/answer/" + id;
        $http.delete(url,configHeader).
            success(function (data, status, headers, config) {
                $timeout(function () {
                    location.reload();
                }, 500);
            }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        })
    }
    $scope.deleteCmtShow = function (index, answerId, id) {
        confirm('Bạn có thực sự muốn xóa bình luận này?');
        $("#okBtn").click(function () {
            $scope.deleteCmt(index, answerId,id);
        });
    }
    $scope.deleteCmt = function (id,answerId,index) {
        var url = "../../Services/CommentService.svc/comment/" + id;
        $http.delete(url, configHeader).
            success(function (data, status, headers, config) {
                var url2 = "../../Services/CommentService.svc/comment/" + answerId;
                $http.get(url2).
                    success(function (data2, status, headers, config) {
                        $scope.commentList[index] = data2;
                        $scope.cmt[index] = "";
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        })
    }
    //end questiondetail
}])

.factory('AvatarPhoto', function(){
    var data = {
        Avatar: '',
        Name: '',
    };

    return {
        getAvatar: function () {
            return data.Avatar;
        },
        setAvatar: function (avatar) {
            data.Avatar = avatar;
        },
        getName: function () {
            return data.Name;
        },
        setName: function (name) {
            data.Name = name;
        }
    };
})
.controller("profileController", ['$scope', '$rootScope', '$cookies', '$state', '$http', '$stateParams','hozy.config','AvatarPhoto',function ($scope, $rootScope, $cookies, $state, $http, $stateParams,hozyConfig,AvatarPhoto) {
    var configHeader = {
        headers: {
            'Token':$cookies.get('token')
        }
    };
    $scope.showDetail = false;
    //xu ly authorized
    $scope.uid = $cookies.get('id');
    var uid = $stateParams.uid;
    if($scope.uid == uid){
        $scope.isUser = true;
    }else{
        $scope.isUser = false;
    }
    $scope.tab = 1;

    $scope.setTab = function (tabId) {
        $scope.tab = tabId;
    };

    $scope.isSet = function (tabId) {
        return $scope.tab === tabId;
    };
    
    $scope.answerTab = true;
    $scope.questionTab = false;
    $scope.topicTab = false;
    $scope.openTab = function(tabType){
        if(tabType==1){
            $scope.answerTab = true;
            $scope.questionTab = false;
            $scope.topicTab = false;
        }else if(tabType==2){
            $scope.answerTab = false;
            $scope.questionTab = true;
            $scope.topicTab = false;
        }else if(tabType==3){
            $scope.answerTab = false;
            $scope.questionTab = false;
            $scope.topicTab = true;
        }
    }

    
    var url =  "../../Services/UserService.svc/user/" + uid;
    $http.get(url).
        success(function (data, status, headers, config) {
            $scope.address = data.address;
            $scope.dob = data.dateofbirth;
            $scope.description = data.description;
            $scope.txtDescription = data.description;
            $scope.education = data.education;
            $scope.employment = data.employment;

            $scope.txtaddress = data.address;
            $scope.txtdob = data.dateofbirth;
            $scope.txteducation = data.education;
            $scope.txtemployment = data.employment;
            $scope.txtName = data.displayname;
            $scope.website = data.website;
            $scope.displayname = data.displayname;
            if (typeof(data.avatar) == 'undefined' || data.avatar == null || data.avatar == "") {
                $scope.avatar = "http://i.imgur.com/hUQGGwE.png";
            } else {
                $scope.avatar = data.avatar;
            }            
            $scope.cover = data.cover;
            $(".profile-header").css("background-image", "url(" + data.cover+")");
            if(typeof($scope.description) == 'undefined' || $scope.description == ""){
                $scope.isDes = false;
            }
            $scope.showDetail = true;
            
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    $scope.answerInfo = false;
    $scope.answerList = [];
    var answerUserUrl =  "../../Services/UserService.svc/getanswerofuser/" + uid;
    $http.get(answerUserUrl,configHeader).
        success(function (answer, status, headers, config) {
            if (answer.length == 0) {
                $scope.answerInfo = true;
            }else{
                $scope.answerContainNull = answer;
                var count = 0;
                angular.forEach($scope.answerContainNull, function (value, key) {
                    if (value != null) {
                        $scope.answerList.push(value);
                    } 
                })
                $scope.answerCount = answer.length;
            }
            
        }).
        error(function (data1, status, headers, config) {
            //$state.go('errorPage');
        });    
    $scope.questionInfo = false;
    var questionUserUrl =  "../../Services/UserService.svc/getquestionofuser/" + uid;
    $http.get(questionUserUrl,configHeader).
        success(function (data1, status, headers, config) {
            if (data1.length == 0) {
                $scope.questionInfo = true;
            }else{
                $scope.json = data1;
            }
            
        }).
        error(function (data1, status, headers, config) {

        });
    $scope.questionFollowInfo = false;
    $scope.questionFollowList = [];
    var questionFollowUrl = "../../Services/UserService.svc/followingquestion/" + uid;
    $http.get(questionFollowUrl,configHeader).
        success(function (datafollow, status, headers, config) {
            if (datafollow.length == 0) {
                $scope.questionFollowInfo = true;
            }else{
                $scope.questionFollowList = datafollow;
            }
            
        }).
        error(function (datafollow, status, headers, config) {

        });
    $scope.listFollowTopic = [];
    $scope.topicInfo = false;
    var urlGetTopic = "../../Services/TopicService.svc/getusertopic/" + uid;
    $http.get(urlGetTopic).
        success(function (data, status, headers, config) {
            if (data.length == 0) {
                $scope.topicInfo = true;
            } else {
                $scope.listFollowTopic = data;
                $scope.countTopic = data.length;
            }
            
            
        }).
        error(function (data, status, headers, config) {

        });
    $scope.functiona = function (id) {
        angular.forEach($scope.json, function (value, key) {
            if (value.id == id) {
                $("#content" + id).html(value.content);
            }
        })
    }
    $scope.getQuestionFollow = function (id) {
        angular.forEach($scope.questionFollowList, function (value, key) {
            if (value.id == id) {
                $("#questionfollowcontent" + id).html(value.content);   
            }
        })
    }
    $scope.getAnswerUser = function (id) {
        angular.forEach($scope.answerList, function (value, key) {
            if (value.id == id) {
                $("#answercontent" + id).html(value.commentcontent);
            }
        })
    }

    $scope.isEdit = false;   
    $scope.isDes = true;   
    $scope.isName = true;   
    $scope.editAbout = function(){
        $scope.isEdit = true;
        $('.right-col-body .listTopic').css("min-height","160px");
        $scope.txtaddress = $scope.address;
        $scope.txtdob = $scope.dob;
        $scope.txteducation = $scope.education;
        $scope.txtemployment = $scope.employment;
    }
    $scope.update =function(img,type){
        var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-="
        var check = function(string){
            for(i = 0; i < specialChars.length;i++){
                if(string.indexOf(specialChars[i]) > -1){
                    return true
                }
            }
            return false;
        }
        if($scope.txtName.trim().length > 20){
            alert('Tên hiển thị không dài quá 20 kí tự')
            $scope.txtName = $scope.displayname;
        }else if($scope.txtName.trim().length == 0 ){
            alert('Tên hiển thị không được để trống');
            $scope.txtName = $scope.displayname;
        }else if($scope.txtName.trim().length < 3 ){
            alert('Tên hiển thị phải dài hơn 3 kí tự');
            $scope.txtName = $scope.displayname;
        }else if(check($scope.txtName) == true) {
            alert('Tên hiển thị không được chứa kí tự đặc biệt');
            $scope.txtName = $scope.displayname;
        }else{
            if (type == 1) {
                var data = { "displayname": $scope.txtName, "description": $scope.txtDescription, "address": $scope.txtaddress, "dateofbirth": $scope.txtdob, "education": $scope.txteducation, "employment": $scope.txtemployment, "avatar": img,"cover":$scope.cover };
            } else
            {
                var data = { "displayname": $scope.txtName, "description": $scope.txtDescription, "address": $scope.txtaddress, "dateofbirth": $scope.txtdob, "education": $scope.txteducation, "employment": $scope.txtemployment, "avatar": $scope.avatar, "cover": img };
            }
           // var data = { "displayname": $scope.txtName, "description": $scope.txtDescription, "address": $scope.txtaddress, "dateofbirth": $scope.txtdob, "education": $scope.txteducation, "employment": $scope.txtemployment , "avatar":img};
            var dataJSON = JSON.stringify(data);
            var url = "../../Services/UserService.svc/user/"+uid;
            $http.put(url, dataJSON,configHeader).
                success(function (data, status, headers, config) {
                    $scope.isEdit = false;
                    $('.right-col-body .listTopic').css("min-height","0");
                    var url2 =  "../../Services/UserService.svc/user/" + uid;
                    $http.get(url2).
                        success(function (data2, status, headers, config) {
                            $scope.address = data2.address;
                            $scope.dob = data2.dateofbirth;
                            $scope.description = data2.description;
                            $scope.education = data2.education;
                            $scope.employment = data2.employment;
                            $scope.website = data2.website;
                            $scope.avatar = data2.avatar;
                            $scope.cover = data2.cover;
                            $scope.displayname = data2.displayname;
                            $(".profile-header").css("background-image", "url(" + data2.cover + ")");
                            //update des show hide
                            if (data2.description == '' || typeof($scope.description) == 'undefined') {
                                
                                $scope.isDes = false;
                            } else {
                                $scope.isDes = true;
                            }
                            $scope.isName = true;

                        }).
                        error(function (data, status, headers, config) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                        });
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        }
    }
    //watch avatar len header
    $scope.$watch('avatar', function (newValue, oldValue) {
        if (uid == $cookies.get('id')) {
            if (newValue !== oldValue) AvatarPhoto.setAvatar($scope.avatar);
        }
    });
    $scope.$watch('txtName', function (newValue, oldValue) {
        if (uid == $cookies.get('id')) {
            if (newValue !== oldValue) AvatarPhoto.setName($scope.txtName);
        }
    });
    $scope.cancelEdit = function(){
      $scope.isEdit = false;
      $('.right-col-body .listTopic').css("min-height","0");
    }
    $scope.putDes = function(){
        $scope.isDes = false;
        $scope.txtDescription = $scope.description;
    }
    $scope.putName = function(){
        $scope.isName = false;
        $scope.txtName = $scope.displayname;
    }
   
    $scope.imageBase64 = "";
    $scope.loadImageFileAsURL = function(type){
        

        //
        if (type == 1) {
            var filesSelected = document.getElementById("file-input").files;
        } else {
            var filesSelected = document.getElementById("file-input1").files;
        }
        var url = 'https://api.imgur.com/3/upload.json';
       
        //
        var configHeaderImg = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Client-ID b322ae9d9804726'
            }
        };

        var validFormats = ['jpg', 'jpeg', 'png'];
        var ext = filesSelected[0].name.substr(filesSelected[0].name.lastIndexOf('.') + 1);
        

    if (filesSelected.length > 0 && validFormats.indexOf(ext.toLowerCase()) != -1){
            var fileToLoad = filesSelected[0];

            var fileReader = new FileReader();

            fileReader.onload = function (fileLoadedEvent) {
                
                $http.post(url, fileToLoad, configHeaderImg).
                success(function (data1, status, headers, config) {
                    var link = data1.data.link.slice(0, 4) + 's' + data1.data.link.slice(4);
                    $scope.update(link,type);

                }).
                error(function (data1, status, headers, config) {
                   // called asynchronously if an error occurs
                   // or server returns response with an error status.
                });
                    //var srcData = fileLoadedEvent.target.result; // <--- data: base64

                    //var divTest = document.getElementById("photo");
                    //var newImage = document.createElement('img');
                    //newImage.src = srcData;

                    //alert(srcData);

            }

            fileReader.readAsDataURL(fileToLoad);
        }
    }

    $scope.onFileSelect = function ($file) {
        //$upload.upload({
        //    url: '../../Services/UploadService.svc/Upload',
        //    file: $file,
        //    progress: function (e) { }
        //}).then(function (data, status, headers, config) {
        //    // file is uploaded successfully
        //    console.log(data);
        //});
    }


    
}])

.controller("headerController", ['$window', '$timeout', '$scope', '$state', '$http', '$cookies', 'hozy.config','$sce','AvatarPhoto', function ($window, $timeout, $scope, $state, $http, $cookies, hozyConfig,$sce,AvatarPhoto) {
    var adminRight = $cookies.get('rights');
    if(adminRight == 'admin'){
        $scope.isAdmin = true;
    }
    else{
        $scope.isAdmin = false;
    }
    var configHeader = {
        headers: {
            'Token': $cookies.get('token')
        }
    };
        var token = $cookies.get('token');
    $scope.messages = [];
    $scope.count = 0;
    if($cookies.get('token')!=undefined){
        var url = "../../Services/NotificationService.svc/notification";
        $http.get(url, configHeader).
           success(function (data, status, headers, config) {
               var rvdata = data.reverse();
               angular.forEach(rvdata, function (value, key) {
                   $scope.messages.push({ mes: value.notificationdescription, qid: value.questionid, id: value.notificationid, img: value.avatar, ischecked: value.ischecked, elapsedTime: $scope.calElapsed(parseInt(value.creationdate)),time:value.creationdate });
                   if (!value.ischecked) {
                       $scope.count++;
                   }
               })
              
           }).
       error(function (data, status, headers, config) {
           // called asynchronously if an error occurs
           // or server returns response with an error status.
   });
    }
    //xu li noti
    // //signalR notification
     // collection of messages coming from server
    $scope.notificationHub = null; // holds the reference to hub

    $scope.notificationHub = $.connection.notificationHub; // initializes hub
    $.connection.hub.start(); // starts hub

    $scope.notificationHub.client.requestcookie = function (message) {
        //console.log("123123");
        $scope.notificationHub.server.doprocesscookie(token);
    }
    $scope.notificationHub.client.notify = function (received, time, qid, id, img) {
        var elapsedTime = $scope.calElapsed(time);
        $scope.messages.push({ mes: received, elapsedTime: elapsedTime, qid: qid, id: id, img: img,time:time,ischecked:false });
        $scope.count++;
        var audio = new Audio('../sound/ting.mp3');
        audio.play();
        //sessionStorage["received"] = received;
        //sessionStorage.setItem("received", JSON.stringify(received[i]));
        //console.log(received);
        $scope.$apply();
    }
    $scope.notificationHub.client.sendtime = function (time) {

        $scope.$apply();
    }
    var token = $cookies.get('token');
    $scope.clickNoti = function (id, qid) {
        var index = 0;
        $scope.notificationHub.server.getchecked(token, id);
        $state.go('questionDetail', { qid: qid });
        angular.forEach($scope.messages, function (value, key) {
            if (value.id == id) {
                if (!$scope.messages[index].ischecked) {
                    $scope.messages[index].ischecked = true;
                    $scope.count--;
                }
            }
            index++;
        })
        
    }

    
    $scope.logout = function () {

        var url = "../../Services/SessionService.svc/session";
        $http.delete(url, configHeader).
           success(function (data, status, headers, config) {
               
           }).
       error(function (data, status, headers, config) {
           // called asynchronously if an error occurs
           // or server returns response with an error status.
       });
        $cookies.remove('token');
        $cookies.remove('rights');
        $cookies.remove('id');
        $cookies.remove('username');
        $timeout(function () {
            window.location.href = "mainTemplate.html";
        }, 1000);

    }
    $scope.$watch(function () { return AvatarPhoto.getAvatar(); }, function (newValue, oldValue) {
        if (newValue !== oldValue) $scope.avatar = newValue;
    });
    $scope.$watch(function () { return AvatarPhoto.getName(); }, function (newValue, oldValue) {
        if (newValue !== oldValue) $scope.name = newValue;
    });
    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }
    // $scope.$on("username", function(event, username){
    //     $scope.name = username;
    // });
    // $scope.logged = false;
    if (typeof ($cookies.get('id')) != 'undefined' && $cookies.get('id') != null) {
        $scope.logged = true;
    } else {
        $scope.logged = false;
        $timeout(function () {
            $('#login').modal({
                show: true
            });
        }, 1000);
        
    }
    $scope.showDetail = false;
    $scope.uid = $cookies.get('id');
    var url =  "../../Services/UserService.svc/user/" + $scope.uid;
    $http.get(url).
        success(function (data, status, headers, config) {
            $scope.name = data.displayname;
            if (data.avatar == 'undefined' || data.avatar == null || data.avatar == "") {
                $scope.avatar = "http://i.imgur.com/hUQGGwE.png";
            } else {
                $scope.avatar = data.avatar;
            }
            AvatarPhoto.setAvatar($scope.avatar);
            AvatarPhoto.setName($scope.name);
            $scope.showDetail = true;
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    // $scope.name = $cookies.get('username');
    
    $scope.linkToProfile = function(){
      $state.go('profile', {uid:$scope.uid});
      var a = 0;
    }

    //login fb
    $scope.loginFacebook = function () {
        var url = "../../Services/userService.svc/facebook";
        $http.get(url).
            success(function (data, status, headers, config) {
                var fbUrl = data.facebookurl;
                $window.location.href = fbUrl;
            }).
            error(function (data, status, headers, config) {

            });
    }
      
    $scope.calElapsed = function (timeinput) {
        var time = timeinput - 7 * 60 * 60;
           var dateNow = Math.round((new Date()).getTime() / 1000);
           var diff = dateNow - time;
           var hours_diff = Math.floor(diff / 3600);
           var mins_diff = Math.floor((diff % 3600) / 60)
           var secs_diff = diff % 60;
           var returnMes;
           if (hours_diff == 0 && mins_diff == 0) {
               returnMes = secs_diff + " giây trước";
           } else if (hours_diff == 0 && mins_diff != 0) {
               returnMes = mins_diff + " phút trước";
           } else {
               returnMes = "khoảng " +  hours_diff + " giờ trước";
           }
           return returnMes;
       }
       $scope.reCalElapsedTime = function () {
           var index = 0;
           angular.forEach($scope.messages, function (value, key) {
               $scope.messages[index].elapsedTime = $scope.calElapsed($scope.messages[index].time);
               index++;
           })
       }
        $scope.$watch('txtSearchQuestion', function () {
            $timeout(function () {
               if ($scope.txtSearchQuestion != "" && typeof($scope.txtSearchQuestion) != 'undefined') {
                   var searchQUrl = "../../Services/SearchService.svc/searchquestion/" + $scope.txtSearchQuestion;
                   $http.post(searchQUrl).
                   success(function (data, status, headers, config) {
                       $scope.rs = [];
                       var count = 0;
                       angular.forEach(data, function (key, value) {
                           if (count < 3) {
                               $scope.rs.push({ title: key.title, id: key.id, avatar: key.avatar , followcount:key.followcount});
                           }
                           count++;
                       })
                   }).
                   error(function (data, status, headers, config) {
                       // called asynchronously if an error occurs
                       // or server returns response with an error status.
                       $scope.rs = [];
                   });
               } else {
                   $scope.rs = [];
               }
               
            }, 500);
        })

        $scope.goSearch = function () {
            $state.go('search',{keyword:$scope.txtSearchQuestion});
        }
       
        
      
}])

.controller("homepageController", ['$scope', '$rootScope', '$cookies', '$state', '$http', 'hozy.config','$sce','$q','$window','$timeout', function ($scope, $rootScope, $cookies, $state, $http, hozyConfig,$sce,$q,$window,$timeout) {
    //slide bar load first
    var url = "../../Services/NotificationService.svc/slide";
    $http.get(url).
        success(function (data, status, headers, config) {
            $scope.slide = data;
        }).
        error(function (data, status, headers, config) {

        });
    $scope.openLink = function (index) {
        $window.location.href = $scope.slide[index].sliderlink;
    }
    var timeout;
    angular.element($window)
		.bind(
			"scroll",
 	 			function () {
 	 			    clearTimeout(timeout);
 	 			    timeout = setTimeout(function () {
 	 			        var windowHeight = "innerHeight" in window ? window.innerHeight
							: document.documentElement.offsetHeight;
 	 			        var body = document.body, html = document.documentElement;
 	 			        var docHeight = Math.max(body.scrollHeight,
                                body.offsetHeight, html.clientHeight,
                                html.scrollHeight, html.offsetHeight);
 	 			        windowBottom = windowHeight + window.pageYOffset;
 	 			        if (windowBottom >= docHeight) {
 	 			            $scope.loadMore();
 	 			        }
 	 			    }, 500);
 	 			    
 	 			});
    $scope.imgArr = [];
    $scope.imgAns = [];
    $scope.ready = false;
    $scope.skip = 10;
    var uid = $cookies.get('id');
    $scope.loadMore = function () {
        var stType = '';
        if ($scope.type == 1) {
            stType = "../../Services/UserService.svc/feed/" + uid+"/";
        } else if ($scope.type == 2) {
            stType = "../../Services/QuestionService.svc/newquestions/";
        } else if ($scope.type == 3) {
            stType = "../../Services/QuestionService.svc/topquestions/";
        } else {
            stType = "../../Services/UserService.svc/newquestion/"+uid+"/";
        }
        if ($scope.type != undefined) {
            
            var url = stType + $scope.skip;
            $http.get(url).
                success(function (data, status, headers, config) {
                    //var data1 = data.splice(5,data.length);
                    angular.forEach(data, function (value, key) {
                        if (value.isdeleted == undefined) {
                            $scope.json.push(value);
                            $scope.imgArr.push($scope.cutImg(value.content));
                        }
                       
                        value.bestanswer == null || value.isdeleted == undefined ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
                        $scope.skip++;
                    })

                }).
                error(function (data, status, headers, config) {

                });
            console.log('load more');
            
        }
    }
    $scope.getQuestionMore = function(index){
        $scope.questionMore = $scope.json[index];
        $("#contentMore").html($scope.questionMore.content);
    }
    $scope.hottopic = [];
    var urlGetHotTopic = "../../Services/TopicService.svc/hottopic";
    $http.get(urlGetHotTopic).
        success(function (hottopic, status, headers, config) {
            $scope.hottopic = hottopic;
            $scope.hottopic.splice(5, hottopic.length);
        }).
        error(function (hottopic, status, headers, config) {

        });
    $scope.topUser = [];
    var urlGetTopUser = "../../Services/UserService.svc/topuser";
    $http.get(urlGetTopUser).
        success(function (topuser, status, headers, config) {
            $scope.topUser = topuser;
            $scope.topUser.splice(5, topuser.length);
        }).
        error(function (data, status, headers, config) {

        });
    $scope.apiQuerys = [];
    
    $scope.calElapsed = function (time) {
           var dateNow = Math.round((new Date()).getTime() / 1000);
           var diff = dateNow - time;
           var hours_diff = Math.floor(diff / 3600);
           var mins_diff = Math.floor((diff % 3600) / 60)
           var secs_diff = diff % 60;
           var returnMes;
           if (hours_diff == 0 && mins_diff == 0) {
               returnMes = secs_diff + " giây trước";
           } else if (hours_diff == 0 && mins_diff != 0) {
               returnMes = mins_diff + " phút trước";
           } else {
               returnMes = "khoảng " +  hours_diff + " giờ trước";
           }
           return returnMes;
       }
    
    $scope.listFollowTopic = [];
    var configHeader = {
        headers: {
            'Token':$cookies.get('token')
        }
    };
    $scope.functiona = function (id) {
        angular.forEach($scope.json, function (value, key) {
            if (value != 0) {
                if (value.id == id) {
                    $("#content" + id).html(value.content);
                }
            }
        })
    }
    $scope.functionb = function (id,index) {
        if ($scope.json[index].bestanswer != null) {
            var content = $scope.json[index].bestanswer.commentcontent;
            $("#answer" + id).html(content);
            var heightAnswer = $("#answer" + id).height();
            if (content.length > 500 || heightAnswer > 139) {
                $("#show-more-link" + id).show();
                $("#show-more-link" + id).css("display", "block");
                $("#answer" + id).css("height", "140px");
                $("#answer" + id).css("overflow", "hidden");
            } else {
                $("#show-more-link" + id).hide();
                $("#show-more-link" + id).css("display", "none");
            }
                //var heightAnswer = $("#answer" + id).height();
                    
         }
    }
    //$scope.functionb = function (id) {
    //    angular.forEach($scope.topAnswerArr, function (value, key) {
    //        if (value != 0) {
    //            if (value.question.id == id) {
    //                $("#answer" + id).html(value.commentcontent);
    //                var heightAnswer = $("#answer" + id).height();
    //                if(heightAnswer > 235){
    //                    $("#top-answer-more" + id).show();
    //                    $("#top-answer" + id).addClass('answer-bar-top');
    //                    $("#top-answer-more" + id).addClass('show-more');
    //                    $("#show-more-link" + id).click(function () {
    //                        $("#top-answer" + id).removeClass('answer-bar-top');
    //                        $("#top-answer-more" + id).hide();
    //                    });
    //                }
    //            }
    //        }
    //    })
    //}
    $scope.functionc = function (id) {
        angular.forEach($scope.json, function (value, key) {
            if (value != 0) {
                if (value.id == id) {
                    if (value.content.indexOf("</p>") != -1 && value.content !="") {
                        //if (value.content.indexOf("</p>") < value.content.indexOf("img")) {
                        //    var v = value.content.substring(0, value.content.indexOf("</p>") + 4);
                        //    $("#content" + id).html(v);
                        //} else {
                        //    var str = value.content.substring( value.content.indexOf("</p>") +4,value.content.length);
                        //    var v = str.substring(0, str.indexOf("</p>") + 4);
                        //    $("#content" + id).html(v);
                        //}
                        $("#content" + id).html($scope.recursive(value.content).st);
                        $scope.recursive(value.content).havesm ? $("#showmore" + id).css("display", "block") : $("#showmore" + id).css("display", "none");
                    } else {
                        $("#content" + id).html(value.content);
                        $("#showmore" + id).css("display", "none");
                    }
                }
            }
        })
    }
    $scope.functiond = function (id) {
        angular.forEach($scope.json, function (value, key) {
            if (value.bestanswer != null) {
                if (value.id == id) {
                    if (value.bestanswer.commentcontent.indexOf("</p>") != -1 && value.bestanswer.commentcontent != "") {
                        //if (value.content.indexOf("</p>") < value.content.indexOf("img")) {
                        //    var v = value.content.substring(0, value.content.indexOf("</p>") + 4);
                        //    $("#content" + id).html(v);
                        //} else {
                        //    var str = value.content.substring( value.content.indexOf("</p>") +4,value.content.length);
                        //    var v = str.substring(0, str.indexOf("</p>") + 4);
                        //    $("#content" + id).html(v);
                        //}
                        $("#answer" + id).html($scope.recursive(value.bestanswer.commentcontent).st);
                        $scope.recursive(value.bestanswer.commentcontent).havesm ? $("#showmore-answer" + id).css("display", "block") : $("#showmore-answer" + id).css("display", "none");
                    } else {
                        $("#answer" + id).html(value.bestanswer.commentcontent);
                        $("#showmore-answer" + id).css("display", "none");
                    }
                }
            }
        })
    }
    $scope.recursive = function (str) {
        if (str.indexOf("</p>") > str.indexOf("img") && str.indexOf("img") != -1) {
            var returnstr = str.substring(str.indexOf("</p>") + 4, str.length);
            return $scope.recursive(returnstr);
        } else{
            var returnstr = str.substring(0, str.indexOf("</p>") + 4);
            var havesm;
            str.indexOf("</p>") + 4 < str.length ? havesm = true : havesm = false;
            return {st:returnstr,havesm:havesm};
        } 
    }
    $scope.$emit('event', getUrlParameter('id'));
   // alert(getUrlParameter('id'));
    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    $scope.LoadTopQuestion = function () {
        $scope.skip = 10;
        $scope.type = 3;
        var url = "../../Services/QuestionService.svc/topquestions/0";
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.json = [];
                angular.forEach(data, function (value, key) {
                    if (value.isdeleted == undefined) {
                        $scope.json.push(value);
                    }
                })
                $scope.imgArr = [];
                $scope.imgAns = [];
                angular.forEach($scope.json, function (value, key) {
                    $scope.imgArr.push($scope.cutImg(value.content));
                    value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
                })
                $("#icontop").css("color", "#2989bc");
                $("#iconnew").css("color", "#333");
                $("#iconfeed").css("color", "#333");
                $("#iconnewfeed").css("color", "#333");
            }).
            error(function (data, status, headers, config) {

            });
    }
    //load feed
    var uid = parseInt($cookies.get('id'));
    
    if (typeof ($cookies.get('id')) != 'undefined' && $cookies.get('id') != null) {
        $scope.logged = true;
        $scope.type = 1;
        //var url = "../../Services/UserService.svc/feed/" + uid + "/0";
        //$http.get(url).
        //    success(function (data, status, headers, config) {
        //        $scope.json = data;
        //        for (var i = 0; i < $scope.json.length; i++) {
        //            $scope.show[i] = true;

        //        }
        //    }).
        //    error(function (data, status, headers, config) {

        //    });

        var uid = parseInt($cookies.get('id'));
        var urlUser = "../../Services/UserService.svc/user/" + uid;
        $http.get(urlUser).
            success(function (userDetail, status, headers, config) {
                $scope.avatar = userDetail.avatar;
            }).
            error(function (userDetail, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        $scope.chooseTopic = [];
        var urlGetTopic = "../../Services/TopicService.svc/getusertopic/" + uid;
        $http.get(urlGetTopic).
            success(function (data, status, headers, config) {
                if (data.length == 0) {
                    var urlRandomTopic = "../../Services/TopicService.svc/random/12";
                    $scope.randomTopoic = [];
                    $http.get(urlRandomTopic).
                        success(function (data2, status2, headers2, config2) {
                            $scope.randomTopoic = data2;
                            angular.forEach(data2, function(value,key){
                                $scope.chooseTopic.push(false);
                            })
                            $('#followTopic').modal({
                                backdrop: 'static',
                                keyboard: false,
                                show: true
                            });
                        }).
                        error(function (data, status, headers, config) {

                        });

                    
                } else {
                    // neu da chon topic thi load feed luon
                    $scope.listFollowTopic = data;
                    var url = "../../Services/UserService.svc/feed/" + uid + "/0";
                    $http.get(url).
                        success(function (data, status, headers, config) {
                            $scope.json = [];
                            angular.forEach(data, function (value, key) {
                                if (value.isdeleted == undefined) {
                                    $scope.json.push(value);
                                }
                            })
                            $scope.imgArr = [];
                            $scope.imgAns = [];
                            angular.forEach($scope.json, function (value, key) {
                                $scope.imgArr.push($scope.cutImg(value.content));
                                value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
                            })
                            $("#icontop").css("color", "#333");
                            $("#iconnew").css("color", "#333");
                            $("#iconfeed").css("color", "#2989bc");
                            $("#iconnewfeed").css("color", "#333");
                        }).
                        error(function (data, status, headers, config) {

                        });
                }
                
                
            }).
            error(function (data, status, headers, config) {

            });
        
    } else {
        $scope.feedType = 3;
        var urlGetTopic = "../../Services/TopicService.svc/getusertopic/" + uid;
        $http.get(urlGetTopic).
            success(function (data, status, headers, config) {
                if (data.length == 0) {
                    var urlRandomTopic = "../../Services/TopicService.svc/random/5";
                    $scope.randomTopicGuest = [];
                    $http.get(urlRandomTopic).
                        success(function (data2, status2, headers2, config2) {
                            $scope.randomTopicGuest = data2;
                        }).
                        error(function (data, status, headers, config) {

                        });

                    
                } else {
                    $scope.listFollowTopic = data;
                }
                
                
            }).
            error(function (data, status, headers, config) {

            });
        $scope.logged = false;
        $scope.LoadTopQuestion();
    }
    
    //change feed
    
    $scope.LoadNewQuestion = function () {
        $scope.skip = 10;
        var url = "../../Services/QuestionService.svc/newquestions/0";
        $scope.type = 2;
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.json = [];
                angular.forEach(data, function (value, key) {
                    if (value.isdeleted == undefined) {
                        $scope.json.push(value);
                    }
                })
                $scope.imgArr = [];
                $scope.imgAns = [];
                angular.forEach($scope.json, function (value, key) {
                    $scope.imgArr.push($scope.cutImg(value.content));
                    value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
                })
                $("#icontop").css("color", "#333");
                $("#iconnew").css("color", "#2989bc");
                $("#iconfeed").css("color", "#333");
                $("#iconnewfeed").css("color", "#333");
            }).
            error(function (data, status, headers, config) {

            });
    }
    $scope.cutImg = function (obj) {
        if (obj != undefined) {
            if (obj.indexOf("img") == -1) {
                return 0;
            } else {
                var v = obj.substring(obj.indexOf("src") + 5, obj.length)
                var imgLink = v.substring(0, v.indexOf(" ") - 1);
                return imgLink;
            }
        }
    }
    $scope.LoadFeed = function () {
        $scope.skip = 10;
        var url = "../../Services/UserService.svc/feed/" + uid + "/0";
        $scope.type = 1;
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.json = [];
                angular.forEach(data, function (value, key) {
                    if (value.isdeleted == undefined) {
                        $scope.json.push(value);
                    }
                })
                $scope.imgArr = [];
                $scope.imgAns = [];
                angular.forEach($scope.json, function (value, key) {
                    $scope.imgArr.push($scope.cutImg(value.content));
                    value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
                })
                $("#icontop").css("color", "#333");
                $("#iconnew").css("color", "#333");
                $("#iconfeed").css("color", "#2989bc");
                $("#iconnewfeed").css("color", "#333");
            }).
            error(function (data, status, headers, config) {

            });
    }

    $scope.LoadnewFeed = function () {
        $scope.skip = 10;
        var url = "../../Services/UserService.svc/newquestion/" + uid + "/0";
        $scope.type = 4;
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.json = [];
                angular.forEach(data, function (value, key) {
                    if (value.isdeleted == undefined) {
                        $scope.json.push(value);
                    }
                })
                $scope.imgArr = [];
                $scope.imgAns = [];
                angular.forEach($scope.json, function (value, key) {
                    $scope.imgArr.push($scope.cutImg(value.content));
                    value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
                })
                $("#icontop").css("color", "#333");
                $("#iconnew").css("color", "#333");
                $("#iconfeed").css("color", "#333");
                $("#iconnewfeed").css("color", "#2989bc");
            }).
            error(function (data, status, headers, config) {

            });
    }


    
     
    
   
    $scope.upvoteArr = [];
    angular.forEach($scope.json, function (value, key) {
        $scope.upvoteArr.push(true);
    });
    $scope.upvote = function (index) {
        $scope.upvoteArr[index] = false;
        $scope.json[index].totalVote++;
    }
    $scope.downvote = function (index) {
        $scope.upvoteArr[index] = true;
        $scope.json[index].totalVote--;
    }
    $scope.show = [];
    
    $scope.pass = function (index) {
        $scope.show[index] = !$scope.show[index];
        var qesId = 'qes' + index;
        $scope.show[index] ? document.getElementById(qesId).style.color = '#333' : document.getElementById(qesId).style.color = '#999';

    }
    $scope.askQuestion = function () {
        var questionCheck = $scope.title;
        if(questionCheck == "" || questionCheck == undefined){
            alert("Tiêu đề của câu hỏi không được để trống.");
        }else if(questionCheck.length > 100){
            alert("Tiêu đề của câu hỏi không được quá 100 ký tự.");
            $scope.title = ""; 
        }else{
            var url = "../../Services/QuestionService.svc/question";
            var data = { "title": $scope.title, "content": "" };
            var dataJSON = JSON.stringify(data);
            $http.post(url, dataJSON, configHeader).
                success(function (data, status, headers, config) {
                    $state.go('questionDetail', {qid:data.id});
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        }
       
        
    }
    
    //call asyn for typeahead in search topic
    //$scope.getTopic = function (val) {
    //    var searchTopicUrl = "../../Services/SearchService.svc/search/" + val;
    //    var returnValue = [];
    //    return $http.post(searchTopicUrl).then(function (response) {
    //        angular.forEach(response.data,function (key,value){
    //            returnValue.push({ name: key.topicname });
    //            rs.push({ name: key.topicname });
    //        })
    //        return returnValue;
    //    });
    //};
    $scope.$watch('txtSearchTopic', function () {
        if ($scope.txtSearchTopic != "" && typeof($scope.txtSearchTopic) !='undefined') {
            var searchTopicUrl = "../../Services/SearchService.svc/searchtopic/" + $scope.txtSearchTopic;
            $http.post(searchTopicUrl).
            success(function (data, status, headers, config) {
                $scope.rs = [];
                data.splice(5, data.length);
                angular.forEach(data, function (key, value) {
                    $scope.rs.push({ name: key.topicname, topicid: key.topicid, image: key.topicimage,followcount:key.followcount });
                })
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.rs = [];
            });
        } else {
            $scope.rs = [];
        }
    })
    //
    //$scope.$watch('listFollowTopic', function () {
    //    if ($scope.listFollowTopic.length != 0) {
    //        $('#followTopic').modal({
    //        });
    //    }
    //})
    //follow topic
    $scope.followTopic = function (id, name,flc) {
        var check = false;
        angular.forEach($scope.listFollowTopic, function (value, key) {
            if (name == value.topicname) {
                check = true;
                }
        })
        if (!check) {

            var url = "../../Services/TopicService.svc/topic?topicid=" + id;
            var data = {};
            var dataJSON = JSON.stringify(data);
            $http.post(url, dataJSON, configHeader).
                success(function (data, status, headers, config) {

                    $scope.listFollowTopic.push({ topicname: name, topicid: id ,followcount: flc});
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        } else {
            alert("Bạn đã theo dõi chủ đề này!");
        }
    }
    $scope.arrTopic=[];
    $scope.followTopicGuest = function(id,name,index,followcount){
        $scope.chooseTopic[index] = !$scope.chooseTopic[index];
        if ($scope.chooseTopic[index]) {
            $scope.arrTopic.push({"topicid": id, "topicname":name,"followcount":followcount});
        }
        else{
            var count = 0;
            angular.forEach($scope.arrTopic,function(value,key){
                if(value.topicid == id){
                    $scope.arrTopic.splice(count,1);
                }
                count++;
            })
        }
        
    }
    $scope.followAllTopic = function () {
        if($scope.arrTopic.length >= 3){
            angular.forEach($scope.arrTopic,function(value,key){
            
                var check = false;
                angular.forEach($scope.listFollowTopic, function (value1, key) {
                    if (name == value1.topicname) {
                        check = true;
                    }
                })
                if (!check) {
                    
                    var url = "../../Services/TopicService.svc/topic?topicid=" + value.topicid;
                    var data = {};
                    var dataJSON = JSON.stringify(data);
                    $http.post(url, dataJSON, configHeader).
                        success(function (data, status, headers, config) {
                            $scope.listFollowTopic.push({ topicname: value.topicname, topicid: value.topicid,followcount:value.followcount });
                            $('#followTopic').modal("hide");
                        }).
                        error(function (data, status, headers, config) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                        });
                }
            })
            //load feed sau khi đã chọn topic    
            var url = "../../Services/UserService.svc/feed/" + uid + "/0";
            $http.get(url).
                success(function (data, status, headers, config) {
                    $scope.json = [];
                    angular.forEach(data, function (value, key) {
                        if (value.isdeleted == undefined) {
                            $scope.json.push(value);
                        }
                    })
                    $scope.imgArr = [];
                    $scope.imgAns = [];
                    angular.forEach($scope.json, function (value, key) {
                        $scope.imgArr.push($scope.cutImg(value.content));
                        value.bestanswer == null ? $scope.imgAns.push(0) : $scope.imgAns.push($scope.cutImg(value.bestanswer.commentcontent));
                    })
                    $("#icontop").css("color", "#333");
                    $("#iconnew").css("color", "#333");
                    $("#iconfeed").css("color", "#2989bc");
                    $("#iconnewfeed").css("color", "#333");
                }).
                error(function (data, status, headers, config) {

                });
        }
        else{
            alert("Bạn phải chọn ít nhất 3 chủ đề");
        }
        
        
    }

    //unfollow topic
    $scope.unfollowTopic = function (id,index) {
        if ($scope.listFollowTopic.length != 3) {
            var url = "../../Services/TopicService.svc/unfollow?topicid=" + id;
            var data = {};
            var dataJSON = JSON.stringify(data);
            $http.put(url, dataJSON, configHeader).
                success(function (data, status, headers, config) {
                    $scope.listFollowTopic.splice(index, 1);


                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        } else {
            alert("Bạn phải theo dõi ít nhất 3 chủ đề!");
        }
    }
    //link to topic Detail
    $scope.linkTopicDetail = function(topicId){
        $state.go('topicDetail', {tid:topicId});
    }
    $scope.topAnswerArr = [];
    //$scope.$watch('json', function (user,callback) {
    //    $scope.topAnswerArr = [];
    //    var deferred = $q.defer();
    //    var promise = deferred.promise;
    //    var prom = [];
    //    var mot = $q.defer();
    //    var hai = $q.defer();
    //    var ba = $q.defer();
    //    var bon = $q.defer();
    //    var nam = $q.defer();
    //    var sau = $q.defer();
    //    var bay = $q.defer();
    //    var tam = $q.defer();
    //    var chin = $q.defer();
    //    var muoi = $q.defer();
    //    var  count =0;
    //    var arrdefer = [mot, hai, ba, bon, nam, sau, bay, tam, chin, muoi];
    //    var promisearr = [];
    //    angular.forEach($scope.json, function (value1, key) {
    //        promisearr.push(arrdefer[count].promise);
    //        count++;
    //    })
        
        

    //    deferred.resolve();
    //    var count2 = 0;
    //    angular.forEach($scope.json, function (value1, key) {
    //        arrdefer[count2].resolve($scope.getMaxVote(value1.id));
    //        count2++;
    //    })
    //    $q.all(promisearr).then(function (data) {
    //        angular.forEach(data,function(value,key){
    //            $scope.topAnswerArr.push($scope.calMaxVote(value.data));
    //        })
            
    //    });
    //})

    $scope.getMaxVote = function (id) {
        return $http.get("../../Services/AnswerService.svc/answer/" + id).success(
            function (value) {
                 return value;
            }
        );
    }

    $scope.calMaxVote = function (arr) {
        if (arr.length != 0) {
            var rv = arr[0];
            angular.forEach(arr, function (value, key) {
                if ((value.votecountup - value.votecountdown) > (rv.votecountup - rv.votecountdown)) {
                    rv = value;
                }
            })
            return rv;
        } else {
            return 0;
        }

    }
    //$scope.truyenidtraobj = function (arr) {
    //    var testarr = [];
    //    angular.forEach($scope.topAnswerArr, function (value,key) {
    //        if (value != 0) {
    //            if (value.question.id == id) {
    //                testarr.push(value);
    //                return testarr;
    //            }
    //        }
    //    })
    //}
}])
.controller("topicController", ['$scope', '$rootScope', '$cookies', '$state', '$http', '$stateParams', 'hozy.config', 'AvatarPhoto', function ($scope, $rootScope, $cookies, $state, $http, $stateParams, hozyConfig, AvatarPhoto) {
    var topicid = $stateParams.tid;
    var configHeader = {
        headers: {
            'Token':$cookies.get('token')
        }
    };
    $scope.showDetail = false;
    $scope.userAva = AvatarPhoto.getAvatar();
    var adminRight = $cookies.get('rights');
    if (typeof ($cookies.get('id')) != 'undefined' && $cookies.get('id') != null) {
        $scope.logged = true;
    } else {
        $scope.logged = false;
    }
    if(adminRight == 'admin'){
        $scope.isAdmin = true;
    }
    else{
        $scope.isAdmin = false;
    }
    $scope.isDelete = [];
    var url = "../../Services/TopicService.svc/newquestion/"+ topicid +"/0";
    $http.get(url).
        success(function (data, status, headers, config) {
            $scope.topicQuestion = [];
            angular.forEach(data, function (value, key) {
                if (value.isdeleted == undefined) {
                    $scope.topicQuestion.push(value);
                }
            })
            $scope.countQuestion = data.length;
            $("#icontop").css("color", "#333");
            $("#iconnew").css("color", "#2989bc");
        }).
        error(function (data, status, headers, config) {
            $state.go('errorPage');
        });
    $scope.LoadTopQuestion = function () {
        var url = "../../Services/TopicService.svc/topquestion/" + topicid + "/0";
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.topicQuestion = [];
                angular.forEach(data, function (value, key) {
                    if (value.isdeleted == undefined) {
                        $scope.topicQuestion.push(value);
                    }
                })
                $scope.countQuestion = data.length;
                $("#icontop").css("color", "#2989bc");
                $("#iconnew").css("color", "#333");
            }).
            error(function (data, status, headers, config) {
                $state.go('errorPage');
            });
    }
    $scope.LoadNewQuestion = function () {
        var url = "../../Services/TopicService.svc/newquestion/" + topicid + "/0";
        $http.get(url).
            success(function (data, status, headers, config) {
                $scope.topicQuestion = [];
                angular.forEach(data, function (value, key) {
                    if (value.isdeleted == undefined) {
                        $scope.topicQuestion.push(value);
                    }
                })
                $scope.countQuestion = data.length;
                $("#icontop").css("color", "#333");
                $("#iconnew").css("color", "#2989bc");
            }).
            error(function (data, status, headers, config) {
                $state.go('errorPage');
            });
    }
    var url2 = "../../Services/TopicService.svc/gettopic/"+ topicid;
    $http.get(url2).
        success(function (data1, status, headers, config) {
            $scope.topicDes = data1.topicdescription;
            $scope.topicName = data1.topicname;
            $scope.topicImage = data1.topicimage;
            $scope.topicId = data1.topicid;
            $scope.followcount = data1.followcount;
            $scope.showDetail = true;
            $scope.txtName = $scope.topicName;
        }).
        error(function (data1, status, headers, config) {
            
        });
    $scope.functiona = function (id) {
        angular.forEach($scope.topicQuestion, function (value, key) {
            if (value.id == id) {
                $("#content" + id).html(value.content);
            }
        })
    }
    $scope.isfollow = false;
    var uid = parseInt($cookies.get('id'));
    var urlGetTopic = "../../Services/TopicService.svc/getusertopic/" + uid;
    $http.get(urlGetTopic).
        success(function (data, status, headers, config) {
            angular.forEach(data, function(value,key){
                if (topicid == value.topicid) {
                    $scope.isfollow = true;
                }
            })
            
            
            
        }).
        error(function (data, status, headers, config) {

        });
    $scope.followTopic = function(id){
        var url = "../../Services/TopicService.svc/topic?topicid=" + id;
        var data = {};
        var dataJSON = JSON.stringify(data);
        $http.post(url, dataJSON, configHeader).
            success(function (data, status, headers, config) {
                $scope.isfollow = true;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
    $scope.unfollowTopic = function (id) {
        var url = "../../Services/TopicService.svc/unfollow?topicid=" + id;
        var data = {};
        var dataJSON = JSON.stringify(data);
        $http.put(url, dataJSON, configHeader).
            success(function (data, status, headers, config) {
                $scope.isfollow = false;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
    $scope.isDes = false;
    $scope.changeDescription = function(){
        $scope.isDes = !$scope.isDes;
        $scope.txtDescription = $scope.topicDes;
    }
    $scope.isName = true;  
    $scope.putName = function(){
        $scope.isName = false;
        $scope.txtName = $scope.topicName;
    }
    
    $scope.update =function(img){
        if($scope.txtName.length < 3 || $scope.txtName.length > 50){
            alert("Tên chủ đề phải trong khoảng từ 3 - 50 kí tự");
            $scope.txtName = $scope.topicName;
        }else{
            if($scope.txtDescription == '' || $scope.txtDescription == undefined){
                var data = {"topicimage": img,"topicdescription": $scope.topicDes, "topicname": $scope.txtName};
            }else if($scope.txtName == '' || $scope.txtName == undefined){
                var data = {"topicimage": img,"topicdescription": $scope.txtDescription, "topicname": $scope.topicName};
            }
            else{
                var data = {"topicimage": img,"topicdescription": $scope.txtDescription, "topicname": $scope.txtName };
            }
            $scope.isDes = false;
            
            var dataJSON = JSON.stringify(data);
            var url = "../../Services/TopicService.svc/updatetopic/"+topicid;
            $http.put(url, dataJSON,configHeader).
                success(function (data, status, headers, config) {
                    var url2 = "../../Services/TopicService.svc/gettopic/"+ topicid;
                    $http.get(url2).
                        success(function (data1, status, headers, config) {
                            $scope.topicDes = data1.topicdescription;
                            $scope.topicName = data1.topicname;
                            $scope.topicImage = data1.topicimage;
                            $scope.isName = true;
                        }).
                        error(function (data1, status, headers, config) {

                        });
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        }
        
    }
    $scope.loadImageFileAsURL = function(){
        var filesSelected = document.getElementById("file-input").files;
        var url = 'https://api.imgur.com/3/upload.json';
       
        //
        var configHeaderImg = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Client-ID b322ae9d9804726'
            }
        };
        var validFormats = ['jpg', 'jpeg', 'png'];
        var ext = filesSelected[0].name.substr(filesSelected[0].name.lastIndexOf('.') + 1);


        if (filesSelected.length > 0 && validFormats.indexOf(ext.toLowerCase()) != -1) {
            var fileToLoad = filesSelected[0];

            var fileReader = new FileReader();

            fileReader.onload = function (fileLoadedEvent) {
                $http.post(url, fileToLoad, configHeaderImg).
                success(function (data1, status, headers, config) {
                    var link = data1.data.link.slice(0, 4) + 's' + data1.data.link.slice(4);
                   $scope.update(link);
                }).
                error(function (data1, status, headers, config) {
                   // called asynchronously if an error occurs
                   // or server returns response with an error status.
                });
                    //var srcData = fileLoadedEvent.target.result; // <--- data: base64

                    //var divTest = document.getElementById("photo");
                    //var newImage = document.createElement('img');
                    //newImage.src = srcData;

                    //alert(srcData);

            }

            fileReader.readAsDataURL(fileToLoad);
        }
    }

    $scope.askQuestion = function () {
        var questionCheck = $scope.title;
        if(questionCheck == "" || questionCheck == undefined){
            alert("Tiêu đề của câu hỏi không được để trống.");
        }else if(questionCheck.length > 100){
            alert("Tiêu đề của câu hỏi không được quá 100 ký tự."); 
            $scope.title = "";
        }else{
            var url = "../../Services/QuestionService.svc/question";
            var data = { "title": $scope.title, "content": "" };
            var dataJSON = JSON.stringify(data);
            $http.post(url, dataJSON, configHeader).
                success(function (data, status, headers, config) {
                    var url2 = "../../Services/TopicService.svc/addquestiontopic?topicid=" + $stateParams.tid + "&questionid=" + data.id;
                    var data2 = {};
                    var dataJSON2 = JSON.stringify(data);
                    $http.post(url2, dataJSON2, configHeader).
                        success(function (data2, status2, headers2, config2) {
                            $state.go("questionDetail", { qid: data.id });
                        }).
                        error(function (data, status, headers, config) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                        });
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
        }
        
    }
    //end topic controller
    
}])
.controller("analyticsController", ['$scope', '$rootScope', '$cookies', '$state', '$http', '$stateParams', 'hozy.config', '$sce', '$filter', 'dateFilter', function ($scope, $rootScope, $cookies, $state, $http, $stateParams, hozyConfig, $sce, $filter, dateFilter) {
    $(function(){
        $('#datepicker1').datepicker({
            format: 'dd/mm/yyyy',
            startDate: '-0d'
        });
    });
    if($cookies.get('rights') != "admin"){
        $state.go('errorPage');
    }
    var configHeader = {
        headers: {
            'Token': $cookies.get('token')
        }
    };
    $scope.functiona = function (id) {
        angular.forEach($scope.reportList, function (value, key) {
            if (value.id == id) {
                $("#content" + id).html(value.content);
            }
        })
    }
    //tab 1 Q&A
    $scope.buildChart = function (skip) {
        var urlQuestionlog = "../../Services/AnalyticService.svc/questionlog/12/" + skip*12;
        $http.get(urlQuestionlog).
               success(function (data, status, headers, config) {
                   $scope.topQuestion = [];
                   angular.forEach(data[0].questions, function (value, key) {
                       if (value.question.isdeleted == undefined) {
                           $scope.topQuestion.push(value);
                       }
                   })
                   questionLog = data;
                   $scope.category = [];
                   $scope.question = [];
                   $scope.answer = [];
                   angular.forEach(questionLog, function (value, key) {
                       $scope.category.push({ "label": value.createtime });
                       $scope.question.push({ "value": value.questioncount });
                       $scope.answer.push({ "value": value.commentanswercount });

                   })
                   $scope.category.reverse();
                   $scope.question.reverse();
                   $scope.answer.reverse();
                   $scope.myDataSource = {
                       "chart": {
                           "caption": "Câu hỏi và câu trả lời",
                           "numberprefix": "",
                           "plotgradientcolor": "",
                           "bgcolor": "FFFFFF",
                           "showalternatehgridcolor": "0",
                           "divlinecolor": "CCCCCC",
                           "showvalues": "0",
                           "showcanvasborder": "0",
                           "canvasborderalpha": "0",
                           "canvasbordercolor": "CCCCCC",
                           "canvasborderthickness": "1",
                           "captionpadding": "30",
                           "linethickness": "3",
                           "yaxisvaluespadding": "15",
                           "legendshadow": "0",
                           "legendborderalpha": "0",
                           "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                           "showborder": "0"
                       },
                       "categories": [
                           {
                               "category": $scope.category
                           }
                       ],
                       "dataset": [
                           {
                               "seriesname": "Câu hỏi",
                               "data": $scope.question
                           },
                           {
                               "seriesname": "Trả lời",
                               "data": $scope.answer
                           }
                       ]
                   };
                   $("#chart-container").updateFusionCharts({ dataSource: $scope.myDataSource, dataFormat: 'json' });
               }).
               error(function (data, status, headers, config) {
                   // called asynchronously if an error occurs
                   // or server returns response with an error status.
               });
    }
    $scope.buildChart(0);
    $(function() {
        $("#chart-container").insertFusionCharts({
            type: 'zoomline',
            width: '800',
            height: '500',
           
        })
    });
    $scope.timeCount = 0;
    $scope.drawChart = function (type) {
        if (type == 0) {
            $scope.timeCount++;
            $scope.getTime($scope.today,-1);
        } else {
            $scope.timeCount--;
            $scope.getTime($scope.today, 1);
        }
        $scope.buildChart($scope.timeCount);
    }
    $scope.today = new Date();
    $scope.getTime = function (date, i) {
        $scope.today = new Date(date.getTime() + 86400000*i); 
        var dd = $scope.today.getDate();
        var mm = $scope.today.getMonth() + 1; //January is 0!
        var yyyy = $scope.today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }
        $scope.date = dd + '/' + mm + '/' + yyyy;
    }
    $scope.getTime($scope.today, 0);
    $scope.clickChange = function () {
        $("#chart-container").insertFusionCharts({
            type: 'zoomline',
            width: '800',
            height: '500',

        })
        $("#chart-container").updateFusionCharts({ dataSource: $scope.myDataSource2, dataFormat: 'json' });
    }
    // $("#chart-container").updateFusionCharts({ dataSource: $scope.myDataSource, dataFormat: 'json' });
    
    $scope.tab = 1;

    $scope.setTab = function (tabId) {
        $scope.tab = tabId;
    };

    $scope.isSet = function (tabId) {
        return $scope.tab === tabId;
    };
    $scope.getTrend = function () {
        $scope.apiQuerys = [];
        $scope.topicArr = [];
        var urlTopiclog = "../../Services/AnalyticService.svc/topiclog/1/0";
        $http.get(urlTopiclog).
               success(function (data, status, headers, config) {
                   $scope.topicArr = data[0].toptopics;
                   $scope.topicArr.splice(5, $scope.topicArr.length);
                   angular.forEach($scope.topicArr, function (key, value) {
                       $scope.link = "https://www.google.com/trends/fetchComponent?hl=vn-VN&q=" + key.topicname + "&cmpt=q&content=1&cid=TIMESERIES_GRAPH_0&export=5&w=640&h=330&date=now%207-d";
                       $scope.trusted = $sce.trustAsResourceUrl($scope.link);
                       $scope.apiQuerys.push($scope.trusted);
                   })
               }).
               error(function (data, status, headers, config) {
                   // called asynchronously if an error occurs
                   // or server returns response with an error status.
               });
    }
    $scope.getTrend();
    //tab report
    $scope.reportList = [];
    $scope.reportDetail = [];
    var urlReport = "../../Services/QuestionService.svc/questionreported";
    $http.get(urlReport,configHeader).
           success(function (data, status, headers, config) {
               $scope.reportList = data;
               
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });

    $scope.getListReport = function (id) {
        var urlRpDetail = "../../Services/ReportService.svc/listreport/" + id;
        $http.get(urlRpDetail, configHeader).
               success(function (data, status, headers, config) {
                   $scope.reportDetail = data;

               }).
               error(function (data, status, headers, config) {
                   // called asynchronously if an error occurs
                   // or server returns response with an error status.
               });
    }
    //end tab report
    $scope.goProfile = function (id) {
        $state.go("profile", { uid: id });
    }
    //tab ama
    $scope.startDate = new Date();
    $scope.createAma = function () {
        var urlRpDetail = "../../Services/AmaService.svc/ama";
        var data = { userid: $scope.amaOwner.id, startdate: Math.round($scope.startDate.getTime() / 1000), amaimage: $scope.amaimage };
        var dataJson = JSON.stringify(data);
        $http.post(urlRpDetail,dataJson, configHeader).
               success(function (data, status, headers, config) {
                   alert("Tạo thành công");
                   var urlamalist = "../../Services/AmaService.svc/amalist/0";
                    $http.get(urlamalist, configHeader).
                           success(function (amalist, status, headers, config) {
                                $scope.amaList = amalist;
                                $scope.showAmaList = false;
                           }).
                           error(function (data, status, headers, config) {
                               // called asynchronously if an error occurs
                               // or server returns response with an error status.
                           });
               }).
               error(function (data, status, headers, config) {
                   // called asynchronously if an error occurs
                   // or server returns response with an error status.
               });
    }
    
    $scope.$watch('txtSearchUser', function () {
        if ($scope.txtSearchUser != "" && typeof ($scope.txtSearchUser) != 'undefined') {
            var searchUserUrl = "../../Services/SearchService.svc/searchuser/" + $scope.txtSearchUser;
            $http.post(searchUserUrl).
            success(function (data, status, headers, config) {
                $scope.rs = data;
                $scope.rs.splice(5,data.length);
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.rs = [];
            });
        } else {
            $scope.rs = [];
        }
    })
    $scope.chooseOwner = function (id, ava, name) {
        $scope.choosed = true;
        $scope.rs = [];
        $scope.txtSearchUser = "";
        $scope.amaOwner = {id:id,name:name,ava:ava};
    }


    var urlamalist = "../../Services/AmaService.svc/amalist/0";
    $http.get(urlamalist, configHeader).
           success(function (data, status, headers, config) {
                if(data.length == 0){
                    $scope.showAmaList = true;
                }else{
                    $scope.amaList = data;
                    $scope.showAmaList = false;
                }
               
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
    //end tab ama
    $scope.loadImageFileAsURL = function(){

        //
        var filesSelected = document.getElementById("photo-upload-ama").files;
        var url = 'https://api.imgur.com/3/upload.json';
       
        //
        var configHeaderImg = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Client-ID b322ae9d9804726'
            }
        };
        var validFormats = ['jpg', 'jpeg', 'png'];
        var ext = filesSelected[0].name.substr(filesSelected[0].name.lastIndexOf('.') + 1);


        if (filesSelected.length > 0 && validFormats.indexOf(ext.toLowerCase()) != -1) {
            var fileToLoad = filesSelected[0];

            var fileReader = new FileReader();

            fileReader.onload = function (fileLoadedEvent) {
                
                $http.post(url, fileToLoad, configHeaderImg).
                success(function (data1, status, headers, config) {
                    // $scope.update(data1.data.link,type);
                    $(".photo-ama").css("display","block");
                    $(".photo-ama").css("background-image", "url(" + data1.data.link + ")");
                    $scope.amaimage = data1.data.link.slice(0, 4) + 's' + data1.data.link.slice(4);
                }).
                error(function (data1, status, headers, config) {
                   // called asynchronously if an error occurs
                   // or server returns response with an error status.
                });
                    //var srcData = fileLoadedEvent.target.result; // <--- data: base64

                    //var divTest = document.getElementById("photo");
                    //var newImage = document.createElement('img');
                    //newImage.src = srcData;

                    //alert(srcData);

            }

            fileReader.readAsDataURL(fileToLoad);
        }
    }
}])
.controller('loginFacebookController', ['$scope', '$http', '$state', '$window', 'hozy.config', '$cookies', function ($scope, $http, $state, $window, hozyConfig, $cookies) {
    var currentUrl = window.location.href;
    var oauth = currentUrl.split("https://hozy.info/mainTemplate.html?code=")[1];
    oauth = oauth.split("#/facebook")[0];
    var url = "../../Services/userService.svc/newUserFb?code=" + oauth;
    $http.get(url).
           success(function (data, status, headers, config) {
               $cookies.put('token', data.m_Item2.token);
               $cookies.put('rights', data.m_Item1.rights);
               $cookies.put('id', data.m_Item1.id);
               $cookies.put('username', data.m_Item1.username);
               $window.location.href = "https://hozy.info/mainTemplate.html#/index";
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
    

}])
.controller("changePassController", ['$window', '$timeout', '$scope', '$http', '$state', '$cookies', 'loginService', 'hozy.config', function ($window, $timeout, $scope, $http, $state, $cookies, loginService, hozyConfig) {
    var configHeader = {
        headers: {
            'Token':$cookies.get('token')
        }
    };
    var uid = $scope.uid = parseInt($cookies.get('id'));
    $scope.changePassword = function () {
        var data = { "password": $scope.oldPass,"newpassword":$scope.newPass };
        var dataJSON = JSON.stringify(data);
        var url = "../../Services/UserService.svc/changepw/" + uid;
        $http.put(url, dataJSON, configHeader).
            success(function (data, status, headers, config) {
                $state.go("changepwsuccess");
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                if(status = 406){
                    $scope.rightPassword = true;
                }
                // or server returns response with an error status.
            });
    }
    $scope.$watchCollection('[oldPass, newPass, confirmPass]', function () {
        if($scope.oldPass != undefined && $scope.confirmPass != undefined && $scope.newPass != undefined){
            if($scope.oldPass == $scope.newPass || $scope.confirmPass != $scope.newPass){
                $scope.formInvalid = true;
            }
            else{
                $scope.formInvalid = false;
            }  
        }
        else{
            $scope.formInvalid = true;
        }
    })
    
    


}])
.controller("searchReultController", ['$window', '$timeout', '$scope', '$http', '$state', '$cookies',  'hozy.config','$stateParams', function ($window, $timeout, $scope, $http, $state, $cookies, hozyConfig,$stateParams) {
    var configHeader = {
        headers: {
            'Token':$cookies.get('token')
        }
    };
    var keyword = $stateParams.keyword;
    $scope.keyword = $stateParams.keyword;
    //search question
    var url = "../../Services/SearchService.svc/searchquestion/" + keyword;
    $scope.searchResult =[];
    $http.post(url).
           success(function (data, status, headers, config) {
               $scope.searchResult = [];
               angular.forEach(data, function (value, key) {
                   if (value.isdeleted == undefined) {
                       $scope.searchResult.push(value);
                   }
               })
               $("#keyword").html(keyword);
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
    //searchtopic
    var urltopic = "../../Services/SearchService.svc/searchtopic/" + keyword;
    $scope.searchTopicResult = [];
    $http.post(urltopic).
           success(function (data, status, headers, config) {
               $scope.searchTopicResult = data;
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
    //searchuser
    var urluser = "../../Services/SearchService.svc/searchuser/" + keyword;
    $scope.searchUserResult = [];
    $http.post(urluser).
           success(function (data, status, headers, config) {
               $scope.searchUserResult = data;
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });
    //spelling
    var url = "https://www.googleapis.com/customsearch/v1?key=AIzaSyAczaR_e3CuDAJTHr_uOBHddl8VL2-E9xI&cx=001789124926485329932%3Abedd34cvrlq&q=" + keyword;
    $scope.isCorrect = true;
    $http.get(url).
           success(function (data, status, headers, config) {
               $scope.ggrs = data;
               if ($scope.ggrs.spelling != undefined) {
                   $scope.isCorrect = false;
                   $("#correctSpelling").html($scope.ggrs.spelling.htmlCorrectedQuery);
                   
               }
           }).
           error(function (data, status, headers, config) {
               // called asynchronously if an error occurs
               // or server returns response with an error status.
           });

    $scope.tab = 1;

    $scope.setTab = function (tabId) {
        $scope.tab = tabId;
    };

    $scope.isSet = function (tabId) {
        return $scope.tab === tabId;
    };
}])
.controller("amaController", ['$window', '$timeout', '$scope', '$http', '$state', '$cookies', 'hozy.config', '$stateParams', 'AvatarPhoto','$filter', function ($window, $timeout, $scope, $http, $state, $cookies, hozyConfig, $stateParams, AvatarPhoto,$filter) {
    var configHeader = {
        headers: {
            'Token': $cookies.get('token')
        }
    };
    $scope.amaList = [];
    var urlamalist = "../../Services/AmaService.svc/amalist/0";
    $http.get(urlamalist, configHeader).
       success(function (data, status, headers, config) {
            if(data.length == 0){
                $scope.showAmaList = true;
            }else{
                $scope.amaList = data;
                $scope.showAmaList = false;
            }
           
       }).
       error(function (data, status, headers, config) {
           // called asynchronously if an error occurs
           // or server returns response with an error status.
       });
    $scope.answer = [];
    var Uid = $cookies.get('id');
    $scope.amaid = $stateParams.amaid;
    var getAma = "../../Services/AmaService.svc/ama/" + $scope.amaid;
    $http.get(getAma).
        success(function (data, status, headers, config) {
            Uid == data.owner.id ? $scope.isOwn = true : $scope.isOwn = false;
            $scope.data = data;
            var currentDate = $filter('date')(new Date(), 'MM/dd/yyyy');
            currentDate == data.opendate ? $scope.isOpenDate = true : $scope.isOpenDate = false;
        }).
        error(function (data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    if (typeof ($cookies.get('id')) != 'undefined' && $cookies.get('id') != null) {
        $scope.logged = true;
        $scope.displayName = AvatarPhoto.getName();
        $scope.avatar = AvatarPhoto.getAvatar();
       
        

    } else {
        $scope.logged = false;
    }

    $scope.askQuestion = function () {
        var ask = "../../Services/AmaService.svc/ama/question";
        var data = { amasessionid: $scope.amaid, content: $scope.title };
        var dataJson = JSON.stringify(data);
        $http.post(ask,dataJson,configHeader).
            success(function (data, status, headers, config) {

                var getAma = "../../Services/AmaService.svc/ama/" + $scope.amaid;
                $http.get(getAma).
                    success(function (data, status, headers, config) {
                        $scope.data = data;
                        $scope.title = "";
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });


            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
    $scope.answerQuestion = function (index, id) {
        var asw = "../../Services/AmaService.svc/ama/answer";
        var data = { amaquestionid: id ,answercontent: $scope.answer[index]};
        var dataJson = JSON.stringify(data);
        $http.put(asw, dataJson, configHeader).
            success(function (data, status, headers, config) {

                var getAma = "../../Services/AmaService.svc/ama/" + $scope.amaid;
                $http.get(getAma).
                    success(function (data, status, headers, config) {
                        $scope.data = data;
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });

            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
    $scope.$watch('data', function () {
        if($scope.data!=undefined){
            $scope.auth = [];
            $scope.editMode = [];
            $scope.editA = [];
            angular.forEach($scope.data.amaquestions, function (value, key) {
                value.asker.id == Uid ? $scope.auth.push(true) : $scope.auth.push(false);
                $scope.editMode.push(false);
                $scope.editA.push(false);
            })
        }
    })
    $scope.clickEdit = function (index) {
        $scope.auth[index] = false;
        $scope.editMode[index] = true;
    }
    $scope.clickEditA = function (index) {
        $scope.editA[index] = true;
    }
    $scope.editQ = function (index, id) {
        var editQ = "../../Services/AmaService.svc/ama/question/" + id;
        var data = { content: $scope.data.amaquestions[index].content };
        var dataJson = JSON.stringify(data);
        $http.put(editQ,dataJson,configHeader).
            success(function (data, status, headers, config) {
                $scope.auth[index] = true;
                $scope.editMode[index] = false;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
    $scope.editAnswer = function (index, id) {
        var editQ = "../../Services/AmaService.svc/ama/answer";
        var data = { amaquestionid:id, answercontent: $scope.data.amaquestions[index].answercontent };
        var dataJson = JSON.stringify(data);
        $http.put(editQ, dataJson, configHeader).
            success(function (data, status, headers, config) {
                $scope.editA[index] = false;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
}])
.directive("fileread", [function () {
    return {
        $scope: {
            fileread: "="
        },
        link: function ($scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                $scope.$apply(function () {
                    $scope.fileread = changeEvent.target.files[0].name;
                    // or all selected files:
                    // $scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}])

.filter('currentdate', ['$filter', function ($filter) {
    return function () {
        return $filter('date')(new Date(), 'dd/MM/yyyy hh:mm:ss a');
    };
}])
.filter('filesize', function () {
    return function (value) {
        return (parseFloat(value / 1024000).toFixed(2) + " MB");
    };
})
.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
})
.directive('bsTooltip', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).hover(function () {
                // on mouseenter
                $(element).tooltip('show');
            }, function () {
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
})
.directive('validFile', function () {
return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModel) {
        var validFormats = ['jpg','jpeg','png'];
        elem.bind('change', function () {
            // validImage(false);
            scope.$apply(function () {
                ngModel.$render();
            });
        });
        ngModel.$render = function () {
            ngModel.$setViewValue(elem.val());
        };
        // function validImage(bool) {
        //     // ngModel.$setValidity('extension', bool);
        //     // if(bool==true){
        //     //     alert('hop le');
        //     // }else{
        //     //     alert('k hop le');
        //     // }
        // }
        ngModel.$parsers.push(function(value) {
            var ext = value.substr(value.lastIndexOf('.')+1);

            if(ext=='') return;
            if(validFormats.indexOf(ext.toLowerCase()) == -1){
                // return value;
                alert('Vui lòng chọn tệp tin hình ảnh có đuôi JPG, JPEG, PNG');
            }
            // validImage(true);
            return value;
        });
    }
  };
})
.factory('errorInterceptor', function ($q,$cookies,$timeout) {

    var preventFurtherRequests = false;

    return {
        request: function (config) {

            if (preventFurtherRequests) {
                return;
            }

            return config || $q.when(config);
        },
        requestError: function(request){
            return $q.reject(request);
        },
        response: function (response) {
            return response || $q.when(response);
        },
        responseError: function (response) {
            if (response && response.status === 401) {
                $cookies.remove('token');
                $cookies.remove('rights');
                $cookies.remove('id');
                $cookies.remove('username');
                $timeout(function () {
                    window.location.href = "mainTemplate.html";
                }, 1000);
            }

            return $q.reject(response);
        }
    };
})

.config(function ($httpProvider) {
    $httpProvider.interceptors.push('errorInterceptor');    
})

.directive('dateInput', function (dateFilter) {
            return {
                require: 'ngModel',
                template: '<input type="date"></input>',
                replace: true,
                link: function (scope, elm, attrs, ngModelCtrl) {
                    ngModelCtrl.$formatters.unshift(function (modelValue) {
                        return dateFilter(modelValue, 'yyyy-MM-dd');
                    });

                    ngModelCtrl.$parsers.unshift(function (viewValue) {
                        return new Date(viewValue);
                    });
                },
            };
        });;


angular.module('lgm', ['ui.router', 'ngMessages','ui.bootstrap', 'ngCookies','hozy.config'])
.config([ '$stateProvider', '$urlRouterProvider', function (flowFactoryProvider, $stateProvider, $urlRouterProvider) {
   

}])
// .run(['$rootScope', '$state',function($rootScope, $state){

//   $rootScope.$on('$stateChangeStart',function(){
//       $rootScope.stateIsLoading = true;
//  });


//   $rootScope.$on('$stateChangeSuccess',function(){
//       $rootScope.stateIsLoading = false;
//  });

// }])


.run(['$location', '$rootScope', '$window', function ($location, $rootScope, $window, $localStorage) {
        $window.fbAsyncInit = function () {
            FB.init({
                appId: '878913002208485',
                xfbml: true,
                status: false,
                cookie: true,
                version: 'v2.8'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
}])

// //login controller
// .controller('login', ['$scope', '$http', '$state', '$window', '$timeout', '$rootScope', '$cookies', 'loginService', 'hozy.config', function ($scope, $http, $state, $window, $timeout, $rootScope, $cookies, loginService, hozyConfig) {
//     $scope.errorMsg = "invalid";
//     $scope.showInvalid = false;
//     $scope.login = function () {
        
//         var url =  "../../Services/SessionService.svc/session";
//         var data = { "password": $scope.password, "username": $scope.username };
//         var dataJSON = JSON.stringify(data);
//         $http.post(url, dataJSON).
//        success(function (data, status, headers, config) {
//            $cookies.put('token', data.token);
//            $cookies.put('rights', data.rights);
//            $cookies.put('id', data.userid);
//            $cookies.put('username', data.username);
//            $timeout(function () {
//                $window.location.href = 'mainTemplate.html';
//            }, 2000);
//        }).
//        error(function (data, status, headers, config) {
//            // called asynchronously if an error occurs
//            // or server returns response with an error status.
//        });
        
//     }
// }])

angular.module('hozy.config', []).value('hozy.config', {
    basePath: 'http://localhost:60252/' // Set your base path here
});