﻿//all ajax service here
//service for login
app.service('loginService', function ($http,$cookies) {
    var url = "http://localhost:60252/Services/SessionService.svc/session";
    this.post = function ($http,$cookies) {
        $http({
            method: 'POST',
            url: url
        }).success(function (data) {
            $cookies.put('token', data.token);
            $cookies.put('rights', data.rights);
            $cookies.put('id', data.userid);
            $cookies.put('username', data.username);
        }).error(function () {

        })
    }
});