$(document).ready(function () {
    // $(window).load(function(){ 
    //   // Log the position with jQuery
    //   var x = $(".topic-tag show-div").offset();
    //     var leftA = x.left + "px";
    //     $("#tooltip-topic").css('left',leftA);
    // });
    
    $(".ask-field").focus(function () {
        $(".hover").addClass('ask-hover');
        $(".hide-ask").addClass('hidden-div');
        $("#tooltip-question").css('display','block');
    });
    // $(".ask-field").blur(function () {

    //     $("#tooltip-question").css('display','none');
    // });
    $(".text-search").focus(function () {
        $(".hover").addClass('ask-hover');
        $(".show-div").css('z-index','0');
        $(".hide-ask").addClass('hidden-div');
    });
    $(".text-search").blur(function () {
        $(".hover").removeClass('ask-hover');
        $(".hide-ask").removeClass('hidden-div');
        $(".show-div").css('z-index','1');
        $("#tooltip-question").css('display','none');
    });
    $(".hover").click(function () {
        $(".hover").removeClass('ask-hover');
        $(".hide-ask").removeClass('hidden-div');
        $(".show-div").css('z-index','1');
        $("#tooltip-question").css('display','none');
    });
    $('.left-content-header .dropdown .dropdown-menu').on('click', function(){
        $('.left-content-header .dropdown .dropdown-menu').addClass('keep-open');
    });
    $('body').click(function(event) {
        if (!$(event.target).closest('.left-content-header .dropdown .dropdown-menu').length) {
            $('.left-content-header .dropdown .dropdown-menu').removeClass('keep-open');
        };
    });
    $('.topic-tooltip').click(function(event) {
        $('.topic-tooltip').hide();
        $('#tooltip-topic').hide();
        $('#topic-tag2').hide();
    });
    // $("#login").on('show.bs.modal', function(event){
    //     $("#container").css("opacity","0.3");
    // });
    // $("#register").on('show.bs.modal', function(event){
    //     $("#container").css("opacity","0.3");
    // });
    $( ".dropdown-noti" ).mouseover(function() {
        $( ".dropdown-noti>div" ).css("overflow","auto");
    });
    $( ".dropdown-noti" ).mouseout(function() {
        $( ".dropdown-noti>div" ).css("overflow","hidden");
    });
    //var configFroala = { "bucket": "hozys3", "region": "s3-ap-northeast-1", "keyStart": "upload", "params": { "acl": "public-read", "policy": "eyJleHBpcmF0aW9uIjoiMjAxNi0xMi0wNlQxNDo0ODozMC4wMDBaIiwiY29uZGl0aW9ucyI6W3siYnVja2V0IjoiaG96eXMzIn0seyJhY2wiOiJwdWJsaWMtcmVhZCJ9LHsic3VjY2Vzc19hY3Rpb25fc3RhdHVzIjoiMjAxIn0seyJ4LXJlcXVlc3RlZC13aXRoIjoieGhyIn0seyJ4LWFtei1hbGdvcml0aG0iOiJBV1M0LUhNQUMtU0hBMjU2In0seyJ4LWFtei1jcmVkZW50aWFsIjoiQUtJQUlGWklMSVVNRE5MTUlSN0EvMjAxNjEyMDYvYXAtbm9ydGhlYXN0LTEvczMvYXdzNF9yZXF1ZXN0In0seyJ4LWFtei1kYXRlIjoiMjAxNjEyMDZUMDAwMDAwWiJ9LFsic3RhcnRzLXdpdGgiLCIka2V5IiwidXBsb2FkIl0sWyJzdGFydHMtd2l0aCIsIiRDb250ZW50LVR5cGUiLCIiXV19", "x-amz-algorithm": "AWS4-HMAC-SHA256", "x-amz-credential": "AKIAIFZILIUMDNLMIR7A/20161206/ap-northeast-1/s3/aws4_request", "x-amz-date": "20161206T000000Z", "x-amz-signature": "03510e5ebbd7ebabf12636f57c8463a08158623f8eabf396b442da619c9b4702" } };
    //    $('.edit').froalaEditor({
    //    theme: 'gray',
    //    heightMin: 100,
    //    toolbarButtons: ['undo', 'redo' , '|', 'bold', 'italic', 'underline', 'align', 'outdent', 'indent','html'],
    //    toolbarButtonsXS: ['undo', 'redo', '-', 'bold', 'italic', 'underline'],
    //    imageUploadToS3: configFroala
    //});
    $('.tab-bar .tab-element>li').on('click', function(){
        $(this).addClass('active-tab').siblings().removeClass('active-tab');
    });
    $( ".bigavatar" ).mouseover(function() {
        $( ".profile-header>.photo_info>.photo-upload>label" ).css("display","block");
        $( ".topic-header>.photo_info>.photo-upload>label" ).css("display","block");
    });
    $( ".profile-header>.photo_info>.photo-upload>label" ).mouseout(function() {
        $(this).css("display","none");
    });
    $( ".topic-header>.photo_info>.photo-upload>label" ).mouseout(function() {
        $(this).css("display","none");
    });
    // $( ".profile-header a.topicTitle" ).mouseover(function() {
    //     $( ".editName" ).css("display","block");
    // });
    $( ".modal-login" ).mouseout(function() {
        // alert('abc');
    });
    $('iframe').load(function () {
        $('iframe').contents().find("head")
          .append($("<style type='text/css'>  .embed-footer{display:none;}  </style>"));
    });

    $( window ).resize(function() {
        var width = $(".text-search").width();
        var setwidth = (width + 62) +'px';
        $( ".search-result" ).css("width",setwidth);
    });
    $(document).on('click', function (e) {
        if(e.target.id != 'search-div') {
            $("#search-result-div").hide();
        }else{
            $("#search-result-div").show();
        }
    });
    $('.text-search').keyup(function(e){
        if(e.keyCode == 13)
        {
            $("#search-result-div").hide();
            $(".text-search").blur(); 
        }
    });
    $('.text-search').on('click', function (e) {
        $("#search-result-div").show();
    });
    $('[data-toggle="tooltip"]').tooltip();
    $( window ).resize(function() {
        var width = $(document).width();
        if(width >768){
            $('.content-body').css("padding-left","10px");
        }else{
            $('.content-body').css("padding-left","0px");
            $('.left-content').css("position","relative");
            $('.left-content').css("top","0");
        }
    });
    
    $(window).scroll(function() {
        var height = $('#myCarousel').height();
        var scrollTop = $(window).scrollTop(); 
        var width = $(document).width();  
        if(scrollTop > height) {
            if(width >992){
                var left1Height = $('.right1').height();
                $('.left-content').css("position","fixed");
                $('.right1').css("top","63px");
                $('.left0').css("top","63px");
                $('.right2').css("top",left1Height+85);
                $('.right2').css("margin-top","10px");
            }else if(width <991 && width >768){
                var left0Height = $('.left0').height();
                $('.left1').css("position","fixed");
                $('.left0').css("position","fixed");
                $('.left0').css("top","63px");
                $('.left1').css("top",left0Height+85);
                $('.left1').css("margin-top","10px");
            }
        }
        else{
            $('.left-content').css("position","relative");
            $('.left-content').css("top","0");
            $('.left2').css("margin-top","0px");
        }
    });
    (function($) {
      $.fn.setCursorPosition = function(pos) {
        if ($(this).get(0).setSelectionRange) {
          $(this).get(0).setSelectionRange(pos, pos);
        } else if ($(this).get(0).createTextRange) {
          var range = $(this).get(0).createTextRange();
          range.collapse(true);
          range.moveEnd('character', pos);
          range.moveStart('character', pos);
          range.select();
        }
      }
    }(jQuery));

        $("#txtAskQuestion").focus(function(){
            $("#txtAskQuestion").keyup(function(){
                if ($(this).val().split('').pop() !== '?') {
                    $(this).val($(this).val() + "?");
                    $(this).setCursorPosition( $(this).val().length - 1)
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();
});
    var ALERT_TITLE = "Thông báo";
    var ALERT_BUTTON_TEXT = "Ok";
    var CONFIRM_BUTTON_TEXT1 = "Đồng ý";
    var CONFIRM_BUTTON_TEXT2 = "Hủy";

    if(document.getElementById) {
        window.alert = function(txt) {
            createCustomAlert(txt);
        }
        window.confirm = function(txt) {
            createCustomConfirm(txt);
        }
    }

    function createCustomAlert(txt) {
        d = document;

        if(d.getElementById("modalContainer")) return;

        mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
        mObj.id = "modalContainer";
        mObj.style.height = d.documentElement.scrollHeight + "px";
        
        alertObj = mObj.appendChild(d.createElement("div"));
        alertObj.id = "alertBox";
        if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
        alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth)/2 + "px";
        alertObj.style.visiblity="visible";

        h1 = alertObj.appendChild(d.createElement("div"));
        h1.appendChild(d.createTextNode(ALERT_TITLE));

        msg = alertObj.appendChild(d.createElement("p"));
        //msg.appendChild(d.createTextNode(txt));
        msg.innerHTML = txt;

        btn = alertObj.appendChild(d.createElement("a"));
        btn.id = "closeBtn";
        btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
        btn.href = "#";
        btn.focus();
        btn.onclick = function() { removeCustomAlert();return false; }

        alertObj.style.display = "block";
        alertObj.style.animationDuration = "0.7s";
        alertObj.style.animationName = "fadeIn";
        
    }
    function createCustomConfirm(txt) {
        d = document;

        if(d.getElementById("modalContainer")) return;

        mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
        mObj.id = "modalContainer";
        mObj.style.height = d.documentElement.scrollHeight + "px";
        
        alertObj = mObj.appendChild(d.createElement("div"));
        alertObj.id = "alertBox";
        if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
        alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth)/2 + "px";
        alertObj.style.visiblity="visible";

        h1 = alertObj.appendChild(d.createElement("div"));
        h1.appendChild(d.createTextNode(ALERT_TITLE));

        msg = alertObj.appendChild(d.createElement("p"));
        //msg.appendChild(d.createTextNode(txt));
        msg.innerHTML = txt;

        btnOk = alertObj.appendChild(d.createElement("a"));
        btnHuy = alertObj.appendChild(d.createElement("a"));
        btnOk.id = "okBtn";
        btnHuy.id = "huyBtn";
        btnOk.appendChild(d.createTextNode(CONFIRM_BUTTON_TEXT1));
        btnHuy.appendChild(d.createTextNode(CONFIRM_BUTTON_TEXT2));
        btnOk.href = "#";
        btnHuy.href = "#";
        btnOk.focus();
        btnHuy.focus();
        btnOk.onclick = function() { removeCustomAlert();return false; }
        btnHuy.onclick = function() { removeCustomAlert();return false; }

        alertObj.style.display = "block";
        alertObj.style.animationDuration = "0.7s";
        alertObj.style.animationName = "fadeIn";
        
    }

    function removeCustomAlert() {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
    }
