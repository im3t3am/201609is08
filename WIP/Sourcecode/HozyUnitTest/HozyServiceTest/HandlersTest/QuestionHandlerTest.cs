﻿using System;
using System.Collections.Generic;
using System.Linq;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.DTOs;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class QuestionHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        private QuestionHandler QuestionHandler = new QuestionHandler(dbContext);

        [TestMethod]
        public void DeleteQuestionTest()
        {
            int questionId = 1;
            Session session = new Session();
            Right right = new Right();
            right.RightName = "admin";
            right.Id = 1;
            session.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            session.UserId = 2;
            session.RightList = new List<Right>();
            session.RightList.Add(right);
            QuestionHandler.DeleteQuestion(questionId, session);
        }

        [TestMethod]
        public void IncreaseViewCountTest()
        {
            int questionid = 1;
            QuestionHandler.IncreaseViewCount(questionid);
        }

        [TestMethod]
        public void GetQuestionTest()
        {
            int id = 1;
            QuestionHandler.GetQuestion(id);
        }

        [TestMethod]
        public void AddQuestionTest()
        {
            QuestionDto questionDto = new QuestionDto();
            questionDto.QuestionTitle = "This is for test add question";
            questionDto.QuestionContent = "This is for test add question";
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.AddQuestion(questionDto, currentSession);
        }

        [TestMethod]
        public void UpdateQuestionTest()
        {
            QuestionDto questionDto = new QuestionDto();
            questionDto.QuestionTitle = "This is for test update question";
            questionDto.QuestionContent = "This is for test update question";
            int id = 18;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.UpdateQuestion(questionDto, id, currentSession);
        }

        [TestMethod]
        public void GetTopQuestionTest()
        {
            int skip = 3;
            QuestionHandler.GetTopQuestion(skip);
        }

        [TestMethod]
        public void GetNewQuestionTest()
        {
            int skip = 3;
            QuestionHandler.GetNewQuestion(skip);
        }

        [TestMethod]
        public void AddQuestionSubscribeTest()
        {
            int questionid = 18;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.AddQuestionSubscribe(questionid, currentSession);
        }

        [TestMethod]
        public void RemoveQuestionSubscribeTest()
        {
            int questionid = 18;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.RemoveQuestionSubscribe(questionid, currentSession);
        }

        [TestMethod]
        public void GetQuestionReportedTest()
        {
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.GetQuestionReported(currentSession);
        }

        [TestMethod]
        public void ListQuestionFollowedTest()
        {
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.ListQuestionFollowed(currentSession);
        }

        [TestMethod]
        public void ListUserFollowedTest()
        {
            int qid = 2;
            QuestionHandler.ListUserFollowed(qid);
        }

        [TestMethod]
        public void IsFollowingTest()
        {
            int questionid = 2;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.IsFollowing(questionid, currentSession);
        }

        [TestMethod]
        public void IsReportedTest()
        {
            int questionid = 2;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            QuestionHandler.IsReported(questionid, currentSession);
        }


    }
}
