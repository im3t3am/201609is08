﻿using System;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class NotificationHandlerTest
    {
        
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        NotificationHandler NotificationHandler = new NotificationHandler(dbContext);

        [TestMethod]
        public void GetNotificationTest()
        {
            Session session = new Session();
            session.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            session.UserId = 2;
            dbContext.Sessions.Add(session);
            NotificationHandler.GetNotification(session);
            dbContext.Sessions.Remove(session);
            dbContext.SaveChanges();
        }
    }
}
