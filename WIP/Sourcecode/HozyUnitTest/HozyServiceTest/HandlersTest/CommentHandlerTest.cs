﻿using System;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.DTOs;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class CommentHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        CommentHandler CommentHandler = new CommentHandler(dbContext);

        [TestMethod]
        public void GetCommentListTest()
        {
            int id = 1;
            CommentHandler.GetCommentList(id);
        }

        [TestMethod]
        public void AddCommentTest()
        {
            CommentDto commentDto = new CommentDto();
            commentDto.CommentContent = "This is for test add comment";
            commentDto.ParentCmtId = 9;
            Session session = new Session();
            session.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            session.UserId = 2;
            dbContext.Sessions.Add(session);
            CommentHandler.AddComment(commentDto, session);
            dbContext.Sessions.Remove(session);
            dbContext.SaveChanges();
        }

        [TestMethod]
        public void UpdateCommentTest()
        {
            CommentDto commentDto = new CommentDto();
            commentDto.CommentContent = "This is for test update comment";
            Session session = new Session();
            session.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            session.UserId = 2;
            dbContext.Sessions.Add(session);
            int id = 38;
            CommentHandler.UpdateComment(commentDto,id, session);
        }
    }
}
