﻿using System;
using System.Linq;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.DTOs;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HozyService.Services;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class AnswerHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        private AnswerHandler AnswerHandler = new AnswerHandler(dbContext);

        [TestMethod]
        public void GetAnswerTest()
        {
            int id = 2;
            AnswerHandler.GetAnswer(id);
        }

        [TestMethod]
        public void AddAnswerTest()
        {
            AnswerCreateDto answerCreateDto = new AnswerCreateDto();
            answerCreateDto.CommentContent = "This is for test add answer";
            answerCreateDto.QuestionId = 2;
            Session session = new Session();
            session.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            session.UserId = 2;
            dbContext.Sessions.Add(session);
            AnswerHandler.AddAnswer(answerCreateDto, session);
            dbContext.Sessions.Remove(session);
            dbContext.SaveChanges();
        }

        [TestMethod]
        public void UpdateAnswerTest()
        {
            AnswerDto answerDto = new AnswerDto();
            answerDto.CommentId = 3;
            answerDto.CommentContent = "This is for test update answer";
            Session session = new Session();
            session.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            session.UserId = 2;
            dbContext.Sessions.Add(session);
            dbContext.SaveChanges();
            AnswerHandler.UpdateAnswer(answerDto, session);
            dbContext.Sessions.Remove(session);
            dbContext.SaveChanges();
        }
    }
}
