﻿using System;
using HozyDb.EfDbServices;
using HozyService.DTOs;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class UserHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        private UserHandler UserHandler = new UserHandler(dbContext);

        [TestMethod]
        public void GetUserTest()
        {
            int id = 1;
            UserHandler.GetUser(id);
        }

        [TestMethod]
        public void GenerateFeedTest()
        {
            int id = 1;
            UserHandler.GenerateFeed(id);
        }

        [TestMethod]
        public void GetQuestionOfUserTest()
        {
            int id = 1;
            UserHandler.GetQuestionOfUser(id);
        }

        [TestMethod]
        public void GetAnswerOfUserTest()
        {
            int id = 1;
            UserHandler.GetAnswerOfUser(id);
        }

        [TestMethod]
        public void UserGetFeedTest()
        {
            int id = 1;
            int skip = 2;
            UserHandler.UserGetFeed(id, skip);
        }

        [TestMethod]
        public void UserGetNewQuestionTest()
        {
            int id = 1;
            int skip = 2;
            UserHandler.UserGetNewQuestion(id, skip);
        }
    }
}
