﻿using System;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.DTOs;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class ReportHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        private ReportHandler ReportHandler = new ReportHandler(dbContext);

        [TestMethod]
        public void AddReportTest()
        {
            ReportCreateDto reportCreateDto = new ReportCreateDto();
            reportCreateDto.QuestionId = 1;
            reportCreateDto.ReportContent = "This is for report test";
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            ReportHandler.AddReport(reportCreateDto, currentSession);
        }

        [TestMethod]
        public void GetReportTest()
        {
            int id = 1;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            ReportHandler.GetReport(id, currentSession);
        }
    }
}
