﻿using System;
using System.Diagnostics;
using HozyDb.EfDbServices;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class SearchHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        SearchHandler SearchHandler = new SearchHandler(dbContext);

        [TestMethod]
        public void SearchTopicTest()
        {
            SearchHandler.SearchTopic("Hozy");
        }

        [TestMethod]
        public void SearchQuestionsTest()
        {
            SearchHandler.SearchQuestions("Hozy");
        }
    }
}
