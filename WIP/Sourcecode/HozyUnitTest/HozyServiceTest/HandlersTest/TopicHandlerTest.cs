﻿using System;
using System.Collections.Generic;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.DTOs;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class TopicHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        private TopicHandler TopicHandler = new TopicHandler(dbContext);

        [TestMethod]
        public void AddTopicTest()
        {
            TopicDto topicDto = new TopicDto();
            topicDto.Name = "Topic test";
            topicDto.Description = "Topic test";
            TopicHandler.AddTopic(topicDto);
        }

        [TestMethod]
        public void AddUserTopicTest()
        {
            int id = 1;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            TopicHandler.AddUserTopic(id, currentSession);
        }

        [TestMethod]
        public void AddQuestionTopicTest()
        {
            int topicid = 1;
            int questionid = 1;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            TopicHandler.AddQuestionTopic(topicid, questionid, currentSession);
        }

        [TestMethod]
        public void UserUnfollowTest()
        {
            int topicid = 1;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            TopicHandler.UserUnfollow(topicid, currentSession);
        }

        [TestMethod]
        public void QuestionUnfollowTest()
        {
            int topicid = 1;
            int questionid = 1;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            TopicHandler.QuestionUnfollow(topicid, questionid, currentSession);
        }

        [TestMethod]
        public void GetUserTopicTest()
        {
            int userid = 1;
            TopicHandler.GetUserTopic(userid);
        }

        [TestMethod]
        public void GetTopicNewQuestionTest()
        {
            int topicId = 1;
            int skip = 2;
            TopicHandler.GetTopicNewQuestion(topicId, skip);
        }

        [TestMethod]
        public void GetTopicTopQuestionTest()
        {
            int topicId = 1;
            int skip = 2;
            TopicHandler.GetTopicTopQuestion(topicId, skip);
        }

        [TestMethod]
        public void GetHotTopicTest()
        {
            TopicHandler.GetHotTopic();
        }

        [TestMethod]
        public void GetDetailTopicTest()
        {
            int topicId = 1;
            TopicHandler.GetDetailTopic(topicId);
        }

        [TestMethod]
        public void GetRandomTopicsTest()
        {
            int amount = 1;
            TopicHandler.GetRandomTopics(amount);
        }

        [TestMethod]
        public void UpdateTopicTest()
        {
            TopicDto topicDto = new TopicDto();
            topicDto.Name = "Topic test";
            topicDto.Description = "Topic test";
            int id = 1;
            Session currentSession = new Session();
            Right right = new Right();
            right.RightName = "admin";
            right.Id = 1;
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            currentSession.RightList = new List<Right>();
            currentSession.RightList.Add(right);
            TopicHandler.UpdateTopic(topicDto, id, currentSession);
        }
    }
}
