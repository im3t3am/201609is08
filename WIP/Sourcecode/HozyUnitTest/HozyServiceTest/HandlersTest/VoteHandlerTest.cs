﻿using System;
using HozyDb.EfDbServices;
using HozyDb.Entities;
using HozyService.DTOs;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class VoteHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        private VoteHandler VoteHandler = new VoteHandler(dbContext);

        [TestMethod]
        public void VoteQuestionTest()
        {
            VoteDto voteDto = new VoteDto();
            voteDto.VoteTarget = 1;
            voteDto.VoteTargetId = 2;
            voteDto.VoteValue = true;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            VoteHandler.VoteQuestion(voteDto, currentSession);
        }

        [TestMethod]
        public void NotificationJobsTest()
        {
            Question question = new Question();
            User user =  new User();
            Notification noti = new Notification();
            user.Id = 2;
            user.Username = "minh@gmail.com";
            question.User = user;
            User votingUser = new User();
            votingUser.Id = 2;
            votingUser.Username = "minh@gmail.com";
            long time = 123;
            VoteHandler.NotificationJobs(question, votingUser, noti);
        }

        [TestMethod]
        public void VoteAnswerTest()
        {
            VoteDto voteDto = new VoteDto();
            voteDto.VoteTarget = 1;
            voteDto.VoteTargetId = 2;
            voteDto.VoteValue = true;
            Session currentSession = new Session();
            currentSession.Token = "ee6a47c3-6556-4550-8274-7800b19e7a5f";
            currentSession.UserId = 2;
            VoteHandler.VoteAnswer(voteDto, currentSession);
        }
    }
}
