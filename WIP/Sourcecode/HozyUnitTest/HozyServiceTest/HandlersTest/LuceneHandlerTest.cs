﻿using System;
using HozyDb.EfDbServices;
using HozyService.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyServiceTest.HandlersTest
{
    [TestClass]
    public class LuceneHandlerTest
    {
        public static string connstr = "Data Source=(local);Initial Catalog=Hozy;Integrated Security=True";
        public static HozyDbContext dbContext = new HozyDbContext(connstr);
        LuceneHandler LuceneHandler = new LuceneHandler(dbContext);

        [TestMethod]
        public void createIndexTest()
        {
            LuceneHandler.CreateFullIndex();
        }

        [TestMethod]
        public void CreateDocumentTest()
        {
            string id = "2";
            string questionTitle = "";
            string questionContent = "";
            string comment = "";
            LuceneHandler.CreateDocument(id, questionTitle, questionContent , comment);
        }
    }
}
