﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Mime;
using HozyDb.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HozyUnitTest.HozyBaseTest.Utilities
{
    [TestClass]
    public class UtilityTest
    {
        [TestMethod]
        public void GetHashSha256Test()
        {
            Utility.GetHashSha256("Hozy");
        }

        [TestMethod]
        public void PasswordHashTest()
        {
            Utility.GetHashSha256("Hozy");
        }

        [TestMethod]
        public void ReadToEndTest()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Utility.ReadToEnd(stream);
            }
            
        }

        [TestMethod]
        public void SetRespondTest()
        {
            HttpStatusCode currentStatus = HttpStatusCode.OK;
            string message = "Hozy";
            Utility.SetRespond(currentStatus, message);
        }

        [TestMethod]
        public void Log4NetTest1()
        {
            int mode = 0;
            string loginfo = "Hozy";
            Utility.Log4Net(mode, loginfo);
        }
        [TestMethod]
        public void Log4NetTest2()
        {
            int mode = 1;
            string loginfo = "Hozy";
            Utility.Log4Net(mode, loginfo);
        }
    }
}
