﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class Report
    {
        public int Id { get; set; }

        public String ReportContent { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual  User User { get; set; }

        public virtual Question Question { get; set; }
    }
}
