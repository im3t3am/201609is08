﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class TopQuestion
    {
        public int Id { get; set; }

        public virtual List<Question> Questions { get; set; }
        public int CommentAnswerCountToDate { get; set; }
        public int QuestionCountToDate { get; set; }
        public DateTime CreationDate { get; set; }
        
    }
}
