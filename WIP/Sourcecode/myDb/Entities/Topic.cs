﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class Topic
    {
        public int Id { get; set; }

        [Index("TopicName", IsUnique = true)]
        [MaxLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastQuestionDate { get; set; }

        public string Image { get; set; }

        public virtual List<User> SubscribedUsers { get; set; } 
        public virtual List<Question> TaggedQuestions { get; set; }
        public virtual List<TopTopic> TopTopics { get; set; } 
    }
}
