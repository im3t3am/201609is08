﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class AmaAnswer
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }

        //public virtual AmaQuestion AmaQuestion { get; set; }

        public string Content { get; set; }
    }
}
