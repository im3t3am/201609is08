﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class TopTopic
    {
        public int Id { get; set; }
        
        public virtual List<Topic> Topics { get; set; }
        public virtual DateTime CreationDate { get; set; }
    }
}
