﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class Right
    {

        public int Id { get; set; }

        public String RightName { get; set; }

        public virtual List<User> Users {get; set; }

        public virtual List<Session> Sessions { get; set; } 
    }
}
