﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class AmaQuestion
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }

        [Required]
        public virtual AmaSession AmaSession { get; set; }
        public virtual User AskUser { get; set; }
        public string Content { get; set; }
        public virtual AmaAnswer AmaAnswer { get; set; }
    }
}
