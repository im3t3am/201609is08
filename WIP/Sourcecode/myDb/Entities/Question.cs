﻿using System;
using System.Collections.Generic;

namespace HozyDb.Entities
{
    public class Question
    {
        public int Id { get; set; }

        public String QuestionTitle { get; set; }

        public String QuestionContent { get; set; }

        public bool isDeleted { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime LastInteraction { get; set; }
        public DateTime LastRanked { get; set; }

        public int Rank { get; set; }
        public int ViewCount { get; set; }
        public Boolean IsReported { get; set; }

        public virtual List<Vote> Votes { get; set; }

        public virtual User User { get; set; }
        public virtual List<Comment> CommentList { get; set; }
        public virtual List<Topic> Topics { get; set; } 
        public virtual List<Feed> Feeds { get; set; } 

        public virtual List<User> SubscribedUsers { get; set; } 
        public virtual List<TopQuestion> TopQuestions { get; set; }
        public virtual List<Report> Reports { get; set; } 
    }
}
