﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;

namespace HozyDb.Entities
{
    public class User
    {
        public int Id { get; set; }

        [Index("UsernameIndex",IsUnique = true)]
        [MaxLength(30)]
        public String Username { get; set; }

        [StringLength(128)]
        public String Password { get; set; }

        [MaxLength(30)]
        public  String DisplayName { get; set; }

        public String Description { get; set; }

        public String DateOfBirth { get; set; }

        public String Email { get; set; }

        public String Address { get; set; }

        public String Website { get; set; }
         
        public String Employment { get; set; }

        public String Education { get; set; }

        public String Avatar { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastLogin { get; set; }

        public String FacebookUrl { get; set; }

        public String Cover { get; set; }

        public virtual List<Right> RightList { get; set; }
        public virtual List<Topic> SubscribedTopics { get; set; }
        public virtual List<Comment> CommentList { get; set; }
        public virtual Feed Feed {get; set; }
        public virtual List<Report> Report { get; set; }
        public virtual List<AmaSession> AmaSessions { get; set; } 

        public User()
        { 
            Avatar = "http://i.imgur.com/hUQGGwE.png";
        }
    }
}
