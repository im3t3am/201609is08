﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class Notification
    {
        public int Id { get; set; }

        public String Description { get; set; }

        //mark that user is read this noti or not 
        public bool IsChecked { get; set; }

        public bool IsVote { get; set; }
        public DateTime CreationDate { get; set; }

        public int QuestionId { get; set; }

        //Id cua thang answer/upvote
        public int UserId { get; set; }
         
        //Id cua thang can noti
        public int TargetUserId { get; set; }

        public int CommentId { get; set; }
    }
}
