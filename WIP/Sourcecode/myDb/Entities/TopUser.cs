﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public static class TopUser
    {
        public static List<User> TopUsers { get; set; }
        public static DateTime CreationDate { get; set; }
    }
}
