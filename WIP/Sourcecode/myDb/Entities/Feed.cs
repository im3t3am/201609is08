﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class Feed
    {
        public Feed()
        {
            QuestionFeed = new List<Question>();
        }

        [Key, ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }
        
        public virtual List<Question> QuestionFeed { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
