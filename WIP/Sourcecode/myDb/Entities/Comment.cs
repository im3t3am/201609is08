﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public String Content { get; set; }
        public DateTime CreationDate { get; set; }
        public virtual User User { get; set; }
        public virtual Question Question { get; set; }

        public virtual List<Vote> Votes { get; set; } 

        public bool MarkedHelpful { get; set; }
        public bool isDeleted { get; set; }

        public int? ParentCommentID { get; set; }

        [ForeignKey("ParentCommentID")]
        public virtual Comment ParentComment { get; set; }

        [InverseProperty("ParentComment")]
        public virtual List<Comment> ChildComments { get; set; }
    }
}
