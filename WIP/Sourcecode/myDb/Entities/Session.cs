﻿using System;
using System.Collections.Generic;

namespace HozyDb.Entities
{
    public class Session
    {
        public Session()
        {
            
        }

        public int Id { get; set; }

        public int UserId { get; set; }
        public String Username { get; set; }
        public String Token { get; set; }
        public String IpAddress { get; set; }
        public virtual List<Right> RightList { get; set; } 
    }
}
