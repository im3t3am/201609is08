﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class Vote
    {
        public int Id { get; set; }

        //Người tạo ra Vote
        public virtual User User { get; set; }

        //Giá trị vote : true = + 1 .  False = -1. Nếu bỏ vote/null thì sẽ xóa Vote khỏi DB
        public bool State { get; set; }
    }
}
