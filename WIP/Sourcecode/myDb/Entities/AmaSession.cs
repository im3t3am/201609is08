﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class AmaSession
    {
        public int Id { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime OpenDate { get; set; }

        public virtual User AmaOwner { get; set; }

        public virtual List<AmaQuestion> AmaQuestions { get; set; }

        public string AmaImage { get; set; }

    }
}
