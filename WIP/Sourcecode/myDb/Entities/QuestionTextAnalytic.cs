﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HozyDb.Entities
{
    public class QuestionTextAnalytic
    {
        public int Id { get; set; }

        public virtual Question Question { get; set; }

        public DateTime CreationDate { get; set; }

        public string Sentiment { get; set; }

        public string Agreement { get; set; }

        public string Subjectivity { get; set; }


    }
}
