﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace HozyDb.Utilities
{
    public class Constant
    {
        public static string AnswerNoti = "đã trả lời vào câu hỏi: ";

        public static string ClientAnswerNoti = "đã trả lời vào câu hỏi: ";

        public static string VoteNoti = "đã upvote câu hỏi của bạn: ";

        //public static string ApiKey = "b322ae9d9804726";

        //public static string ApiSecret = "c3425bd7cef1a5762bbe72e0369f2f9ce31c7a7d";


        public static string FaceBookRedirectUrl = "https://hozy.info/mainTemplate.html#/facebook";

        public static string ClientId = "878913002208485";

        public static string ClientSecret = "a01b7ac7c6ccb06db638e425bcb7ee5c";

        public static int MaxTimePoint = 240;
        public static int MaxInteractionTimePoint = 120;
        public static int MaxInteractionPoint = 200;
        public static int MaxQuestionPoint = 500;

        public static int TimeModifier = 1;
        public static int InteractionTimeModifier = 1;
        public static int InteractionModifier = 1;
        public static int QuestionModifier = 1;

        public static int QuestionFillDelay = 15; // in Minute
        public static int QuestionTakeDelay = 180; // in Second
        public static int QuestionToTake = 100;
        public static int QuestionToFill = 1000;
        public static int TopQuestionLogDelay = 480; // in Minute


        public static int TopicRankDelay = 1440; // in Minute
        public static int NewTopicThreshold = 72; // in Hour
        public static int TopicRankNewQuestionModifier = 300;
        public static int RecentQuestionInTopicThreshold = 7;

        public static bool InitialIndex = true;
        public static string GoogleKey = "AIzaSyAczaR_e3CuDAJTHr_uOBHddl8VL2-E9xI";
        public static string MeaningCloudKey = "c1904f465dd0c7f7a1c1cc14e9a37b97";

        public static int UserRankingDelay = 24;
        public static int UserRankingDateRange = 7;

        public static int TopQuestionToLogNumber = 10; // Số lượng câu hỏi được log lại thành TOP, Cũng là số câu hỏi được mang đi xử lí text

        public static string DefaultAvatar = "http://i.imgur.com/hUQGGwE.png";

        public static int UserFeedCreateDelay = 15;

        public static void LoadConstants()
        {
            string file = Utility.GetAssemblyLocation() + "\\Constants.json";
            string json = File.ReadAllText(file);
            dynamic Constants = Json.Decode(json);
            TopicRankDelay = Constants.TopicRankDelay;
            MaxTimePoint = Constants.MaxTimePoint;
            MaxInteractionTimePoint = Constants.MaxInteractionTimePoint;
            MaxInteractionPoint = Constants.MaxInteractionPoint;
            MaxQuestionPoint = Constants.MaxQuestionPoint;
            QuestionFillDelay = Constants.QuestionFillDelay;
            QuestionTakeDelay = Constants.QuestionTakeDelay;
            TopQuestionLogDelay = Constants.TopQuestionLogDelay;
            GoogleKey = Constants.GoogleKey;
            MeaningCloudKey = Constants.MeaningCloudKey;
            FaceBookRedirectUrl = Constants.FaceBookRedirectUrl;
            ClientId = Constants.ClientId;
            ClientSecret = Constants.ClientSecret;
            QuestionToTake = Constants.QuestionToTake;
            QuestionToFill = Constants.QuestionToFill;
            UserFeedCreateDelay = Constants.UserFeedCreateDelay;

            TimeModifier = Constants.TimeModifier;
            InteractionTimeModifier = Constants.InteractionTimeModifier;
            InteractionModifier = Constants.InteractionModifier;
            QuestionModifier = Constants.QuestionModifier;

            if (Constants.Testmode)
            {
                TestMode();
            }

            Utility.Log4Net(1, json);
        }

        public static void TestMode()
        {
            QuestionFillDelay = 1;
            QuestionTakeDelay = 20;
            TopicRankDelay = 1;
            TopQuestionLogDelay = 1;
        }
    }
}
