using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using HozyDb.EfDbServices;
using HozyDb.Entities;

namespace HozyDb.EfDbServices
{
    
    public class HozyDbContext : DbContext
    {
        public HozyDbContext(string connstring) : base(connstring)
        {
            Database.SetInitializer<HozyDbContext>(new HozyDbInitializer());
            Database.Connection.ConnectionString = connstring;
        }

        public HozyDbContext()
            : base("SQLSERVER")
        {
            Database.SetInitializer<HozyDbContext>(new HozyDbInitializer());
        }

        public DbSet<Session> Sessions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Right> Rights { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<Topic> Topics { get; set; } 
        public DbSet<Feed> Feeds { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<TopTopic> TopTopics { get; set; }
        public DbSet<TopQuestion> TopQuestions { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<QuestionTextAnalytic> QuestionTextAnalytics { get; set; }
        public DbSet<AmaSession> AmaSessions { get; set; }
        public DbSet<AmaQuestion> AmaQuestions { get; set; }
        public DbSet<AmaAnswer> AmaAnswers { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Question>()
                .HasMany(x => x.SubscribedUsers)
                .WithMany()
                .Map(x =>
                {
                    x.ToTable("QuestionSubscription"); // third table is named QuestionSubscription
                    x.MapLeftKey("QuestionId");
                    x.MapRightKey("UserId");
                });
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Entity<AmaQuestion>().HasOptional(x => x.AmaAnswer).WithOptionalPrincipal();
        }
    }
}
