﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Security.Permissions;
using HozyDb.Entities;
using HozyDb.Utilities;

namespace HozyDb.EfDbServices
{
    class HozyDbInitializer : System.Data.Entity.CreateDatabaseIfNotExists<HozyDbContext>
    {
        protected override void Seed(HozyDbContext context)
        {
            

            #region User
            Right adminRight = new Right();
            adminRight.RightName = "admin";


            Right userRight = new Right();
            userRight.RightName = "user";

            // set adminitrator
            User adminUser = new User();
            adminUser.Username = "admin@gmail.com";
            adminUser.DisplayName = "admin";
            adminUser.Password = Utility.PasswordHash("admin1234");

            adminUser.RightList = new List<Right>();
            adminUser.RightList.Add(adminRight);
            // set normal user
            User normalUser1 = new User();
            normalUser1.Username = "user1@gmail.com";
            normalUser1.DisplayName = "Nguyễn Văn Nam";
            normalUser1.Password = Utility.PasswordHash("user1234");
            normalUser1.RightList = new List<Right>();
            normalUser1.RightList.Add(userRight);
            normalUser1.Address = "Hải Dương";
            normalUser1.DateOfBirth = "01/01/1995";
            normalUser1.Education = "Đại học quốc gia Hà Nội";
            normalUser1.Email = "user1@gmail.com";
            normalUser1.Employment = "FPT Coporation";

            User normalUser2 = new User();
            normalUser2.Username = "user2@gmail.com";
            normalUser2.DisplayName = "Dương Đức Chung";
            normalUser2.Password = Utility.PasswordHash("user1234");
            normalUser2.RightList = new List<Right>();
            normalUser2.RightList.Add(userRight);
            normalUser2.Address = "Hải Dương";
            normalUser2.DateOfBirth = "02/02/1994";
            normalUser2.Education = "Đại học quốc gia Hà Nội";
            normalUser2.Email = "user2gmail.com";
            normalUser2.Employment = "FPT Coporation";

            User normalUser3 = new User();
            normalUser3.Username = "user3@gmail.com";
            normalUser3.DisplayName = "Đỗ Nam Trăm";
            normalUser3.Password = Utility.PasswordHash("user1234");
            normalUser3.RightList = new List<Right>();
            normalUser3.RightList.Add(userRight);
            normalUser3.Address = "Hưng Yên";
            normalUser3.DateOfBirth = "01/03/1993";
            normalUser3.Education = "Đại học kinh tế quốc dân";
            normalUser3.Email = "user3@gmail.com";
            normalUser3.Employment = "Boss tại gia";

            User normalUser4 = new User();
            normalUser4.Username = "user4@gmail.com";
            normalUser4.DisplayName = "Đỗ Nam Trung";
            normalUser4.Password = Utility.PasswordHash("user1234");
            normalUser4.RightList = new List<Right>();
            normalUser4.RightList.Add(userRight);
            normalUser4.Address = "New York";
            normalUser4.DateOfBirth = "05/03/1986";
            normalUser4.Education = "Đại học kinh tế quốc dân";
            normalUser4.Email = "user4@gmail.com";
            normalUser4.Employment = "CEO of Trung company";

            User normalUser5 = new User();
            normalUser5.Username = "user5@gmail.com";
            normalUser5.DisplayName = "Nguyễn Xuân Trường";
            normalUser5.Password = Utility.PasswordHash("user1234");
            normalUser5.RightList = new List<Right>();
            normalUser5.RightList.Add(userRight);
            normalUser4.Address = "TP Hồ Chí Minh";
            normalUser4.DateOfBirth = "12/07/1980";
            normalUser4.Education = "Đại học Boston";
            normalUser4.Email = "user5@gmail.com";
            normalUser4.Employment = "Cầu thủ bóng đá";

            User normalUser6 = new User();
            normalUser6.Username = "user6@gmail.com";
            normalUser6.DisplayName = "user6";
            normalUser6.Password = Utility.PasswordHash("user1234");
            normalUser6.RightList = new List<Right>();
            normalUser6.RightList.Add(userRight);

            User normalUser7 = new User();
            normalUser7.Username = "user7@gmail.com";
            normalUser7.DisplayName = "user7";
            normalUser7.Password = Utility.PasswordHash("user1234");
            normalUser7.RightList = new List<Right>();
            normalUser7.RightList.Add(userRight);

            User normalUser8 = new User();
            normalUser8.Username = "user8@gmail.com";
            normalUser8.DisplayName = "user8";
            normalUser8.Password = Utility.PasswordHash("user1234");
            normalUser8.RightList = new List<Right>();
            normalUser8.RightList.Add(userRight);

            User normalUser9 = new User();
            normalUser9.Username = "user9@gmail.com";
            normalUser9.DisplayName = "user9";
            normalUser9.Password = Utility.PasswordHash("user1234");
            normalUser9.RightList = new List<Right>();
            normalUser9.RightList.Add(userRight);

            User normalUser10 = new User();
            normalUser10.Username = "user10@gmail.com";
            normalUser10.DisplayName = "user10";
            normalUser10.Password = Utility.PasswordHash("user1234");
            normalUser10.RightList = new List<Right>();
            normalUser10.RightList.Add(userRight);

            User normalUser11 = new User();
            normalUser11.Username = "user11@gmail.com";
            normalUser11.DisplayName = "user11";
            normalUser11.Password = Utility.PasswordHash("user1234");
            normalUser11.RightList = new List<Right>();
            normalUser11.RightList.Add(userRight);

            User normalUser12 = new User();
            normalUser12.Username = "user12@gmail.com";
            normalUser12.DisplayName = "user12";
            normalUser12.Password = Utility.PasswordHash("user1234");
            normalUser12.RightList = new List<Right>();
            normalUser12.RightList.Add(userRight);

            User normalUser13 = new User();
            normalUser13.Username = "user13@gmail.com";
            normalUser13.DisplayName = "user13";
            normalUser13.Password = Utility.PasswordHash("user1234");
            normalUser13.RightList = new List<Right>();
            normalUser13.RightList.Add(userRight);

            User normalUser14 = new User();
            normalUser14.Username = "user14@gmail.com";
            normalUser14.DisplayName = "user14";
            normalUser14.Password = Utility.PasswordHash("user1234");
            normalUser14.RightList = new List<Right>();
            normalUser14.RightList.Add(userRight);

            User normalUser15 = new User();
            normalUser15.Username = "user15@gmail.com";
            normalUser15.DisplayName = "user15";
            normalUser15.Password = Utility.PasswordHash("user1234");
            normalUser15.RightList = new List<Right>();
            normalUser15.RightList.Add(userRight);

            User normalUser16 = new User();
            normalUser16.Username = "user16@gmail.com";
            normalUser16.DisplayName = "user16";
            normalUser16.Password = Utility.PasswordHash("user1234");
            normalUser16.RightList = new List<Right>();
            normalUser16.RightList.Add(userRight);

            User normalUser17 = new User();
            normalUser17.Username = "user17@gmail.com";
            normalUser17.DisplayName = "user17";
            normalUser17.Password = Utility.PasswordHash("user1234");
            normalUser17.RightList = new List<Right>();
            normalUser17.RightList.Add(userRight);

            User normalUser18 = new User();
            normalUser18.Username = "user18@gmail.com";
            normalUser18.DisplayName = "user18";
            normalUser18.Password = Utility.PasswordHash("user1234");
            normalUser18.RightList = new List<Right>();
            normalUser18.RightList.Add(userRight);

            User normalUser19 = new User();
            normalUser19.Username = "user19@gmail.com";
            normalUser19.DisplayName = "user19";
            normalUser19.Password = Utility.PasswordHash("user1234");
            normalUser19.RightList = new List<Right>();
            normalUser19.RightList.Add(userRight);

            User normalUser20 = new User();
            normalUser20.Username = "user20@gmail.com";
            normalUser20.DisplayName = "user20@gmail.com";
            normalUser20.Password = Utility.PasswordHash("user1234");
            normalUser20.RightList = new List<Right>();
            normalUser20.RightList.Add(userRight);

            User normalUser21 = new User();
            normalUser21.Username = "user21@gmail.com";
            normalUser21.DisplayName = "user21";
            normalUser21.Password = Utility.PasswordHash("user1234");
            normalUser21.RightList = new List<Right>();
            normalUser21.RightList.Add(userRight);

            User normalUser22 = new User();
            normalUser22.Username = "user22@gmail.com";
            normalUser22.DisplayName = "user22";
            normalUser22.Password = Utility.PasswordHash("user1234");
            normalUser22.RightList = new List<Right>();
            normalUser22.RightList.Add(userRight);

            User normalUser23 = new User();
            normalUser23.Username = "user23@gmail.com";
            normalUser23.DisplayName = "user23";
            normalUser23.Password = Utility.PasswordHash("user1234");
            normalUser23.RightList = new List<Right>();
            normalUser23.RightList.Add(userRight);

            User normalUser24 = new User();
            normalUser24.Username = "user24@gmail.com";
            normalUser24.DisplayName = "user24";
            normalUser24.Password = Utility.PasswordHash("user1234");
            normalUser24.RightList = new List<Right>();
            normalUser24.RightList.Add(userRight);

            User normalUser25 = new User();
            normalUser25.Username = "user25@gmail.com";
            normalUser25.DisplayName = "user25";
            normalUser25.Password = Utility.PasswordHash("user1234");
            normalUser25.RightList = new List<Right>();
            normalUser25.RightList.Add(userRight);

            User normalUser26 = new User();
            normalUser26.Username = "user26@gmail.com";
            normalUser26.DisplayName = "user26";
            normalUser26.Password = Utility.PasswordHash("user1234");
            normalUser26.RightList = new List<Right>();
            normalUser26.RightList.Add(userRight);

            User normalUser27 = new User();
            normalUser27.Username = "user27@gmail.com";
            normalUser27.DisplayName = "user27";
            normalUser27.Password = Utility.PasswordHash("user1234");
            normalUser27.RightList = new List<Right>();
            normalUser27.RightList.Add(userRight);

            User normalUser28 = new User();
            normalUser28.Username = "user28@gmail.com";
            normalUser28.DisplayName = "user28";
            normalUser28.Password = Utility.PasswordHash("user1234");
            normalUser28.RightList = new List<Right>();
            normalUser28.RightList.Add(userRight);

            User normalUser29 = new User();
            normalUser29.Username = "user29@gmail.com";
            normalUser29.DisplayName = "user29";
            normalUser29.Password = Utility.PasswordHash("user1234");
            normalUser29.RightList = new List<Right>();
            normalUser29.RightList.Add(userRight);

            User normalUser30 = new User();
            normalUser30.Username = "user30@gmail.com";
            normalUser30.DisplayName = "user30";
            normalUser30.Password = Utility.PasswordHash("user1234");
            normalUser30.RightList = new List<Right>();
            normalUser30.RightList.Add(userRight); 
            #endregion

             // Set topic
            #region Topic
            Topic topic1 = new Topic();
            Topic topic2 = new Topic();
            Topic topic3 = new Topic();
            Topic topic4 = new Topic();
            Topic topic5 = new Topic();
            Topic topic6 = new Topic();
            Topic topic7 = new Topic();
            Topic topic8 = new Topic();
            Topic topic9 = new Topic();
            Topic topic10 = new Topic();
            Topic topic11 = new Topic();
            Topic topic12 = new Topic();
            Topic topic13 = new Topic();
            Topic topic14 = new Topic();
            Topic topic15 = new Topic();
            Topic topic16 = new Topic();
            Topic topic17 = new Topic();
            Topic topic18 = new Topic();
            Topic topic19 = new Topic();
            Topic topic20 = new Topic();
            


            topic1.Name = "Khoa học máy tính";
            topic1.Description = "Khoa học Máy tính là một trong những ngành học quan trọng tại các trường đại học đào tạo về công nghệ thông tin (CNTT) nói riêng và kỹ thuật nói chung. Đây là ngành học dành cho những bạn trẻ đam mê nghiên cứu chuyên sâu về CNTT, khả năng tính toán của hệ thống máy tính";
            topic1.CreationDate = DateTime.Now;
            topic1.LastQuestionDate = DateTime.Now;


            topic2.Name = "Ẩm thực";
            topic2.Description = "Văn hóa ẩm thực trên toàn thế giới";
            topic2.CreationDate = DateTime.Now;
            topic2.LastQuestionDate = DateTime.Now;


            topic3.Name = "Bóng đá";
            topic3.Description = "Môn thể thao vua";
            topic3.CreationDate = DateTime.Now;
            topic3.LastQuestionDate = DateTime.Now;

            topic4.Name = "Âm nhạc";
            topic4.Description = "Âm nhạc là một bộ môn nghệ thuật dùng âm thanh để diễn đạt.";
            topic4.CreationDate = DateTime.Now;
            topic4.LastQuestionDate = DateTime.Now;

            topic5.Name = "E-sport";
            topic5.Description = "Thể thao điện tử, một lĩnh vực đang có xu hướng phát triển trên toàn thế giới hiện nay. Với nhiều bộ môn như dotA2, Liên minh huyền thoại với những giải đấu lên tới hàng triệu đô tiền thưởng.";
            topic5.CreationDate = DateTime.Now;
            topic5.LastQuestionDate = DateTime.Now;

            topic6.Name = "Văn học Việt Name";
            topic6.Description = "Các tác phẩm văn học Việt Name";
            topic6.CreationDate = DateTime.Now;
            topic6.LastQuestionDate = DateTime.Now;

            topic7.Name = "Gia đình";
            topic7.Description = "Có rất nhiều nơi để đi nhưng chỉ có một nơi để về";
            topic7.CreationDate = DateTime.Now;
            topic7.LastQuestionDate = DateTime.Now;

            topic8.Name = "Động vật";
            topic8.Description = "Thế giới động vật hoang dã";
            topic8.CreationDate = DateTime.Now;
            topic8.LastQuestionDate = DateTime.Now;

            topic9.Name = "Khiêu vũ";
            topic9.Description = "Khiêu vũ là một loại nghệ thuật dùng hoạt động cơ thể để diễn đạt theo âm nhạc nhằm chuyển tải nội dung và diễn đạt ý tưởng."

            +"Trên thế giới có rất nhiều loại Khiêu vũ khác nhau. Mỗi một loại lại mang những nét đặc trưng của nó. Ví dụ: ballet, khiêu vũ hiện đại, vũ điệu dân gian,...";
            topic9.CreationDate = DateTime.Now;
            topic9.LastQuestionDate = DateTime.Now;

            topic10.Name = "IPhone";
            topic10.Description = "Mẫu điện thoại thông minh của công ty Apple";
            topic10.CreationDate = DateTime.Now;
            topic10.LastQuestionDate = DateTime.Now;

            topic11.Name = "Nhạc cổ điển";
            topic11.Description = "Nhạc cổ điển";
            topic11.CreationDate = DateTime.Now;
            topic11.LastQuestionDate = DateTime.Now;

            topic12.Name = "Trịnh Công Sơn";
            topic12.Description = "Dành cho những người yêu mến nhạc Trịnh";
            topic12.CreationDate = DateTime.Now;
            topic12.LastQuestionDate = DateTime.Now;

            topic13.Name = "Dota2";
            topic13.Description = "Thảo luận về những vấn đề liên quan đến game Dota2";
            topic13.CreationDate = DateTime.Now;
            topic13.LastQuestionDate = DateTime.Now;

            topic14.Name = "Liên Minh Huyền Thoại";
            topic14.Description = "Thảo luận về những vấn đề liên quan đến game Liên Minh Huyền Thoại";
            topic14.CreationDate = DateTime.Now;
            topic14.LastQuestionDate = DateTime.Now;

            topic15.Name = "Legendary Moonlight Scuptor";
            topic15.Description = "Truyện thuộc thể loại Action, Adventure, Comedy, Drama, Fantasy, Sci-fi, Light Novel. Legendary Moonlight Sculptor – Con đường Đế Vương là câu chuyện về kẻ bị thế giới ruồng bỏ, kẻ là nô lệ cho đồng tiền, và đồng thời cũng là Thần Chiến Tranh huyền thoại của trò chơi MMORPG nổi tiếng Lục Địa Phép Thuật";
            topic15.CreationDate = DateTime.Now;
            topic15.LastQuestionDate = DateTime.Now;

            topic16.Name = "Cristiano Ronaldo";
            topic16.Description = "Cầu thủ bóng đá nổi tiếng";
            topic16.CreationDate = DateTime.Now;
            topic16.LastQuestionDate = DateTime.Now;

            topic17.Name = "Giáo dục";
            topic17.Description = "Chỉ có quốc gia thu hút những cái đầu vĩ đại hoặc coi trọng giáo dục mới có thể trở nên giàu có.";
            topic17.CreationDate = DateTime.Now;
            topic17.LastQuestionDate = DateTime.Now;

            topic18.Name = "Dinh dưỡng";
            topic18.Description = "Dinh dưỡng cho cơ thể khỏe mạnh.";
            topic18.CreationDate = DateTime.Now;
            topic18.LastQuestionDate = DateTime.Now;

            topic19.Name = "Giải trí";
            topic19.Description = "Thế giới showbiz Việt";
            topic19.CreationDate = DateTime.Now;
            topic19.LastQuestionDate = DateTime.Now;

            topic20.Name = "Môi trường";
            topic20.Description = "Môi trường";
            topic20.CreationDate = DateTime.Now;
            topic20.LastQuestionDate = DateTime.Now;

            context.Topics.Add(topic1);
            context.Topics.Add(topic2);
            context.Topics.Add(topic3);
            context.Topics.Add(topic4);
            context.Topics.Add(topic5);
            context.Topics.Add(topic6);
            context.Topics.Add(topic7);
            context.Topics.Add(topic8);
            context.Topics.Add(topic9);
            context.Topics.Add(topic10);
            context.Topics.Add(topic11);
            context.Topics.Add(topic12);
            context.Topics.Add(topic13);
            context.Topics.Add(topic14);
            context.Topics.Add(topic15);
            context.Topics.Add(topic16);
            context.Topics.Add(topic17);
            context.Topics.Add(topic18);
            context.Topics.Add(topic19);
            context.Topics.Add(topic20);

            
            #endregion
            //Set Question
             
            Question question1 = new Question();
            question1.User = normalUser1;
            question1.QuestionTitle = "Hàng vạn câu hỏi vì sao.";
            question1.QuestionContent = "Vì sao con gái nên tự mình cố gắng thay vì trông chờ vào người khác?";
            question1.CreationDate = DateTime.Now;
            question1.Topics = new List<Topic>();
            question1.Topics.Add(topic1);
            question1.Topics.Add(topic2);
            

            Question question2 = new Question();
            question2.User = normalUser1;
            question2.QuestionTitle = "Loài hổ ở Việt Nam";
            question2.QuestionContent = "Ở Việt Nam hiện nay còn bao nhiêu con hổ tự nhiên?";
            question2.CreationDate = DateTime.Now;
            question2.Topics = new List<Topic>();
            question2.Topics.Add(topic1);
            question2.Topics.Add(topic2);

            Question question3 = new Question();
            question3.User = normalUser2;
            question3.QuestionTitle = "Loài bò sát";
            question3.QuestionContent = "Loại rắn nào là độc nhất và cách sơ cứu khi bị rắn cắn";
            question3.CreationDate = DateTime.Now;
            question3.Topics = new List<Topic>();
            question3.Topics.Add(topic1);
            question3.Topics.Add(topic2);

            Question question4 = new Question();
            question4.User = normalUser2;
            question4.QuestionTitle = "Siêu nhiên";
            question4.QuestionContent = "Làm cách nào để có sức mạnh như Super man?";
            question4.CreationDate = DateTime.Now;
            question4.Topics = new List<Topic>();
            question4.Topics.Add(topic4);
            question4.Topics.Add(topic1);

            Question question5 = new Question();
            question5.User = normalUser3;
            question5.QuestionTitle = "Khoa học";
            question5.QuestionContent = "Có người ngoài hành tinh hay không? Làm sao để liên lạc với họ?";
            question5.CreationDate = DateTime.Now;
            question5.Topics = new List<Topic>();
            question5.Topics.Add(topic2);

            Question question6 = new Question();
            question6.User = normalUser3;
            question6.QuestionTitle = "Cách chưa bệnh";
            question6.QuestionContent = "Cách chữa bệnh viêm họng?";
            question6.CreationDate = DateTime.Now;
            question6.Topics = new List<Topic>();
            question6.Topics.Add(topic5);

            Question question7= new Question();
            question7.User = normalUser4;
            question7.QuestionTitle = "Cách nấu ăn";
            question7.QuestionContent = "Cách nấu món cơm gà cà-ri?";
            question7.CreationDate = DateTime.Now;

            Question question8 = new Question();
            question8.User = normalUser14;
            question8.QuestionTitle = "Khoa học vũ trụ";
            question8.QuestionContent = "Vụ nổ Big Bang là gì?";
            question8.CreationDate = DateTime.Now;

            Question question9 = new Question();
            question9.User = normalUser17;
            question9.QuestionTitle = "Khoa học hạt nhân";
            question9.QuestionContent = "Ứng dụng của nhà máy điện hạt nhân trong cuộc sống?";
            question9.CreationDate = DateTime.Now;

            Question question10 = new Question();
            question10.User = normalUser20;
            question10.QuestionTitle = "Giới trẻ";
            question10.QuestionContent = "Xu thế thời trang hot nhất năm 2016 là gì?";
            question10.CreationDate = DateTime.Now;

            Question question11 = new Question();
            question11.User = normalUser7;
            question11.QuestionTitle = "Văn học";
            question11.QuestionContent = "Tác phẩm Lão Hạc là của nhà văn nào? Ra đời trong hoàn cảnh nào?";
            question11.CreationDate = DateTime.Now;

            Question question12 = new Question();
            question12.User = normalUser13;
            question12.QuestionTitle = "Game dotA2";
            question12.QuestionContent = "Cách build đồ Crystal Maiden carry cân team?";
            question12.CreationDate = DateTime.Now;

            Question question13 = new Question();
            question13.User = normalUser14;
            question13.QuestionTitle = "Game liên minh huyền thoại";
            question13.QuestionContent = "Cách build đồ Ya sua gank tem 20 phút gg?";
            question13.CreationDate = DateTime.Now;

            Question question14 = new Question();
            question14.User = normalUser19;
            question14.QuestionTitle = "Khoa học viễn tưởng";
            question14.QuestionContent = "Cỗ máy thời gian có thực hay không?";
            question14.CreationDate = DateTime.Now;

            Question question15 = new Question();
            question15.User = normalUser7;
            question15.QuestionTitle = "Cá chết đuối";
            question15.QuestionContent = "Lý do nào khiến cả ở hồ tây chết hàng loạt?";
            question15.CreationDate = DateTime.Now;

            Question question16 = new Question();
            question16.User = normalUser10;
            question16.QuestionTitle = "Khoa học";
            question16.QuestionContent = "Tại sao muỗng gỗ ngăn bọt khí trào ra ngoài nồi??";
            question16.CreationDate = DateTime.Now;

            // set topic question
           

            // set Answer
            Comment answer1 = new Comment();
            answer1.User = normalUser4;
            answer1.Question = question1;
            answer1.CreationDate = DateTime.Now;
            answer1.Content = "1. Anh cho em tình yêu là đủ rồi, bánh mì thì để em tự mua."

                                    + "2. Bạn là người thuộc đẳng cấp nào thì sẽ gặp được người ở đẳng cấp ấy. 3. Tôi sẽ luôn cố gắng hết mình, nỗ lực làm việc, để có một ngày, khi được đứng bên cạnh người tôi yêu, dù người ấy giàu sang phú quý hay chỉ có hai bàn tay trắng, tôi cũng có thể giang rộng hai tay, thản nhiên mà ôm lấy người ấy. Người ấy giàu, tôi không mang tiếng trèo cao, người ấy nghèo, hai chúng tôi cũng không phải chán nản. Đây chính là ý nghĩa cho sự cố gắng của người phụ nữ. Không có mệnh công chúa, vậy phải có một trái tim nữ hoàng, dùng thái độ tích cực, sống một cuộc đời hoàn mỹ. 4. Bạn cười tôi phải liều mạng để kiếm tiền, tự khiến bản thân nhếch nhác thảm hại, tôi cười bạn xa rời đàn ông ngay cả ăn cơm cũng khó. "
                                    +
                                    "5. Đem cuộc đời mình, số phận mình đánh cược lên một người khác, trên thế giới này còn có người ngốc nghếch hơn thế nữa ư? "
                                    +
                                    "6. Kiếm được một người chồng tốt quá là khó, thà chị đây tự mình cố gắng nuôi mình còn dễ hơn... "
                                    +
                                    "7. Cơm của cha mẹ, nằm mà ăn. Cơm của bản thân, đứng mà ăn. Cơm của đàn ông, quỳ mà ăn. "
                                    +
                                    "8. Tôi muốn dùng dùng học thức và trí tuệ của chính mình kiếm tiền báo hiếu cha mẹ. Tôi muốn dùng tiền của chính mình, tặng cho con bạn thân cái váy mà nó hằng ao ước nhưng không dám mua. Nếu có thể, tôi cũng bằng lòng dùng tiền tự tay mình kiếm ra để khiến những người tôi yêu cả đời sau này không cần ưu phiền, lo lắng. Tôi nghĩ, tôi muốn nuôi dưỡng giấc mộng riêng của bản thân. "
                                    +
                                    "9. “Em phụ trách xinh đẹp, còn anh lo kiếm tiền nuôi gia đình.” “Anh lúc nào cũng có thể kiếm tiền nuôi gia đình, nhưng em có thể xinh đẹp mãi mãi không?”  ";
           
            // answer to question2 
            Comment answer2 = new Comment();
            answer2.User = normalUser2;
            answer2.Question = question2;
            answer2.CreationDate = DateTime.Now;
            answer2.Content = "Từ 30 con năm 2011, hiện nước ta chỉ còn 5 con hổ sống ngoài tự nhiên, đối diện với nguy cơ tuyệt chủng."
                                    +"Tổ chức bảo tồn động vật hoang dã WWF hôm 12/4 cho biết, Việt Nam chỉ còn khoảng 5 con hổ ở ngoài tự nhiên - đi ngược tiến trình của thế giới đang có xu hướng tăng những năm gần đây."
                                    +"Năm năm trước, theo WWF, Việt Nam có khoảng 30 con hổ ngoài tự nhiên. Với đà này, trong vài năm nữa nước ta chỉ còn hổ nuôi nhốt trong các vườn thú, công viên. Cùng cảnh sụt giảm còn có Lào (2 con) và Campuchia hổ tự nhiên đã tuyệt chủng. Tình trạng này được cho là do tình trạng săn bắn bừa bãi, lấy hổ làm thuốc, cao hổ cốt...";
            Comment answer3 = new Comment();
            answer3.User = normalUser2;
            answer3.Question = question2;
            answer3.CreationDate = DateTime.Now;
            answer3.Content = "Tại khu vực Đông Nam Á, Indonesia có đàn hổ ngoài tự nhiên lớn nhất với 371 con, kế đó là Malaysia 250 con và Thái Lan 189 con."
                                    +"Theo số liệu của WWF, hiện tổng đàn hổ trên toàn thế giới khoảng 3.890 con, tăng 690 con so với năm 2010. Lượng hổ tập trung lớn nhất ở Ấn Độ với 2.226 con, tiếp đến là Nga với 433 con."
                                    +"Hổ được xếp vào loại nguy cấp theo Sách đỏ của Liên minh Bảo tồn Thiên nhiên Quốc tế (IUCN) do nguy cơ săn bắt trái phép và mất sinh cảnh sống. Việt Nam nằm trong vùng phân bố của loài hổ Đông Dương. Thời cao điểm, hàng trăm chú hổ sinh sống tại các vùng rừng núi."
                                    +"Thành lập năm 1961, WWF là một trong những tổ chức phi chính phủ về bảo tồn uy tín nhất thế giới. Tổ chức này đã tài trợ cho 12.000 dự án với mạng lưới hoạt động trên hơn 100 quốc gia.";

            // answer to question 3
            Comment answer4 = new Comment();
            answer4.User = normalUser8;
            answer4.Question = question3;
            answer4.CreationDate = DateTime.Now;
            answer4.Content = "Rắn biển Belcher là loài rắn độc nhất dưới nước cũng là loài rắn độc nhất trên thế giới. Chỉ cần vài miligram nọc độc của chúng là đủ để giết chết hàng ngàn người. Những con rắn này bơi lượn trong nước ấm ở Nam Thái Bình Dương và Ấn Độ Dương. Ngư dân thường là nạn nhân của loài rắn này, họ gặp phải khi kéo lưới lên từ đại dương."
                                +" 'Rắn biển Belcher' có thể thấy ở khắp các vùng biển ngoài khơi Đông Nam Á và Bắc Australia. Thức ăn chủ yếu là cá tra, con rắn biển có mỏ cũng sẽ ăn cá nóc và cá khác hoặc đôi khi loài mực ống. Rất may là chưa đến 1/4 các vết cắn của chúng chứa nọc độc và chúng khá hiền lành.";
            Comment answer5 = new Comment();
            answer5.User = normalUser9;
            answer5.Question = question3;
            answer5.CreationDate = DateTime.Now;
            answer5.Content = "Cách xử lý khi bị rắn cắn"
                                +"Nếu bị nhóm rắn hổ cắn"
                                + "Bước 1: băng ép (garô): Phải nhanh chóng buộc garô (nơi nào có thể garô được) ở phía trên vết cắn 3-5cm. Garô bằng mọi thứ dây tự có tại chỗ như: dây thun, dây chuối, dây quai nón... Chú ý khi garô phải dùng dây bản to để giảm tổn thương nơi garô."
                                + "Bước 2: tẩy nọc bằng cách rửa sạch vết rắn cắn, sau đó đến cơ sở y tế rửa lại bằng thuốc tím 1‰, cồn iốt 2%..."
                                + "Bước 3: rạch rộng vết cắn hình chữ thập (+). Độ sâu qua da đến cơ chảy máu là được, rạch rộng dài khoảng 1-2cm. Trước khi rạch rộng phải sát trùng để tránh nhiễm trùng, tránh rạch đứt dây thần kinh, mạch máu và dây chằng."
                                + "Bước 4: hút máu tại chỗ rắn cắn."
                                + "Bước 5: dùng thuốc đơn giản rồi nhanh chóng đưa nạn nhân đến cơ sở y tế mà ở đó có điều kiện cấp cứu hồi sức.";
            // answer to question 4
            Comment answer6 = new Comment();
            answer6.User = normalUser10;
            answer6.Question = question4;
            answer6.CreationDate = DateTime.Now;
            answer6.Content = "Hấp thụ năng lượng mặt trời: Là người Krypton, Sịp có năng lực hấp thu nguồn bức xạ từ những ngôi sao lớn. Cơ thể người Krypton có thể hút và chứa nguồn năng lượng từ mặt trời vàng của Trái Đất( cho phép sịp trữ năng lượng ngay cả khi ở trong nhà và ban đêm), thứ cung cấp cho sịp sức mạnh vl, bao gồm siêu thể chất, siêu giác quan, da inox và chống lại lực hấp dẫn( bay). Cơ thể sịp cũng có thể hấp thu năng lượn của mặt trời xanh, thứ giúp sịp tăng sức mạnh và những khả năng khác."
                                +"Siêu sức mạnh: Sịp có sức mạnh vật lý phi thường và hầu như không thể xác định được giới hạn. Ban đầu là xe cộ, tàu ngâm, sau đó là chém đinh chặt sắt, nghiền nát kim cương bằng tay, nâng dãy núi Galcier lên không trung. Trong phần All-star Sịp dùng một mảnh sao lùn có khối lượng 200 tỉ tỉ tấn làm chìa khóa pháo đài cô đơn và thường mở bằng 1 tay. Trong the new 52, Sịp cũng đã bị ép nâng tạ có khối lượng bằng Trái đất (5 triệu tỉ tỉ tấn) trong năm ngày liên tục. Trở về thời kì bạc, sịp vô tình hắt hơi thổi tung một thiên hà và kéo 1000 hành tinh bằng 1 sợi xích ra khỏi một thiên hà khác. Sịp cũng nâng được búa của Thor trong một cross over giữa DC và Marvel và đập bét nhè Hulk trong một crossover nữa."
                                +"Siêu bền: Thân thể của sịp có cấu tạo nguyên tử với mật độ dày đặc hơn titanium nhiều lần và phát ra một luồng năng lượng bức xạ không thể phá vỡ. Điều này khiến cho sịp trở nên miễn nhiễm với hầu hết tất cả các dạng thương tổn vật lý, bao gồm cả những lực tương tác cực đại và nhiệt độ cực cao. Không một loại vũ khí nào trên trái đất có thể gây ra ảnh hưởng dù là nhỏ nhất đến sịp, súng đạn còn chưa bằng gãi ghẻ, luồng điện 2 triệu volt không là gì. Sịp bơi giữa dung nham, ngủ trong lòng mặt trời, thoát ra khỏi lỗ đen, nói đùa khi ở giữa tâm của một vụ nổ siêu tân tinh. Sịp vô sự trước đòn tấn công bằng 1 triệu vụ nổ hạt nhân và chùm tia Omega siêu mạnh của Darkseid."
                                +"Sịp cũng miễn nhiễm trước tất cả các loại bệnh tật, virus và chất độc trên trái đất. Hơn thế nữa, Sịp sống cực dai, tồn tại đến tận thế kỉ thứ 853, tức là qua 83200 năm mà không chết( đhs nó vẫn lớn được)"
                                +"Siêu tốc độ:Tốc độ của sịp là không xác định, cho phép sịp di chuyển, phản xạ, chạy và bay nhanh khủng khiếp. Cùng thời gian ánh sáng đi từ trái đất tới mặt trời, sịp bay lên Mặt trăng trước 2 phút và lên mặt trời trước 8 phút, cho thấy sịp di chuyển nhanh hơn tốc độ ánh sáng. Thực tế sịp đã từng bay ngược chiều quĩ đạo của trái đất để đảo ngược thời gian. Sịp vẫn có thể suy nghĩ một cách bình thường trong khi di chuyển ở tốc độ cao và thường được so sánh với Flash về tốc độ( hòa nhau là chính).";
            
            // answer to question 5
            Comment answer7 = new Comment();
            answer7.User = normalUser12;
            answer7.Question = question5;
            answer7.CreationDate = DateTime.Now;
            answer7.Content = "Các chuyên gia về laser tại Đại học công nghệ Vienna, Áo, phát triển một thiết bị nhận tín hiệu laser mờ và xuất hiện đều đặn ở khoảng cách xa, với hy vọng xác định sự sống ngoài trái đất. Thiết bị này rất nhạy và có thể phát hiện ánh sáng yếu trên một phần tỷ của giây." +
                                     "nhóm nghiên cứu thực hiện thử nghiệm bằng cách mô phỏng ánh sáng laser ngoài hành tinh phát đi 10.000 nhịp của ánh sáng cận hồng ngoại ở khoảng cách 500 năm ánh sáng. Theo các chuyên gia, mặc dù tia laser được gửi đi với mức năng lượng rất cao, nhưng nó vẫn trở nên mờ nhạt khi đến nơi, bởi nó đã trải qua một đoạn đường dài."
                                  +"Từng có nhiều nhà thiên văn học nỗ lực tìm kiếm sự sống trong vũ trụ, nhưng họ đều không thu được kết quả tốt. Giáo sư Leeb cho biết thêm rằng dự án hiện vẫn chưa hoàn thiện, song công nghệ này sẽ cung cấp nhiều bằng chứng cho thấy con người không cô độc trong vũ trụ.";

            // answer 4 question 16
            Comment answer8 = new Comment();
            answer8.User = normalUser15;
            answer8.Question = question16;
            answer8.CreationDate = DateTime.Now;
            answer8.Content = "Lý do là chiếc muỗng sẽ phá vỡ bề mặt của các bong bóng, ngăn không cho bọt tràn ra khỏi thành nồi."
            +"Về cơ bản, bong bóng sẽ bị vỡ khi gặp một thứ không hấp thu nước và một chiếc muỗng gỗ chắc chắn phù hợp để làm công việc đó."
            +"Ngoài ra, những tính chất nhiệt động lực học của gỗ và nước cũng có thể được sử dụng để giải thích cho hiện tượng này."
            +"Gỗ là một chất dẫn nhiệt kém và sẽ chỉ hoạt động trong một khoảng thời gian giới hạn. Do đó, khi tiếp xúc với các bong bóng của nước đang ở 100 độ C, nhiệt độ thấp của thìa sẽ khiến hơi nước ngay lập tức ngưng tụ, trở về trạng thái lỏng";

            Comment answer9 = new Comment();
            answer9.User = normalUser17;
            answer9.Question = question16;
            answer9.CreationDate = DateTime.Now;
            answer9.Content = "Do gỗ hấp thu các hơi nước. Khiến cho bong bóng bị xì hơi.";

            Comment comment11 = new Comment();
            comment11.User = normalUser19;
            comment11.ParentComment = answer8;
            comment11.CreationDate = DateTime.Now;
            comment11.Content = "Tôi nghĩ do sức căng bề mặt của gỗ và nước bọt lớn làm vỡ bọt khí trước khi trào ra ngoài.";

           


            // set Comment
            Comment comment1 = new Comment();
            comment1.User = normalUser5;
            comment1.ParentComment = answer1;
            comment1.Question = question1;
            comment1.CreationDate = DateTime.Now;
            comment1.Content = "10. Dù phát hiện ra có người thứ ba xen vào cũng không dám làm to chuyện, vì sợ mất đi nguồn kinh tế nên đành cắn răng một mắt nhắm, một mắt mở cho qua. "
                                        +"11. Để tương lai, khi cãi nhau, muốn đập cửa mà bỏ đi, lại không có nơi nào để đi, chỉ có thể đừng khóc dưới mưa, nghe đối phương nói: “Ngoài tôi đây thì cô còn chỗ nào để đi hay sao?” "
                                        +"12. Mọi tình yêu và hôn nhân trong điều kiện sinh hoạt chất lượng thấp, phần lớn đều không duy trì được tới cuối cùng. Ngay từ đâu, khi bị tình yêu làm mê man đầu óc, có thể các bạn vẫn sẽ có những ngày tháng ngọt ngào, nhưng một khi đã đề cập tới những vấn đề cơm áo gạo tiền, những tranh cãi sẽ bộc phát như lũ về cuồn cuộn. Quan điểm cá nhân của từng người vốn là không giống nhau, nên không thể nói rõ ai đúng ai sai, chỉ là không thích hợp mà thôi. Câu “môn đăng hộ đối” kể cả trong xã hội hiện đại vẫn có ý đúng. ";


            Comment comment2 = new Comment();
            comment2.User = normalUser2;
            comment2.ParentComment = answer1;
            comment2.Question = question1;
            comment2.CreationDate = DateTime.Now;
            comment2.Content = "Bổ sung tiếp :13. Người có tiền không phải kẻ ngốc, nên đương nhiên là họ cũng sẽ không lấy một người ngốc về. "
                                        +"14. Tôi không cho rằng phụ nữ hiếu thắng là cực đoan. Tôi không cho rằng phụ nữ có thể vác thùng thước là phải chịu sự xem thường. Tôi không cho rằng phụ nữ độc thân đáng bị lên án. Tôi không cho rằng phụ nữ làm việc quyết đoán bình tĩnh là không nữ tính. Tôi không cho rằng phụ nữ béo gầy, xấu đẹp là yếu tố quan trọng quyết định xem người đó có sống tốt hay không. Tôi cũng không cho rằng phụ nữ khi đối mặt với sự phân biệt giới tính lại nên kìm nén, bỏ qua. Và buồn cười nhất chính là có những người phán đoán một người phụ nữ có thành công hay không bằng cách xem họ có lấy được một người chồng tốt hay không. "
                                        +" 15. Người phụ nữ thành công chân chính là người phụ nữ có thể giúp đỡ chồng mình mình vượt qua gian truân khi người chồng gặp khó khăn. "
                                        +"16. Bạn có thể không trở thành một người phụ nữ thành đạt nhưng bạn nhất định phải có một công việc ổn định, có thu nhập ổn định. Bạn có độc lập thì người đàn ông mới để mắt tới bạn. Dù lấy được một người tốt tới đâu, thì tiền cũng là của người đàn ông, người ta cho bạn dùng thì bạn mới có, nếu người ta không cho bạn thì bạn chẳng có gì cả. ";
            

            Comment comment3 = new Comment();
            comment3.User = normalUser3;
            comment3.ParentComment = answer1;
            comment3.Question = question1;
            comment3.CreationDate = DateTime.Now;
            comment3.Content = "Tớ bổ sung thêm nhé :17. Sau này mua thức ăn, bạn không nhất định phải dùng tới mấy môn như Toán cao cấp, nhưng học tốt môn Toán cao cấp ấy có thể sẽ quyết định xem tương lai bạn mua thức ăn ở đâu. "
                                        +"18. Có kinh tế độc lập mới có quyền được tự do lựa chọn. "
                                        +"19. Nào đâu có chuyện vịt hóa thiên nga, vịt con xấu xí vốn đã là thiên nga, cũng như cô bé Lọ Lem vốn đã là con gái nhà giàu. "
                                        +"20. Tại sao không khiến mình trở nên ưu tú hơn để thu hút người khác mà lại phải chạy theo đuôi người ta? ";

            Comment comment4 = new Comment();
            comment4.User = normalUser3;
            comment4.ParentComment = answer1;
            comment4.Question = question1;
            comment4.CreationDate = DateTime.Now;
            comment4.Content = "Vẫn còn thiếu 2 lý do nữa là: " +
                                      "21. Không bằng cấp, không năng lực, không dáng người, không ngoại hình, không IQ, không EQ, không cùng thế giới quan, giá trị quan, không cùng tư tưởng tầng lớp, không cùng hoàn cảnh, dựa vào đâu mà bạn muốn một người đàn ông tốt có ngoại hình, có năng lực, có tố chất, có phong độ, có điều kiện coi trọng bạn? Bạn không đủ hoàn mỹ, không có tiến bộ, ngày lại ngày qua, khi tuổi tác bạn già đi thì giá trí của bạn cũng theo đó mà giảm dần. Tình yêu chính là món đầu tư có tính phiêu lưu cao nhất. "
                                     +"22. Em muốn cố gắng kiếm tiền, không phải vì em yêu tiền, mà là vì đời này, em không muốn vì tiền mà ở bên một ai đó, và cũng không muốn vì tiền mà rời xa một ai đó. Nếu hỏi tình yêu và bánh mì, em chọn cái nào, em sẽ nói, anh chỉ cần cho em tình yêu thôi, còn bánh mì thì để em tự mình mua.";
            // comment 2 answer 2
            Comment comment5 = new Comment();
            comment5.User = normalUser7;
            comment5.ParentComment = answer2;
            comment5.Question = question2;
            comment5.CreationDate = DateTime.Now;
            comment5.Content = "Hết muốn nói! Con ngừoi hãy bảo vệ thú rừng tự nhiên đi chứ! Lạy lục van xin cũng vô ích sao...Trời ơi! Đau xót ";

            Comment comment6 = new Comment();
            comment6.User = normalUser8;
            comment6.ParentComment = answer2;
            comment6.Question = question2;
            comment6.CreationDate = DateTime.Now;
            comment6.Content = "Nên làm nhu ở nuoc ngoài, họ đem thú hiếm về nuôi trong sở thú, vừa 'bảo vệ' và giúp loài thú này sinh sôi nẩy nở và sau đó lại đem thả về thiên nhiên , nhiều giong vật nhờ thế mà đã không còn bị đe doạ tuyệt chủng nữa .";

            Comment comment7 = new Comment();
            comment7.User = normalUser8;
            comment7.ParentComment = answer3;
            comment7.Question = question2;
            comment7.CreationDate = DateTime.Now;
            comment7.Content = "Vâng loài Hổ đang và sắp tuyệt chủng tại VN ... thực tại đáng buồn ....";

            Comment comment8 = new Comment();
            comment8.User = normalUser8;
            comment8.ParentComment = answer4;
            comment8.Question = question3;
            comment8.CreationDate = DateTime.Now;
            comment8.Content = "Xin nói thêm về loài rắn trên cạn" +
                                       "Rắn Taipan nội địa"
                                        + "Loài rắn độc nhất trên mặt đất là “Rắn dữ” (Fierce Snake), còn được biết là “Taipan nội địa” (có tên khoa học: Oxyuranus microlepidotus) là loài bò sát thuộc họ Rắn hổ (Elapidae) có nguồn gốc từ Australia. Đây là loài rắn có nọc độc độc nhất so với bất kỳ loài rắn sống trên cạn nào trên thế giới."
                                        +"Lượng nọc tối đa của một vết cắn là 110mg, đủ để giết chết 100 người, hay 250.000 con chuột. Mức độ độc của nọc rắn này gấp 10 lần rắn chuông Mojave và 50 lần rắn hổ mang thường.";
            // Comment 2 answer 6
            Comment comment9 = new Comment();
            comment9.User = normalUser11;
            comment9.ParentComment = answer5;
            comment9.Question = question4;
            comment9.CreationDate = DateTime.Now;
            comment9.Content = "Superman gặp đá xanh krypton là tắt điện, đánh đấm gì";
            
            Comment comment10 = new Comment();
            comment10.User = normalUser10;
            comment10.ParentComment = answer5;
            comment10.Question = question4;
            comment10.CreationDate = DateTime.Now;
            comment10.Content = "Sịp đỏ vs batman ai win nhỉ?";
            
            

            //Add
            context.Rights.Add(userRight);
            context.Rights.Add(adminRight);

            context.Users.Add(adminUser);
            context.Users.Add(normalUser1);
            context.Users.Add(normalUser2);
            context.Users.Add(normalUser3);
            context.Users.Add(normalUser4);
            context.Users.Add(normalUser5);
            context.Users.Add(normalUser6);
            context.Users.Add(normalUser7);
            context.Users.Add(normalUser8);
            context.Users.Add(normalUser9);
            context.Users.Add(normalUser10);
            context.Users.Add(normalUser11);
            context.Users.Add(normalUser12);
            context.Users.Add(normalUser13);
            context.Users.Add(normalUser14);
            context.Users.Add(normalUser15);
            context.Users.Add(normalUser16);
            context.Users.Add(normalUser17);
            context.Users.Add(normalUser18);
            context.Users.Add(normalUser19);
            context.Users.Add(normalUser20);
            context.Users.Add(normalUser21);
            context.Users.Add(normalUser22);
            context.Users.Add(normalUser23);
            context.Users.Add(normalUser24);
            context.Users.Add(normalUser25);
            context.Users.Add(normalUser26);
            context.Users.Add(normalUser27);
            context.Users.Add(normalUser28);
            context.Users.Add(normalUser29);
            context.Users.Add(normalUser30);

           

            context.Questions.Add(question1);
            context.Questions.Add(question2);
            context.Questions.Add(question3);
            context.Questions.Add(question4);
            context.Questions.Add(question5);
            context.Questions.Add(question6);
            context.Questions.Add(question7);
            context.Questions.Add(question8);
            context.Questions.Add(question9);
            context.Questions.Add(question10);
            context.Questions.Add(question11);
            context.Questions.Add(question12);
            context.Questions.Add(question13);
            context.Questions.Add(question14);
            context.Questions.Add(question15);
            context.Questions.Add(question16);

            context.Comments.Add(comment1);
            context.Comments.Add(comment2);
            context.Comments.Add(comment3);
            context.Comments.Add(comment4);
            context.Comments.Add(comment5);
            context.Comments.Add(comment6);
            context.Comments.Add(comment7);
            context.Comments.Add(comment8);


            context.Comments.Add(answer1);
            context.Comments.Add(answer2);
            context.Comments.Add(answer3);
            context.Comments.Add(answer4);
            context.Comments.Add(answer5);
            context.Comments.Add(answer6);
            context.Comments.Add(answer7);
            
            //base.Seed(context);
        }
       
    }
         
}
